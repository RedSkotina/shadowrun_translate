import bs4
from bs4 import BeautifulSoup as Soup

print(bs4.builder.HTMLTreeBuilder.preserve_whitespace_tags)
bs4.builder.HTMLTreeBuilder.preserve_whitespace_tags |= set(["p","br"])
print("=>")
print(bs4.builder.HTMLTreeBuilder.preserve_whitespace_tags)
def test():
    r = "<p>   </p><pre>   </pre> <p><br>   </br></p>"
    soup_text = Soup(r)
    print("'"+r+"'")
    print("'"+str(soup_text)+"'")
    print("preserve_whitespace_tags test result:")
    print (r == str(soup_text))
    
    
        
test()