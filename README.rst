===================
shadowrun_translate
===================

shadowrun_translate is a tools for localization games Shadowrun Returns and 
Dragonfall Direct's Cut Extended

shadowrun_translate supports only python 3 x32.

The project code and bugtracker is hosted on 
`Bitbucket <http://bitbucket.org/RedSkotina/shadowrun_translate/>`_. 

Thanks for downloading shadowrun_translate !

Using sources:

Install 3dparty packages:: 
    py -3 -m pip install -r ./requirements.txt
