﻿#!/usr/bin/env python3

# Ugly hack to allow absolute import from the root folder
# whatever its name is. Please forgive the heresy.
'''if __name__ == "__main__" and __package__ is None:
    import sys; import os
    sys.path.append(os.path.abspath('..'))
    __package__ = "shadowrun_returns"
''' 
import sys
sys.path.insert(0, 'shadowrun_returns/lib/proto')

import argparse
#import shadowrun_returns.core.global_settings
from shadowrun_returns.core import *
from shadowrun_returns.lib.utils import * 
from shadowrun_returns.core.extract import * 
from shadowrun_returns.core.translate import * 
from shadowrun_returns.core.fetch import * 
from shadowrun_returns.core.compare import *  
from shadowrun_returns.core.make import *

class settings:
    version = '0.7'
    src_root_path = None
    dst_root_path = None
    src_local_path = ''
    dst_local_path = ''
    localization_path = 'data/fetch'
    extract_path = 'data/extract'
    mode = 'extract'
    deep_analysis = False
    platform = "pc"
    user = None
    password = None
    game_version = None
    language='ru'
    default_language='fr'
    debug = True
    auto_path = True
    fetch_source = None
    @classproperty
    def src_path(self):
        if not self.auto_path or self.src_root_path.startswith('http:'):
            return self.src_root_path + '/' + self.src_local_path
        return os.path.join(os.path.normpath(os.path.join(self.src_root_path, self.platform, self.game_version)),self.src_local_path)
    @classproperty
    def dst_path(self):
        if not self.auto_path:
            return self.dst_root_path + '/' + self.dst_local_path
        return os.path.join(os.path.normpath(os.path.join(self.dst_root_path, self.platform, self.game_version)),self.dst_local_path)
        
class settings_extract:
    src_root_path = 'data/input'
    dst_root_path = 'data/extract'
class settings_merge:
    src_root_path = 'data/extract'
    dst_root_path = 'data/extract'
class settings_translate:
    src_root_path = 'data/input'
    dst_root_path = 'data/output'
class settings_fetch:
    src_root_path = 'http://notabenoid.org'
    dst_root_path = 'data/fetch'
    fetch_context = False
class settings_compare:
    src_root_path = 'data/fetch'
    dst_root_path = 'data/compare'
    
current_mode = lambda : None

def set_mode(mode):
    settings.mode = mode
    
class DeepAnalysisAction(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
        #print ('%r %r %r' % (namespace, values, option_string))
        #setattr(namespace, self.dest, values)
        settings.deep_analysis = values
class LanguageAction(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
        #print ('%r %r %r' % (namespace, values, option_string))
        settings.language = values
class GameVersionAction(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
        #print ('%r %r %r' % (namespace, values, option_string))
        settings.game_version = values
class UserAction(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
        settings.user = values   
class PasswordAction(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
        settings.password = values   
class DefaultSourceAction(argparse.Action):
    CHOICES = ['original','extended']
    def __call__(self, parser, namespace, values, option_string=None):
        if values:
            for value in values:
                if value not in self.CHOICES:
                    message = ("invalid choice: {0!r} (choose from {1})"
                               .format(value,
                                       ', '.join([repr(action)
                                                  for action in self.CHOICES])))

                    raise argparse.ArgumentError(self, message)
            setattr(namespace, self.dest, values)
            
def parse_args():
    parser = argparse.ArgumentParser(description='translate games of shadowrun returns series ')
    parser.add_argument('-i','--inputdir', help='Input directory', type=str, default=None)
    parser.add_argument('-o','--outputdir', help='Output directory', type=str, default=None)
    parser.add_argument('-v', '--version', action='version', version='shadowrun_returns_translator ' + settings.version + '    Creative Commons Attribution 4.0 International Public License')
    subparsers = parser.add_subparsers(dest='subparser_name')
    subparsers.required = True
    parser_extract = subparsers.add_parser('extract', help='extract strings')
    parser_extract.add_argument('-d','--deepanalysis', help='Prefer use game files over mo files', type=bool, action=DeepAnalysisAction,default=True)
    parser_extract.add_argument('-v','--version', help='Prefer use specific game version', type=str, action=GameVersionAction,default=None)
    parser_extract.set_defaults(func=lambda: set_mode('extract'))
    parser_translate = subparsers.add_parser('translate', help='translate strings')
    parser_translate.add_argument('-d','--deepanalysis', help='Prefer use game files over mo files', type=bool, action=DeepAnalysisAction, default=True)
    parser_translate.add_argument('-v','--version', help='Prefer use specific game version', type=str, action=GameVersionAction,default=None)
    parser_translate.add_argument('-l','--language', help='Target localization language',  type=str, action=LanguageAction, required = True)
    parser_translate.add_argument('-p','--locpath', help='Directory with localization files', type=str, default=None)
    parser_translate.set_defaults(func=lambda: set_mode('translate'))
    parser_fetch = subparsers.add_parser('fetch', help='fetch translated strings')
    parser_fetch.add_argument('-u','--user', help='Login', type=str, action=UserAction, default=None)
    parser_fetch.add_argument('-p','--password', help='Password',  type=str, action=PasswordAction, default=None)
    parser_fetch.add_argument('source', nargs='*', action=DefaultSourceAction, default = ['original', 'extended'], metavar='SOURCE')
    parser_fetch.set_defaults(func=lambda: set_mode('fetch'))
    parser_compare = subparsers.add_parser('compare', help='compare fetched and extracted strings')
    parser_compare.add_argument('-p','--extract_path', help='Directory with extracted strings', type=str, default=None)
    parser_compare.set_defaults(func=lambda: set_mode('compare'))
    parser_make = subparsers.add_parser('make', help='make public patch')
    parser_make.add_argument('-l','--language', help='Target localization language',  type=str, action=LanguageAction, required = True)
    parser_make.add_argument('-p','--locpath', help='Directory with localization files', type=str, default=None)
    parser_make.set_defaults(func=lambda: set_mode('make'))
    
    args = parser.parse_args()
    args.func()
    
    if settings.mode=='fetch' and len([x for x in (args.user,args.password) if x is not None]) == 1:
        parser.error('--user and --password must be given together')
    if settings.mode=='fetch' and args.user is None:
        print('Please enter credentials:')
        settings.user = input('User: ')
        settings.password = input('Password: ')
    if settings.mode=='fetch':
        settings.fetch_source = args.source
    if settings.mode == 'extract':
        if len([x for x in (args.inputdir,args.outputdir) if x is not None]) == 1:
            parser.error('--inputdir and --outputdir must be given together')
    if settings.mode == 'translate' or settings.mode == 'make' :
        settings.localization_path = args.locpath if args.locpath is not None else settings.localization_path
        if args.inputdir is None and args.outputdir is not None:
            settings.src_root_path = args.outputdir
    if settings.mode == 'compare':
        settings.extract_path = args.extract_path if args.extract_path is not None else settings.extract_path
            
        #if args.inputdir is None and args.outputdir is not None:
        #    settings.src_root_path = args.outputdir
            
    settings.auto_path = True if args.inputdir is None and args.outputdir is None else False
    settings.src_root_path = args.inputdir if args.inputdir is not None else settings.src_root_path
    settings.dst_root_path = args.outputdir if args.outputdir is not None else settings.dst_root_path
    
    #remove slashes from end of pathes
    settings.src_root_path = settings.src_root_path.rstrip('\\').rstrip('/') if settings.src_root_path is not None else None
    settings.dst_root_path = settings.dst_root_path.rstrip('\\').rstrip('/') if settings.dst_root_path is not None else None
    
    if settings.mode == 'make':
        if settings.src_root_path == settings.dst_root_path:
            parser.error('--inputdir and --outputdir must be not same while make')
            
    mode_dispatcher={'extract':extract,'translate':translate,'fetch':fetch, 'compare':compare, 'make':make}

    try:
        global current_mode
        current_mode=mode_dispatcher[settings.mode]
    except KeyError:
        raise ValueError('invalid mode')
    
def show_settings():
    print ("mode: " + settings.mode)
    print ("game_version: " + repr(settings.game_version))
    print ("src_path: " + settings.src_path)
    print ("dst_path: " + settings.dst_path)
    #print ("deep_analysis: %d" % settings.deep_analysis)
    
def apply_mode_settings(settings,mode_settings):
    for k in vars(mode_settings):
        if not k.startswith('__') and (not hasattr(settings,k) or getattr(settings,k) == None):
            setattr(settings, k, getattr(mode_settings,k))
    if settings.debug:
        show_settings()
    
def apply_settings(module):
    module.settings = type('settings', (settings,), dict(module.settings.__dict__)) 
        
def process(module):
    apply_settings(module)
    r = module.process()
    return r
    
def fetch():
    settings.game_version =  '' # TODO: evade bug
    apply_mode_settings(settings,settings_fetch)
    if 'original' in settings.fetch_source:
        process(fetch_original)
    if 'extended' in settings.fetch_source:
        process(fetch_extended)
    
def extract():
    
    settings.game_version = process(detect_target_version)
    apply_mode_settings(settings,settings_extract)
    
    if settings.game_version == "1.2.7":
        process(extract_interface)
        process(extract_deadman)
        process(extract_seattle)
        process(extract_berlin_abilities)
        process(extract_berlin_chars)
        process(extract_berlin_items)
        process(extract_berlin_modes)
        process(extract_berlin_totems)
        process(extract_berlin_campaign_abilities)
        process(extract_berlin_campaign_chars)
        process(extract_berlin_campaign_convos)
        process(extract_berlin_campaign_items)
        process(extract_berlin_campaign_maps)
        process(extract_berlin_campaign_misc)
        process(extract_berlin_campaign_modes)
        process(extract_berlin_campaign_scenes)
        process(extract_berlin_campaign_stories)
        apply_mode_settings(settings,settings_merge)
        process(merge_po_berlin)
        process(merge_po_berlin_campaign)
    elif settings.game_version == "2.0.9":
        process(extract_seattle) # 2.0.9 bug.
        process(extract_interface)
        process(extract_berlin_abilities)
        process(extract_berlin_chars)
        process(extract_berlin_items)
        process(extract_berlin_modes)
        process(extract_berlin_totems)
        process(extract_dragonfallextended_abilities)
        process(extract_dragonfallextended_chars)
        process(extract_dragonfallextended_convos)
        process(extract_dragonfallextended_items)
        process(extract_dragonfallextended_maps)
        process(extract_dragonfallextended_misc)
        process(extract_dragonfallextended_modes)
        process(extract_dragonfallextended_scenes)
        process(extract_dragonfallextended_stories)
        apply_mode_settings(settings,settings_merge)
        process(merge_po_berlin)
        process(merge_po_dragonfallextended)
    else:
        print('Dont have support for %s version' % settings.game_version)
        
def translate():
    
    settings.game_version = process(detect_target_version)
    
    apply_mode_settings(settings,settings_translate)
    #process(translate_font)
    #TODO: dont work with gog version
    #settings.target_language = process(detect_target_language)
    settings.target_language = settings.language
    
    if settings.game_version == "1.2.7":
        settings.localization_path = settings.localization_path + '/original' # TODO: refactor
        if settings.deep_analysis:
            process(translate_seattle)
            process(translate_deadman)
        else:
            process(translate_seattle_mo)
            process(translate_deadman_mo)
            
        process(translate_berlin_mo)
        process(translate_berlin_campaign_mo)
        process(translate_berlin_campaign_ext)
        process(translate_interface_mo)
        
    elif settings.game_version == "2.0.9":
        settings.localization_path = settings.localization_path + '/extended' # TODO: refactor
        process(translate_shadowrun_core_mo) # important! required because game bug exist !
        process(translate_berlin_mo)
        process(translate_dragonfallextended_mo)
        process(translate_dragonfallextended_ext)
        process(translate_interface_mo)
        process(translate_assembly)
    else:
        print('Dont have support for %s version' % settings.game_version)
    
def compare():
    apply_mode_settings(settings,settings_compare)
    process(compare_patch_strings)

def make():
    
    settings.game_version = process(detect_target_version)
    
    apply_mode_settings(settings,settings_translate)
    #process(translate_font)
    #TODO: dont work with gog version
    #settings.target_language = process(detect_target_language)
    settings.target_language = settings.language
    
    if settings.game_version == "2.0.9":
        settings.localization_path = settings.localization_path + '/extended' # TODO: refactor
        
        process(translate_shadowrun_core_mo) # important! required because game bug exist !
        process(translate_berlin_mo)
        process(translate_dragonfallextended_mo)
        process(translate_dragonfallextended_ext)
        
        #apply_settings(make_environment)
        #translate_interface_mo.inject_interface = make_environment.make_environment
        
        process(translate_interface_mo)
        process(translate_assembly)
        process(make_environment)
    else:
        print('Dont have support for %s version' % settings.game_version)
        
if __name__ == "__main__":
    parse_args()
    current_mode()
    