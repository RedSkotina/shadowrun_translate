#!/usr/bin/env python3
import sys, os, urllib
#import http.cookiejar
from bs4 import BeautifulSoup as Soup
from soupselect import select
import polib
import argparse
import urllib3
import http.cookies
import time
class settings:
    src_root_path = ''
    src_path = ''
    dst_root_path = 'http://notabenoid.org/'
    dst_path = 'book/42395'
    upload_context = True
    debug = True
    book_id = '42395'
    
class Site:
    conn = None
    headers = None
    cookie = None
    base_url = ''
    user = None
    password = None
    def __init__(self,url):
        self.base_url = url
    def connect(self):
        self.conn = urllib3.connectionpool.HTTPConnectionPool(self.base_url)
        payload = urllib.parse.urlencode({'login[login]': self.user, 'login[pass]': self.password }) 
        self.headers = urllib3.make_headers(user_agent='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.11) Gecko/20101012 Firefox/3.6.11')
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        r = self.conn.urlopen('POST', '/', body=payload, headers=self.headers, redirect=False)
        #if r.status != 200:
        #    print ("EROOR: Can't authorize %s  -  check user and password..." % self.base_url)
        #    sys.exit()
        self.cookie=http.cookies.SimpleCookie(r.headers['set-cookie'])
        self.headers['Cookie'] = self.cookie.output(attrs=[], header='').strip()
        if settings.debug:
            print ("Successufull authorize %s" % self.base_url)
        time.sleep(1)
        
    def open_url(self, url):
        #self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        r = self.conn.urlopen('GET', url, headers=self.headers)
        if ('set-cookie' in r.headers):
            self.cookie.load(r.headers['set-cookie'])
            self.headers['Cookie']=self.cookie.output(attrs=[], header='').strip()
        return r
        
    def form_submit(self, url, form_data):
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        r = self.conn.urlopen('POST', url, body=form_data, headers=self.headers, redirect=False)
        if ('set-cookie' in r.headers):
            self.cookie.load(r.headers['set-cookie'])
            self.headers['Cookie']=self.cookie.output(attrs=[], header='').strip()
        return r
        
def create_chapter(site, book_id, chapter_title):
    form_url = '/book/%s/0/edit?ajax=1&placement=1' %(book_id)
    book_url = '/book/%s' % (book_id)
    form_data = urllib.parse.urlencode({'Chapter[title]': chapter_title, 'Chapter[status]': 0 }).encode('utf-8')
    r = site.form_submit(form_url, form_data)
    r = site.open_url(book_url)
    if r.status != 200:
        print ("EROOR: Can't create chapter %s  error_code = %d" % (chapter_title, r.status))
        sys.exit()
    
    # find created chapter id
    soup_html = Soup(r.data)
    r_chapter_titles_tr = select(soup_html, 'table#Chapters tr')
    for i in range(len(r_chapter_titles_tr)):
        soup_tr = Soup(r_chapter_titles_tr[i].encode('utf-8'))
        for k in soup_tr.find_all('tr'):
            soup_td = Soup(k.encode('utf-8'))
            for z in soup_td.find_all('td', class_="t"):
                soup_a = Soup(z.encode('utf-8'))
                for az in soup_a.find_all('a'):
                    #search td with title chapter_title
                    if az.text == chapter_title:
                        # up to td node and 
                        for yz in soup_td.find_all('td'):
                            soup_ya = Soup(yz.encode('utf-8'))
                            for ayz in soup_ya.find_all('a'):
                                #search td with import link
                                if ayz['href'].endswith('import'):
                                    #print(az['href'])
                                    #print ("imp: " + ayz['href'])
                                    # return first new chapter id
                                    return az['href'].split('/')[-1]
    return None            

def add_original(site, book_id, chapter_id, record_id, text):
    form_url = '/book/%s/%s/0/edit' % (book_id,chapter_id)
    form_data = urllib.parse.urlencode({'Orig[ord]': record_id, 'Orig[body]': text, }).encode('utf-8')
    r = site.form_submit(form_url, form_data)
    return r.headers['Location'].split('/')[-1]
    '''
    r = site.open_url(r.headers['Location'])
    
    if r.status != 200:
        print ("EROOR: Can't add original to chapter_id %s  record_id %s error_code = %d" % ( chapter_id, record_id, r.status))
        print ('text: "%s"' % text)
        sys.exit()
    soup_html = Soup(r.data)
    r_records_a = select(soup_html, 'table#Tr tr td.o p.info a.ord')
    for record_a in r_records_a:    
        r_record_id = record_a['href'].split('#')[-1]
        if r_record_id == record_id:
            record_internal_id = record_a['href'].split('#')[0].split('/')[-1]
            return record_internal_id
    '''
def add_translate(site, book_id, chapter_id, record_internal_id, text):
    form_url = '/book/%s/%s/%s/translate' %(book_id,chapter_id, record_internal_id)
    form_data = urllib.parse.urlencode({'Translation[body]': text, }).encode('utf-8')
    r = site.form_submit(form_url, form_data)
    
    '''r = site.open_url(r.headers['Location'])
    if r.status != 200:
        print ("EROOR: Can't add translate to chapter_id %s record_internal_id %s  error_code = %d" % (chapter_id, record_internal_id, r.status))
        print ('text: "%s"' % text)
        sys.exit()
    ''' 
def add_comment(site, book_id, chapter_id, record_internal_id, text):
    form_url = '/book/%s/%s/%s/c0/reply' %(book_id,chapter_id, record_internal_id)
    form_data = urllib.parse.urlencode({'Comment[body]': text, }).encode('utf-8')
    r = site.form_submit(form_url, form_data)
    
def add_record(site, book_id, chapter_id, record_id, original_text, translate_text, context, comment):
    record_internal_id = add_original(site, book_id, chapter_id, record_id, original_text)
    if not record_internal_id:
        print('Cant find created original')
        sys.exit()
    print('%s\t%s' % (record_id,record_internal_id))
    
    add_translate(site, book_id, chapter_id, record_internal_id, translate_text)
    if not settings.upload_context:
        return
    context_text = comment_text = ''
    if context is not None and context != '':
        context_text = '#msgctxt "%s"' % context
    if comment is not None and comment != '':
        comment_text = '#comment "%s"' % comment
    separator = '\n' if context_text != '' and comment_text != '' else ''
    full_comment_text = context_text + separator + comment_text 
    if full_comment_text != '':
        add_comment(site, book_id, chapter_id, record_internal_id, full_comment_text)
    
def process():
    parser = argparse.ArgumentParser(description='upload to notabenoid.org ')
    parser.add_argument('chapter', nargs='?', type=str, default='')
    parser.add_argument('pofile', nargs='?', type=str, default='')
    args = parser.parse_args()
    if args.chapter == '' or args.pofile == '':
        print('check arguments')
        sys.exit()
        
    print('Please enter credentials:')
    user = input('User: ')
    password = input('Password: ')
        
    site = Site('notabenoid.org')
    site.user = user
    site.password = password
    site.connect()
    
    book_id = settings.book_id
    chapter_title = args.chapter
    chapter_id = create_chapter(site, book_id, chapter_title)
    if not chapter_id:
        print('Cant find created chapter')
        sys.exit()
    print(chapter_id)
    
    record_id = 1
    po_file = polib.pofile(args.pofile)
    for po_entry in po_file:
        add_record(site, book_id, chapter_id, str(record_id), po_entry.msgid, po_entry.msgstr, po_entry.msgctxt, po_entry.comment)
        record_id = record_id + 1
        
process()        