from winreg import *
import sys, os
from lunity import lunity

class settings:
    root_key = r"Software\Harebrained Schemes\Dragonfall"
    language_pattern = "Settings.Language"
    data_path = "Dragonfall_Data/"
def read_language_from_registry():
    current_language_key = None
    current_language_value = None

    try:
        aReg = ConnectRegistry(None,HKEY_CURRENT_USER)
        aKey = OpenKey(aReg, settings.root_key)
    
        i = 0
        while True:
            subkey = EnumValue(aKey, i)
            if subkey[0].startswith(settings.language_pattern):
                current_language_key = subkey[0]
                current_language_value = subkey[1]
            i += 1
    except WindowsError:
        # WindowsError: [Errno 259] No more data is available    
        pass

    if current_language_key is None:
        print ("Cant find %s/%s" % (settings.root_key,settings.language_pattern))
        #sys.exit()

    #print ("%s = %s" % (current_language_key,current_language_value))
    return (current_language_key,current_language_value)
    
def write_language_to_registry(language_key, language_value):
    aReg = ConnectRegistry(None,HKEY_CURRENT_USER)
    aKey = OpenKey(aReg, settings.root_key, 0, KEY_ALL_ACCESS)    
    SetValueEx(aKey, current_language_key,0, REG_SZ, language_value)

def read_languages_from_datafiles():
    main_assets_filename = settings.data_path + "mainData"
    locs = []
    if os.path.exists(main_assets_filename) and os.path.exists('./lib/unity/unity.dll'):
        fs_dict_file = lunity.extract_fs_dict_file(main_assets_filename)
        loc_resources = lunity.search_localization_resource(fs_dict_file)
        #print('\tdetected localizations: %s' % loc_resources)
        for loc_resource in loc_resources:
            splited_name = loc_resource.split('/')
            locs.append(splited_name[1])
        return  locs       
    else:
        return None
import argparse        
import EasyDialogs
is_gui = False
parser = argparse.ArgumentParser(description='') 
parser.add_argument('-l','--language', help='Language', type=str, default=None)
args = parser.parse_args()
if args.language is None:
    is_gui = True
    
current_language_key,current_language_value = read_language_from_registry()
if current_language_key is None:
    msg="Cant find current language in registry HKEY_CURRENT_USER/%s.\nTry atleast one time start game and enter to Options" % (settings.root_key)
    if is_gui:
        EasyDialogs.Message(msg)
    else:
        print(msg)
    sys.exit()

msg='Current language: %s' % current_language_value
languages = read_languages_from_datafiles()
#print (languages)
if languages is None:
    msg = "Game languages: NOT DETECTED\n" + msg
    languages = ['it', 'fr', 'ru', 'es', 'cn', 'de']
else:
    msg = "Game languages: DETECTED\n" + msg
# default language
languages.insert(0,'en')
cur_idx = 0
try:
    cur_idx = languages.index(current_language_value.lower())
except ValueError:
    cur_idx = 0

if is_gui:
    choice = EasyDialogs.ChoiceBox(msg = msg, title = 'Language of Shadowrun Returns: Dragonfall',  optionlist=languages, default_idx = cur_idx)
else:
    choice = args.language if args.language in languages else None
    sys.stdout.write("Set language: " + str(choice))
if choice is not None:
    write_language_to_registry(current_language_key,choice.upper())
    sys.exit(0)