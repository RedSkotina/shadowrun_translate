# -*- coding: utf-8 -*-
#from translator.utils import *
#from translator.counters import *
import sys
import polib
import argparse

class settings:
    src_path = './extract/pc/1.2.3/StreamingAssets/ContentPacks/berlin/data/'
    dst_path = './extract/pc/1.2.3/StreamingAssets/ContentPacks/berlin/data/loc/'
    translate_path = './corrected/'
    dst_po_file = 'berlin.po'
    debug = True
    
import os, errno

def silent_remove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured
        
def create_po_file(original_list):
    po = polib.POFile()
    po.metadata = {
    'Project-Id-Version': '1.0',
    'Report-Msgid-Bugs-To': 'you@example.com',
    'POT-Creation-Date': '2007-10-18 14:00+0100',
    'PO-Revision-Date': '2007-10-18 14:00+0100',
    'Last-Translator': 'you <you@example.com>',
    'Language-Team': 'English <yourteam@example.com>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=utf-8',
    'Content-Transfer-Encoding': '8bit',
    'Language': 'ru',
    }
    for original_str in original_list:
        entry = polib.POEntry(
            msgid = original_str,
            msgstr = u'' )
        po.append(entry)
        
    return po
def readall_po_file(po_file):
    original_list = []
    if po_file == None:
        return []
    for po_entry in po_file:
        original_list.append(po_entry.msgid)
    return original_list

def compare_po(pofile1, pofile2, pofile3):
    
    po1 = polib.pofile(pofile1)
    po2 = polib.pofile(pofile2)
    
    original_list1 = readall_po_file(po1)
    original_list2 = readall_po_file(po2)
    
    set1 = set(original_list1)
    set2 = set(original_list2)
    
    result_set = set1 - set2
    result_list = list(result_set)
    po_file = create_po_file(result_list)
    #assure_path_exists( )
    po_file.save(pofile3)

        
def process():
    if settings.debug:
        print (u'compare_po')
    #silent_remove(os.path.join(settings.dst_path, settings.dst_po_file))
    parser = argparse.ArgumentParser(description='compare po files: (pofile1 - pofile2) and save difference to pofile3.')
    parser.add_argument('pofile1', type=str, nargs='?', default="")
    parser.add_argument('pofile2', type=str, nargs='?', default="")
    parser.add_argument('pofile3', type=str, nargs='?', default="")
    args = parser.parse_args()
    if args.pofile1 == "" or args.pofile1 == "" or args.pofile3 == "":
        print ("Check filenames. Require 3 filename")
    
    compare_po(args.pofile1, args.pofile2, args.pofile3)    
    
if __name__ == "__main__":
    process()