#!/usr/bin/env python3
from bs4 import BeautifulSoup as Soup
import shutil
import os
import io
import polib
import sys; import os
sys.path.append(os.path.abspath('..'))
from shadowrun_returns.lib.utils import *

class settings:
    src_path = './corrected/'
    dst_path = './translate/'
    debug = True

#!/usr/bin/env python3
def correct_grammar_errors():
    pass
def correct_structure():
    pass

def convert_format():
    filenames = filelist_from_directory(settings.src_path,"*",FilelistFlags.not_include_empty)
    for filename in filenames:
        with io.open(settings.src_path + filename, 'r', encoding = 'utf-8') as f:
            soup = Soup(f.read())
            originals = soup.find_all(u'original')
            translates = soup.find_all(u'translate')
            txt_entry = zip(originals,translates)
            
            po_file = create_po_file([])
            for original,translate in txt_entry:
                #print("%s %s" % (original.getText(),translate.getText()))
                entry = polib.POEntry(
                    msgid=original.getText(),
                    msgstr=translate.getText()
                    )
                po_file.append(entry)
            po_file.save(settings.dst_path + filename + '.po')
        
def process():
    shutil.rmtree(settings.dst_path,ignore_errors=True)
    os.mkdir(settings.dst_path)
    correct_grammar_errors()
    correct_structure()
    convert_format()
    
if __name__ == "__main__":
    process()