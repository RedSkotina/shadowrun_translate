import polib
import sys

class settings:
    src_path = '../data/extract/pc/2.0.8/StreamingAssets/ContentPacks/berlin/data/loc/'
    dst_path = './berlin_ext_full.po'
    debug = True
def safeprint(s):
    try:
        print(s)
    except UnicodeEncodeError:
        print(s.encode('utf8').decode(sys.stdout.encoding))
        
            
def process():
    if settings.debug:
        print (u'make_full_po')
    pot_filename = 'berlin.pot'
    pot = polib.pofile(settings.src_path + pot_filename)
    po_filename = './zog/berlin.mo.po'
    po = polib.pofile(po_filename)
    
    for pot_entry in pot:
        po_entry = po.find(pot_entry.msgid)
        if po_entry is None:
            safeprint('S: %s' % (pot_entry.msgid))
        else:
            #print(po_entry.msgstr)
            pot_entry.msgstr = po_entry.msgstr
    #for pot_entry in pot:
    #    print(pot_entry.msgstr)
    pot.save(settings.dst_path)
    
process()    