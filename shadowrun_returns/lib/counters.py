# -*- coding: utf-8 -*-
total_replace_string_count = 0
local_replace_string_count = 0
count_list = []
count_dict={}

def create_counts():
    os.chdir("corrected")
    c = 0
    for filename in glob.glob("*"):
        t = 0
        f = codecs.open(filename, 'rb','utf-8')
        soup = Soup(f.read())
        for i in soup.find_all(u'original'):
            c = c + 1
            t = t + 1
        f.close()

        
    print ("Total count original strings from nota: %d" % c)
    global total_original_string_count
    total_original_string_count = c
    os.chdir("../")

def clear_replace_count():
    global local_replace_string_count,count_list
    local_replace_string_count = 0
    count_list = []
    
def add_replace_count(s):
    # check: count strings
    global local_replace_string_count, total_replace_string_count, count_list
    count_list.append(s)
    local_replace_string_count = local_replace_string_count + 1
    total_replace_string_count = total_replace_string_count + 1
    # end count    

def flush_replace_count(translate_filename):
    global count_dict
    if translate_filename in count_dict:
        count_dict[translate_filename].extend(count_list)
    else:
        count_dict[translate_filename] = count_list


    