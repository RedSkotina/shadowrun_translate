#http://my.opera.com/RockBomber/blog/show.dml/36916422
import sys
import urllib, urllib3
import http.cookies
import re
import polib
import time

class Site:
    debug = False
    conn = None
    headers = None
    cookie = None
    base_url = ''
    user = None
    password = None
    def __init__(self,url):
        self.base_url = url
    def connect(self):
        self.conn = urllib3.connectionpool.HTTPConnectionPool(self.base_url)
        payload = urllib.parse.urlencode({'login[login]': self.user, 'login[pass]': self.password }) 
        self.headers = urllib3.make_headers(user_agent='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.11) Gecko/20101012 Firefox/3.6.11')
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        r = self.conn.urlopen('POST', '/', body=payload, headers=self.headers, redirect=False)
        #if r.status != 200:
        #    print ("EROOR: Can't authorize %s  -  check user and password..." % self.base_url)
        #    sys.exit()
        self.cookie=http.cookies.SimpleCookie(r.headers['set-cookie'])
        self.headers['Cookie'] = self.cookie.output(attrs=[], header='').strip()
        if self.debug:
            print ("\tSuccessufull authorize %s" % self.base_url)
        time.sleep(1)
        
    def open_url(self, url, request_type='GET'):
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        redirect = True if request_type == 'GET' else False
        r = self.conn.urlopen(request_type, url, headers=self.headers, redirect=redirect)
        if ('set-cookie' in r.headers):
            self.cookie.load(r.headers['set-cookie'])
            self.headers['Cookie']=self.cookie.output(attrs=[], header='').strip()
        del self.headers['Content-Type']
        return r
        
    def open_ajax_url(self, url):
        self.headers['Content-Type'] = ''
        self.headers['X-Requested-With'] = 'XMLHttpRequest'
        r = self.conn.urlopen('POST', url, headers=self.headers, redirect=False)
        if ('set-cookie' in r.headers):
            self.cookie.load(r.headers['set-cookie'])
            self.headers['Cookie']=self.cookie.output(attrs=[], header='').strip()
        del self.headers['Content-Type']
        del self.headers['X-Requested-With'] 
        return r   
    def form_submit(self, url, form_data):
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        r = self.conn.urlopen('POST', url, body=form_data, headers=self.headers, redirect=False)
        if ('set-cookie' in r.headers):
            self.cookie.load(r.headers['set-cookie'])
            self.headers['Cookie']=self.cookie.output(attrs=[], header='').strip()
        del self.headers['Content-Type']
        return r