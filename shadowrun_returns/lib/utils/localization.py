from .potlib import *
from .fio import *
from .common import *
import difflib
import collections
import logging
logging.basicConfig(filename='translate.log',level=logging.DEBUG)

class Localization(dict):
    debug = False
    cutoff = 0.9
    use_close_match = False
    def __init__(self, *args):
        dict.__init__(self, args)
    def __getitem__(self, key):
        if self.use_close_match:
            key = self.get_close_match(key)
        try:
            val = dict.__getitem__(self, key)
        except KeyError as e:
            if key != '':
                key2 = self.get_close_match(key)
                safeprint("WARNING: cant localize")
                if key2 == key:
                    print("Dont have translation")
                    print("requested string")
                    safeprint(key)
                    logging.debug("dont have translation for: " + key)
                else:
                    print("requested string")
                    safeprint(key)
                    print(" ".join("%02x" %int(c) for c in key.encode('utf-8')))
                    print("most closed string")
                    safeprint(key2)
                    print(" ".join("%02x" %int(c) for c in key2.encode('utf-8')))
                    logging.debug("most close translation for: " + key)
                    logging.debug(" ".join("%02x" %int(c) for c in key.encode('utf-8')))
                    logging.debug("is " + key2)
                    logging.debug(" ".join("%02x" %int(c) for c in key2.encode('utf-8')))
                
            return key
        #log.info("GET %s['%s'] = %s" % str(dict.get(self, 'name_label')), str(key), str(val)))
        #TODO: correct work with poedit
        #if key.endswith('\n') and not val.endswith('\n'):
        #    val = val + '\n'
        return val
    def __setitem__(self, key, val):
        #log.info("SET %s['%s'] = %s" % str(dict.get(self, 'name_label')), str(key), str(val)))
        dict.__setitem__(self, key, val)
    def __contains__(self, item):
        r = dict.__contains__(item)
        return r
    def get_close_match(self, key):
        if self.debug:
            safeprint ("close string for '%s'" % key)
        close_matches = difflib.get_close_matches(key, dict.keys(self), cutoff = self.cutoff)
        close_match = close_matches[0] if close_matches != [] else key
        return close_match
        
def locate_localization_files(target_dir, target_name, use_regex = True):
    filenames = filelist_from_directory(target_dir, r"*", FilelistFlags.not_include_empty)
    r_filenames = []
    for filename in filenames:
        if use_regex:
            c = re.compile(target_name, re.UNICODE)
            m = c.match(filename)
            if m != None:
                r_filenames.append(filename)
                continue
        else:
            if filename == target_name:
                r_filenames.append(filename)
                continue
    return r_filenames
    
def parse_localization_files(dir, filenames):
    loc = Localization()
    for filename in filenames:
        po = polib.pofile(os.path.join(dir, filename))
        for entry in po:
            loc[entry.msgid] = entry.msgstr
    return loc        
__loc_cache__ = {}    
def get_localization(dir, file_pattern):
    global __loc_cache__
    if file_pattern not in __loc_cache__:
        filenames = locate_localization_files(dir, file_pattern, use_regex = True)
        loc = parse_localization_files(dir, filenames)
        __loc_cache__[file_pattern] = loc
    else:
        loc = __loc_cache__[file_pattern]
    return loc
    
############################################
from collections import MutableMapping 
from collections import defaultdict

class LocalizationCatalogEntry():
    msgid = ''
    comment = ''
class LocalizationEntryList(list):
    allow_empty_value = False
    def __init__(self, *args):
        list.__init__(self, *args)
    def __iadd__ (self, value):
        if isinstance(value, LocalizationCatalogEntry):
             if self.allow_empty_value or value.msgid != '':
                list.append(self,value)
             else:
                raise ValueError("msgid == ''")
        elif isinstance(value, str):
            entry = LocalizationCatalogEntry()
            if self.allow_empty_value or value != '':
                entry.msgid = value
                list.append(self, entry)
        else:
            raise ValueError("Not LocalizationCatalogEntry or string")
        return self
        
class LocalizationCatalog(MutableMapping):
    store = None
    context = None
    
    def __init__(self, *args, **kwargs):
        self.store = defaultdict(LocalizationEntryList)
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        if isinstance(value, LocalizationEntryList):
            self.store[key] = value
        else:
            raise TypeError("Allowed only EntryList")
            
    def __delitem__(self, key):
        del self.store[key]
        
    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)
    
    def save(self, filename):
        po = create_po()
        for key in self:
            for e in self[key]:
                entry = polib.POEntry(
                    msgid = e.msgid,
                    msgstr = u'',
                    msgctxt = key,
                    comment = e.comment)
                po.append(entry)
        assure_path_exists(filename)
        po.make_unique()
        po.save(filename)