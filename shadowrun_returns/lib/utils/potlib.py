import polib

import re
import os


class MultiPOFile:
    po = None
    preserve_order = True
    def __init__(self):
        self.po = create_po()
    def add_patch(self, dir, fname):
        po_patch = polib.pofile(os.path.join(dir, fname))
        for po_patch_entry in po_patch:
            is_entry_patched = False
            for po_entry in self.po:
                if po_entry.msgid == po_patch_entry.msgid:
                    po_entry.msgid = po_patch_entry.msgid
                    #TODO: correct work with poedit
                    '''if po_patch_entry.msgid.endswith('\n') and not po_patch_entry.msgstr.endswith('\n'):
                        po_patch_entry.msgstr = po_patch_entry.msgstr + '\n'
                    if not po_patch_entry.msgid.endswith('\n') and po_patch_entry.msgstr.endswith('\n'):
                        po_patch_entry.msgstr = po_patch_entry.msgstr[:-1]
                    '''
                    po_entry.msgstr = po_patch_entry.msgstr
                    is_entry_patched = True    
            if not is_entry_patched:
                #TODO: correct work with poedit when both strings have differrent end '\n'
                '''if po_patch_entry.msgid.endswith('\n') and not po_patch_entry.msgstr.endswith('\n'):
                    po_patch_entry.msgstr = po_patch_entry.msgstr + '\n'
                if not po_patch_entry.msgid.endswith('\n') and po_patch_entry.msgstr.endswith('\n'):
                    po_patch_entry.msgstr = po_patch_entry.msgstr[:-1]
                '''
                self.po.append(po_patch_entry)
    def __sub__(self, other):
        self_entries = self.po[:]
        other_entries = other.po[:]
        sub_entries = []
        #we need compare only msgid
        original_hash_fn = polib.POEntry.__hash__
        polib.POEntry.__hash__ = lambda x: hash(x.msgid)
        if self.preserve_order:
            sub_entries = [ x for x in self_entries if not (x in other_entries)]
        else:    
            sub_entries = set(self_entries) - set(other_entries)
        
        polib.POEntry.__hash__ = original_hash_fn
        
        multi_po_sub = MultiPOFile()
        multi_po_sub.po[:] = list(sub_entries)
        return multi_po_sub
        
    def make_unique(self, preserve_order = True):
        self.po.make_unique(preserve_order)
    def save(self, filename):
        self.po.save(filename)
    def save_as_mofile(self, filename):
        self.po.save_as_mofile(filename) 
        
def encode_number(number, start_codepoint = 1000, range_size = 1000):
    m = int(number/range_size)
    d = number%range_size
    # todo: here is error if number >~ billion .
    c1 = chr(start_codepoint + m)
    c2 = chr(start_codepoint + d)
    s = c1 + c2 if c1 != 0 else c2 
    return s
def cmp_version(key):
    m = re.search(r'(\d+(?:\.\d+)*)',key)
    if m != None:
        i = 0
        for g in m.groups():
            if g != None:
                vd = key[m.start(i):m.end(i)].split('.')
                vs = []
                for number in vd:
                    d = int(number)
                    s = encode_number(d)
                    vs.append(s)
                v  = "+".join(vs)
                key = key[:m.start(i)] + v + key[m.end(i):]
            i = i + 1
    key = re.sub(r'\.','\x1c',key)
    key = re.sub(r'-','\x1d',key)    
    key = re.sub(r' ','\x1e',key)
    key = re.sub(r'_','\x1f',key)
    #safeprint (key)
    return key
    
    
#####################################################

def create_po():
    po = polib.POFile(check_for_duplicates = False)
    po.metadata = {
    'Project-Id-Version': 'shadowrun_ru',
    'Report-Msgid-Bugs-To': 'support@zoneofgames.ru',
    'Last-Translator': 'ZOG <support@zoneofgames.ru>',
    'Language-Team': 'ZOG <support@zoneofgames.ru>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=UTF-8',
    'Content-Transfer-Encoding': '8bit',
    'Language': 'ru'
    }
    return po
    
def po_append(po, original_list):
    for original_str in original_list:
        entry = polib.POEntry(
            msgid = original_str,
            msgstr = u'' )
        po.append(entry) 
        
def create_po_file(original_list):
    po = polib.POFile(check_for_duplicates = False)
    po.metadata = {
    'Project-Id-Version': 'shadowrun_ru',
    'Report-Msgid-Bugs-To': 'support@zoneofgames.ru',
    'Last-Translator': 'ZOG <support@zoneofgames.ru>',
    'Language-Team': 'ZOG <support@zoneofgames.ru>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=UTF-8',
    'Content-Transfer-Encoding': '8bit',
    'Language': 'ru'
    }
    for original_str in original_list:
        entry = polib.POEntry(
            msgid = original_str,
            msgstr = u'' )
        po.append(entry)
        
    return po

def create_mo_file(original_list):
    mo = polib.MOFile(check_for_duplicates = False)
    mo.metadata = {
    'Project-Id-Version': 'Shadowrun RU',
    'Report-Msgid-Bugs-To': '',
    'POT-Creation-Date': '2014-01-07 16:59-0800',
    'PO-Revision-Date': '2014-03-27 11:44-0800',
    'Last-Translator': 'Mikhail Shvyryev <shvyryev@yandex.ru>',
    'Language-Team': 'RU <shvyryev@yandex.ru>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=UTF-8',
    'Content-Transfer-Encoding': '8bit',
    'X-Generator': 'Poedit 1.6.4'

    }
    for original_str in original_list:
        entry = polib.MOEntry(
            msgid = original_str,
            msgstr = u'' )
        mo.append(entry)
        
    return mo
