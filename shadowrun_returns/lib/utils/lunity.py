import os, io
import ctypes
import struct
import tempfile
class tOpenArchiveData(ctypes.Structure):
  _fields_ = [
                ("ArcName", ctypes.c_char_p), 
                ("OpenMode", ctypes.c_int), 
                ("OpenResult", ctypes.c_int), 
                ("CmtBuf", ctypes.c_char_p), 
                ("CmtBufSize", ctypes.c_int), 
                ("CmtSize", ctypes.c_int), 
                ("CmtState", ctypes.c_int)
             ]
class tHeaderData(ctypes.Structure):
  _fields_ = [
                ("ArcName", ctypes.c_char * 260), 
                ("FileName", ctypes.c_char * 260), 
                ("OpenMode", ctypes.c_int), 
                ("Flags", ctypes.c_int), 
                ("PackSize", ctypes.c_int), 
                ("UnpSize", ctypes.c_int), 
                ("HostOS", ctypes.c_int), 
                ("FileCRC", ctypes.c_int),
                ("FileTime", ctypes.c_int),
                ("UnpVer", ctypes.c_int),
                ("Method", ctypes.c_int),
                ("FileAttr", ctypes.c_int),
                ("CmtBuf", ctypes.c_char_p),
                ("CmtBufSize", ctypes.c_int),
                ("CmtSize", ctypes.c_int),
                ("CmtState", ctypes.c_int)
             ]
class unity_index_entry:
    fid      = None
    filename = ""
    type     = ""
    size     = 0
    
class UnityDll:
    lib = None
    state = None
    def __init__(self, path, filename):
        pathToWin32Environment = os.getcwd() + path
        pathToDll = pathToWin32Environment + filename
        if not os.path.exists(pathToDll):
            #Give up if none of the above succeeded:
            raise Exception('Could not locate ' + pathToDll)
        curr_dir_before = os.getcwd()
        os.chdir(pathToWin32Environment)
        self.lib = ctypes.WinDLL(filename)
        os.chdir(curr_dir_before)
    def OpenArchive(self, filename):
        self.lib.OpenArchive.argtypes = [ctypes.POINTER(tOpenArchiveData)]
        self.lib.OpenArchive.restype = ctypes.c_void_p #internal structure
        arg = tOpenArchiveData()
        arg.ArcName = ctypes.cast(ctypes.create_string_buffer(filename.encode('cp1251')) , ctypes.c_char_p)
        self.state = self.lib.OpenArchive(ctypes.byref(arg))
    def CloseArchive(self):
        self.lib.OpenArchive.argtypes = [ctypes.c_void_p]
        self.lib.OpenArchive.restype = ctypes.c_int
        self.lib.CloseArchive(self.state)    
    def ReadHeader(self):
        self.lib.ReadHeader.argtypes = [ctypes.c_void_p, ctypes.POINTER(tHeaderData)]
        self.lib.ReadHeader.restype = ctypes.c_int
        header = tHeaderData()
        res = self.lib.ReadHeader(self.state, ctypes.pointer(header))
        return res, header
    def ProcessFile(self, dst_path, dst_filename):
        self.lib.ProcessFile.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_char_p, ctypes.c_char_p]
        self.lib.ProcessFile.restype = ctypes.c_int
        PK_EXTRACT = 2
        path = ctypes.cast(ctypes.create_string_buffer(dst_path.encode('cp1251')) , ctypes.c_char_p)
        filename = ctypes.cast(ctypes.create_string_buffer(dst_filename.encode('cp1251')) , ctypes.c_char_p)
        res = self.lib.ProcessFile(self.state, PK_EXTRACT, path, filename)
        return res
    def PackFiles(self, packed_file, sub_path, src_path, add_list, flags):
        self.lib.PackFiles.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_int]
        self.lib.PackFiles.restype = ctypes.c_int
        packed_file = ctypes.cast(ctypes.create_string_buffer(packed_file.encode('cp1251')) , ctypes.c_char_p)
        sub_path = ctypes.cast(ctypes.create_string_buffer(sub_path.encode('cp1251')) , ctypes.c_char_p) if sub_path != None else ctypes.POINTER(ctypes.c_char)()
        src_path = ctypes.cast(ctypes.create_string_buffer(src_path.encode('cp1251')) , ctypes.c_char_p)
        add_list = ctypes.cast(ctypes.create_string_buffer(add_list.encode('cp1251')) , ctypes.c_char_p)
        res = self.lib.PackFiles(packed_file, sub_path, src_path, add_list, flags)
        return res
    def get_index(self):
        unity_index = {}
        E_END_ARCHIVE = 10
        AT_DIR = 2
        i = 1
        res = 0
        while (res != E_END_ARCHIVE):
            res, header = self.ReadHeader()
            if (res != E_END_ARCHIVE):
                if (header.FileAttr == AT_DIR):
                    #print ("DIR: %s" % ( header.FileName.decode('cp1251')))
                    continue
                else:
                    #print ("%i: %s" % (i, header.FileName.decode('cp1251')))
                    e = unity_index_entry()
                    e.fid = i
                    e.filename = header.FileName.decode('cp1251')
                    e.type = header.FileName.decode('cp1251').split('.')[-1]
                    e.size = header.UnpSize
                    unity_index[i] = e
            i = i + 1
        return unity_index
    def extract(self, fid, dst):
        splited = dst.split('/')
        dst_filename = splited[-1]
        dst_path = '/'.join(splited[:-1])+'/' if len(splited)>1 else ""
        E_END_ARCHIVE = 10
        i = 1
        res = 0
        res2 = -1
        while (res != E_END_ARCHIVE and res2 != 0):
            res, header = self.ReadHeader()
            #filename = header.FileName.decode('cp1251')
            if (i == fid):
                #print(dst_path, dst_filename)
                res2 = self.ProcessFile(dst_path, dst_filename)
            i = i + 1
    def multi_extract(self, entries, dst_path = ""):
        fids = [ entry.fid for entry in entries]
        E_END_ARCHIVE = 10
        i = 1
        res = 0
        res2 = -1
        entries_count = len(entries)
        while (res != E_END_ARCHIVE and (res2 != 0 or entries_count != 0)):
            res, header = self.ReadHeader()
            #filename = header.FileName.decode('cp1251')
            if (i in fids):
                entries_count -= 1
                dst_filename = entries[entries_count].filename
                print(dst_path, dst_filename)
                assure_path_exists(dst_path  + dst_filename)
                res2 = self.ProcessFile(dst_path, dst_filename)
                
            i = i + 1
         
    def pack(self, assets_filename, dst_textasset_filename, textasset_file):
        splited = textasset_file.split('\\')
        src_filename = splited[-1]
        src_path = '/'.join(splited[:-1])+'/' if len(splited)>1 else ""
        dst_textasset_filename = dst_textasset_filename + '\0'
        res = self.PackFiles(assets_filename, None, src_path, dst_textasset_filename, 0)
        if res != 0:
            print('unity.PackFiles return %d' % res)
        return res 
def assure_path_exists(path):
    dir = os.path.dirname(path)
    if dir != "" and not os.path.exists(dir):
        os.makedirs(dir)    
                
def extract_fs_dict_file(filename):
    print("\textract dictionary of resource names...")
    fs_dict_file = ""
    unity = UnityDll("/lib/unity/", "unity.dll")
    unity.OpenArchive(filename)
    unity_index = unity.get_index()
    unity.CloseArchive()
    fid = -1
    for key,value in unity_index.items():
        if value.type == '147':
            fid = key
            break
    if fid != -1:
        #assure_path_exists(settings.dst_path  + unity_index[fid].filename)
        fs_dict_file = os.path.join(tempfile.gettempdir(), unity_index[fid].filename)
        print("\tto %s" % fs_dict_file)
        unity.OpenArchive(filename)
        res = unity.extract(fid, fs_dict_file )
        unity.CloseArchive()
    else:
        print('\tcant find asset with type 147 in "%s"' % filename)
        sys.exit()
    return fs_dict_file  
    
def search_localization_resource(filename):
    print("\tsearch localizations...")
    loc_resources = {}
    with io.open(filename , "rb") as f:
        buffer = bytearray()
        buffer.extend(f.read())
        cur_ptr = 0x0
        entry_num = buffer[0x0:0x4]
        entry_num = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        for i in range(entry_num):
            entry_name_size = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_name = struct.unpack(str(entry_name_size)+"s",buffer[cur_ptr:cur_ptr+entry_name_size])[0].decode('utf-8')
            cur_ptr = cur_ptr + entry_name_size
            entry_name_padding_size = 4 - (entry_name_size % 4) if (entry_name_size % 4) != 0 else 0
            #skip padding
            cur_ptr = cur_ptr + entry_name_padding_size
            entry_page = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_fid  = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            #print(entry_name)
            if entry_name.startswith('loc'):
                loc_resources[entry_name] = entry_fid
    return loc_resources
    
def extract_loc_resource(assets_filename, fid):
    print("\textract localization TextAsset...")
    loc_resource = ""
    unity = UnityDll("/lib/unity/", "unity.dll")
    unity.OpenArchive(assets_filename)
    unity_index = unity.get_index()
    unity.CloseArchive()
    print("\tfid: %d"%fid)
    #assure_path_exists(settings.dst_path  + unity_index[fid].filename)
    loc_resource = os.path.join(tempfile.gettempdir(), unity_index[fid].filename)
    print("\tto %s" % loc_resource)
    unity.OpenArchive(assets_filename)
    res = unity.extract(fid, loc_resource)
    unity.CloseArchive()
    return loc_resource 
    
def search_resource(filename, search_pattern):
    print("\tsearch localizations...")
    loc_resources = {}
    with io.open(filename , "rb") as f:
        buffer = bytearray()
        buffer.extend(f.read())
        cur_ptr = 0x0
        entry_num = buffer[0x0:0x4]
        entry_num = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        for i in range(entry_num):
            entry_name_size = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_name = struct.unpack(str(entry_name_size)+"s",buffer[cur_ptr:cur_ptr+entry_name_size])[0].decode('utf-8')
            cur_ptr = cur_ptr + entry_name_size
            entry_name_padding_size = 4 - (entry_name_size % 4) if (entry_name_size % 4) != 0 else 0
            #skip padding
            cur_ptr = cur_ptr + entry_name_padding_size
            entry_page = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_fid  = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            #print(entry_name)
            if entry_name.startswith(search_pattern):
                loc_resources[entry_name] = entry_fid
    return loc_resources
    
def search_font_sharedassets(filename):
    print("\tsearch localizations...")
    loc_resources = []
    with io.open(filename , "rb") as f:
        buffer = bytearray()
        buffer.extend(f.read())
        cur_ptr = 0x0
        entry_num = buffer[0x0:0x4]
        entry_num = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        for i in range(entry_num):
            entry_name_size = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_name = struct.unpack(str(entry_name_size)+"s",buffer[cur_ptr:cur_ptr+entry_name_size])[0].decode('utf-8')
            cur_ptr = cur_ptr + entry_name_size
            entry_name_padding_size = 4 - (entry_name_size % 4) if (entry_name_size % 4) != 0 else 0
            #skip padding
            cur_ptr = cur_ptr + entry_name_padding_size
            entry_page = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            entry_fid  = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
            cur_ptr = cur_ptr + 0x4
            #print(entry_name)
            if entry_name.startswith('fonts'):
                entry = unity_index_entry()
                entry.fid = entry_fid
                entry.filename = entry_name
                loc_resources.append(entry)
                
    return loc_resources
    
def extract_font_sharedassets(assets_filename, entries, dst = None):
    print("\textract Font Descriptors...")
    loc_resource = ""
    unity = UnityDll("/lib/unity/", "unity.dll")
    #unity.OpenArchive(assets_filename)
    #unity_index = unity.get_index()
    #unity.CloseArchive()
    #print("\tfid: %d"%fid)
    
    #print(unity_index)
    #assure_path_exists(settings.dst_path  + unity_index[fid].filename)
    dst = tempfile.gettempdir() if dst is None else dst
    #loc_resource = os.path.join(dst, unity_index[fid].filename)
    #print("\tto %s" % loc_resource)
    unity.OpenArchive(assets_filename)
    res = unity.multi_extract(entries, dst)
    unity.CloseArchive()
    return entries      