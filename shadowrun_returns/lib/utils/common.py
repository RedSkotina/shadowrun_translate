import sys
def safeprint(s):
    try:
        print(s)
    except UnicodeEncodeError:
        print(s.encode('utf8').decode(sys.stdout.encoding))
#--------------------------------
class classproperty(object):
    def __init__(self, f):
        self.f = f
    def __get__(self, obj, owner):
        return self.f(owner)
        
def ILmultistring_to_string(il_multistring):
# sample il_multistring: 
#    "Pinning Hit: Blades can pin the target, rendering "
#    + "them unable to move for 2 turns. HP DMG -2, Armor Piercing +4. Cooldown"
#    + ": 2" 
    
    is_string = False
    is_escape = False
    str = ""
    for i,s in enumerate(il_multistring):
        if s == "\"":
            is_string = not is_string # invert
            continue
        if is_escape:
            if s == 'n':
                str += '\n'
            else:
                str += s
            is_escape = False
            continue    
        if s == "\\" : # skip other lead escape symbol
            is_escape = True
            continue
        if is_string:
            str += s
    return str

def string_to_ILmultistring(str):
    return "\"" + str + "\""