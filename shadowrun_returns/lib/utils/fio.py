import os
import sys
import errno
import fnmatch
import io
import shutil

def get_path(filename):
    filename = os.path.normpath(filename)
    head, tail = os.path.split(filename)
    return head
def get_filename_without_path(filename):
    filename = os.path.normpath(filename)
    filename_list = filename.split('\\')
    return filename_list[-1]
def get_name_without_extension(filename):
    filename_list = filename.split('.')
    return filename_list[0]
    
def silent_remove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured

def assure_path_exists(path):
        dir = os.path.dirname(path)
        if dir != "" and not os.path.exists(dir):
                os.makedirs(dir)    
class FilelistFlags:
    include_empty = 0
    not_include_empty = 1

    
def filelist_from_directory(dir_path, pattern = r'*', flags = FilelistFlags.include_empty, ignore=r''):
    #import os
    #print(os.getcwd())
    from os.path import isfile, join, getsize
    names = [ f for f in os.listdir(dir_path) if isfile(join(dir_path,f)) and (flags&FilelistFlags.include_empty or getsize(join(dir_path,f))>0)]
    obscured_names = []
    for name in names:
        if not fnmatch.fnmatchcase(name, ignore):
            obscured_names.append(name)
    
    
    return fnmatch.filter(obscured_names, pattern)

def traverse_directory(root_dir, pattern = '*', ignore_dir = '', func = None):
    for dir, subdir_list, file_list in os.walk(root_dir, topdown=False):
        if fnmatch.fnmatch(dir, ignore_dir):
            continue
        matched_files = fnmatch.filter(file_list, pattern)
        #print('Found directory: %s' % dir)
        for fname in matched_files:
            #if settings.debug:
            print('\t%s' % fname)
            func(dir, fname)
            
def copytree(src, dst, symlinks=False, ignore=None, copy_function=shutil.copy2,
             ignore_dangling_symlinks=False):
    """Recursively copy a directory tree and overwrite all

    """
    dst = os.path.normpath(dst)
    src = os.path.normpath(src)
    names = os.listdir(src)
    if ignore is not None:
        ignored_names = ignore(src, names)
    else:
        ignored_names = set()

    assure_path_exists(dst)
    errors = []
    for name in names:
        if name in ignored_names:
            continue
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if os.path.islink(srcname):
                linkto = os.readlink(srcname)
                if symlinks:
                    # We can't just leave it to `copy_function` because legacy
                    # code with a custom `copy_function` may rely on copytree
                    # doing the right thing.
                    os.symlink(linkto, dstname)
                    shutil.copystat(srcname, dstname, follow_symlinks=not symlinks)
                else:
                    # ignore dangling symlink if the flag is on
                    if not os.path.exists(linkto) and ignore_dangling_symlinks:
                        continue
                    # otherwise let the copy occurs. copy2 will raise an error
                    copy_function(srcname, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks, ignore, copy_function)
            else:
                # Will raise a SpecialFileError for unsupported file types
                copy_function(srcname, dstname)
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except shutil.Error as err:
            errors.extend(err.args[0])
        except OSError as why:
            errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except OSError as why:
        # Copying file access times may fail on Windows
        if why.winerror is None:
            errors.append((src, dst, str(why)))
    if errors:
        raise shutil.Error(errors)
    return dst
###########################################################            
def parse_cp1251_text_file(filename):
    with io.open(filename, 'r', encoding = 'cp1251') as f:
        data = f.read()
    return data
def write_cp1251_text_file(filename,data):
    assure_path_exists(filename)
    with io.open(filename, 'wb',encoding = 'cp1251') as f:
        f.write(data)
def parse_utf8_text_file(filename):
    with io.open(filename, 'r', encoding = 'utf-8') as f:
        data = f.read()
    return data
def write_utf8_text_file(filename,data):
    assure_path_exists(filename)
    with io.open(filename, 'w', encoding = 'utf-8') as f:
        f.write(data)
def parse_utf16_text_file(filename):
    with io.open(filename, 'r', encoding = 'utf-16') as f:
        data = f.read()
    return data
def write_utf16_text_file(filename,data):
    assure_path_exists(filename)
    with io.open(filename, 'wb','utf-16') as f:
        f.write(data)
    
def parse_text_file(filename):
    #TODO:  autodetect library: chardet or UnicodeDammit
    try:
        return parse_utf8_text_file(filename)
    except UnicodeDecodeError:
        print ("encoding utf-8 failed. trying cp1251")
        return parse_cp1251_text_file(filename)
        
def parse_protobuf_file(filename, storage):
    f = open(filename, "rb")
    storage.ParseFromString(f.read())
    f.close()
    return storage

def write_protobuf_file(filename, storage):
    assure_path_exists(filename)
    with io.open(filename, "wb") as f:
        f.write(storage.SerializeToString())
    
def parse_bin_file(filename):
    f = open(filename, "rb")
    data = bytearray()
    data.extend(f.read())
    f.close()
    return data
#################################    
import struct
def get_bin_text(data, offset):
    text_length = data[offset:offset + 0x4]
    text_length = int(struct.unpack("I",str(text_length))[0])
    text = data[offset + 0x4: offset + 0x4 + text_length ].decode('cp1251')
    return text
def mod_addition(number, modulo):
    addition = modulo - int(number % modulo)
    addition = addition % modulo
    return addition
def set_bin_text(data, offset, text):
    old_text_length = data[offset:offset + 0x4]
    old_text_length = int(struct.unpack("I",str(old_text_length))[0])
    text_length = len(bytearray(text,'utf-8'))
    text_bin_length = struct.pack("I",text_length)
    print (mod_addition(old_text_length,4))
    print (mod_addition(text_length,4))
    for i in range(mod_addition(text_length,4)):
        text += "\0"
    data[offset + 0x4: offset + 0x4 + old_text_length + mod_addition(old_text_length,4)] = bytearray(text ,'utf-8')
    data[offset:offset + 0x4] = text_bin_length
def write_bin_file(filename, byte_array):
    f = open(filename, "wb")
    f.write(byte_array)
    f.close()