search_result = []
def search_display_text_predicate(op):
    search_result = []
    if (op.functionName.lower() == u'Display Text over Actor'.lower() or
       op.functionName.lower() == u'Display Text at Screen Position'.lower() or
       op.functionName.lower() == u'Display Text over Prop'.lower() or
       op.functionName.lower() == u'Display Text over Point'.lower() or
       op.functionName.lower() == u'Open Hiring Screen and Open Scene'.lower() ) :
        #if len(op.args) == 10:
        for arg in op.args:
            #if op.args[2].HasField('string_value'):
            #    search_result.append(op.args[2])
            if arg.HasField('string_value') and arg.string_value != "":
                search_result.append( arg )
            
    return search_result

def search_set_var_string_synopsis_predicate(op):
    search_result = []
    if (op.functionName.lower() == u'Set Variable (string)'.lower() ) :
        for arg in op.args:
            if arg.HasField('call_value') and arg.call_value.functionName == "Get Story Variable (string)":
                for call_arg in arg.call_value.args:
                    if call_arg.HasField('string_value') and call_arg.string_value == 'Global_HavenLoadingScreen':
                        search_result.append(op)
            
    return search_result

def search_set_var_string_redorgreen_predicate(op):
    search_result = []
    if (op.functionName.lower() == u'Set Variable (string)'.lower() ) :
        for arg in op.args:
            if arg.HasField('call_value') and arg.call_value.functionName == "Get Story Variable (string)":
                for call_arg in arg.call_value.args:
                    if call_arg.HasField('string_value') and call_arg.string_value == 'str_RedOrGreen':
                        search_result.append(op)
            
    return search_result

def search_set_screen_label_predicate(op):
    search_result = []
    if (op.functionName.lower() == u'Set Screen Label'.lower()):
        if op.args[1].HasField('string_value'):
            search_result.append(op.args[1])
        if op.args[1].HasField('call_value'):
            if op.args[1].call_value.functionName == "Append (string)":
                for arg in op.args[1].call_value.args:
                    if arg.HasField('string_value'):
                        search_result.append(arg)
                    if arg.HasField('call_value'):
                        if arg.call_value.functionName == "Append (string)":
                            for arg2 in arg.call_value.args:
                                if arg2.HasField('string_value'):
                                    search_result.append(arg2)
                        
                        
    if (op.functionName.lower() == u'Set Screen Label Progress Bar (int)'.lower()) :
        if op.args[1].HasField('string_value'):
            search_result.append(op.args[1])
        if op.args[1].HasField('call_value'):
            if op.args[1].call_value.functionName == "Append (string)":
                for arg in op.args[1].call_value.args:
                    if arg.HasField('string_value'):
                        search_result.append(arg)
                    if arg.HasField('call_value'):
                        if arg.call_value.functionName == "Append (string)":
                            for arg2 in arg.call_value.args:
                                if arg2.HasField('string_value'):
                                    search_result.append(arg2)
             
    return search_result
    
def search_display_text_in_popup_predicate(op):
    search_result = []
    if (op.functionName.lower() == u'Display Text In Popup'.lower() ) :
        for arg in op.args:
            if arg.HasField('string_value') and arg.string_value != "":
                search_result.append(arg)
            
    return search_result

def search_function_name_predicate(op):
    search_result = []
    if op.functionName.lower() == u'Get Map Item (Goal)'.lower():
        if len(op.args) == 1:
            if op.args[0].HasField('string_value'):
                search_result.append( op.args[0] )
    return search_result

def search_fn_set_string_specific_predicate(op, var_name = ''):
    search_result = []
    if op.functionName.lower() == u'Set variable (string)'.lower():
        if op.args[0].HasField('call_value'):
            if op.args[0].call_value.functionName == "Get Variable (string)":
                if op.args[0].call_value.args[0].HasField('string_value'):
                    if op.args[0].call_value.args[0].string_value == var_name :
                        # it is target var
                        if op.args[1].HasField('string_value'):
                            search_result.append( op.args[1] )
               
    return search_result   

def search_fn_comparison_string_specific_predicate(op, var_name = ''):
    search_result = []
    if op.functionName.lower() == u'Comparison (string)'.lower():
        if op.args[0].HasField('call_value'):
            if op.args[0].call_value.functionName == "Get Variable (string)":
                if op.args[0].call_value.args[0].HasField('string_value'):
                    if op.args[0].call_value.args[0].string_value == var_name :
                        # it is target var
                        if op.args[2].HasField('string_value'):
                            search_result.append( op.args[2] )
               
    return search_result
    
def search_fn_set_string_predicate(op):
    search_result = []
    if op.functionName.lower() == u'Set variable (string)'.lower():
        if len(op.args) == 2:
            if op.args[1].HasField('string_value'):
                #print op.args[1]
                search_result.append( op.args[1] )
    return search_result   
def walkTsVariant(args, predicate_function):
    search_result = []
    for arg in args:
        result = walkTsCall([arg.call_value],predicate_function)
        search_result.extend(result)    
    return search_result
def walkTsCall(ops, predicate_function):
    search_result = []
    for op in ops:
        result = predicate_function(op)
        if result != None:
            search_result.extend(result)
        result = walkTsVariant(op.args, predicate_function)
        search_result.extend(result)
    return search_result
