#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import polib

class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''

def compare(po_new,po_old):
    added_po = create_po()
    added_po[:] = list(set(po_new.po[:]) - set(po_old.po[:]))
    return added_po
    
def load_multi_po(root_dir, component):
    po = MultiPOFile()
    if not os.path.isdir(root_dir):
        return po
    filenames = filelist_from_directory(root_dir, component)
    filenames.sort(key=cmp_version)
    for filename in filenames:
        if settings.debug:
            print (u'\tfilename: "%s"' % filename)
        po.add_patch(root_dir, filename)
    po.make_unique()
    return po
    
def compare_patch_strings(fetch_dir, extract_dir, out_dir):
    components = {  'berlin':'pc/2.0.9/StreamingAssets/ContentPacks/berlin/data/loc',
                    'seattle':'pc/2.0.9/StreamingAssets/ContentPacks/seattle/data',
                    'dragonfallextended':'pc/2.0.9/StreamingAssets/ContentPacks/DragonfallExtended/data/loc',
                    'interface': 'pc/2.0.9/resources'
                 }
    for component,component_path in components.items():
        print (u'\tcomponent: "%s"' % component)
        print (u'\tcomponent_path: "%s"' % component_path)
        po_fetch = load_multi_po(fetch_dir, component + "*.po")
        po_extract = load_multi_po(extract_dir + '/' + component_path,  component + "*.pot")
        po_out = po_extract - po_fetch
        out_file = out_dir + '/' + component + '.po'
        assure_path_exists(out_file)
        po_out.save(out_file)

def process():
    if settings.debug:
        print (u'compare_patch_strings')
    compare_patch_strings(settings.src_path, settings.extract_path, settings.dst_path)