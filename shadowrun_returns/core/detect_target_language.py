#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
import ctypes
import struct
import tempfile

class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''
    
def process():
    if settings.debug:
        print (u'detect_target_language')
    #settings.auto_path = True # Autogenerate path
    target_language = settings.language
    main_assets_filename = settings.src_path + "mainData"
    
    if os.path.exists(main_assets_filename):
        fs_dict_file = extract_fs_dict_file(main_assets_filename)
        loc_resources = search_localization_resource(fs_dict_file)
        loc_resource_fid = None
        print('\tdetected localizations: %s' % loc_resources)
        is_default_exist = False
        for key in loc_resources.keys():
            if key.startswith('loc/'+target_language):
                loc_resource_fid = loc_resources[key]
            if key.startswith('loc/'+settings.default_language):
                is_default_exist = True
        
        if  loc_resource_fid is None:
            print('\tcant detect localization: %s' % target_language)
            if is_default_exist:
                print('\tusing default localization: %s' % settings.default_language)
                target_language = settings.default_language
            else:
                print('\tcant detect default localization: %s' % settings.default_language)
                sys.exit()
        print('\ttarget_language: %s' % target_language)
        return  target_language       
    else:
        print('\tWarning: Cant find maindata file %s' % main_assets_filename)
        print('\tProbably localization cant be imported in game')
        print('\tusing localization: %s' % settings.language)
        target_language = settings.language
        return target_language
        