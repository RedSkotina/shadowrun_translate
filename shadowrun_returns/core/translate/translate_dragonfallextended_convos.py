# -*- coding: utf-8 -*-

from translator.utils import *
from translator.counters import *
import re
import sys
#sys.path.insert(0, './proto')
import convos_pb2
import scenes_pb2
class settings:
    src_path = './data/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/convos/'
    dst_path = './out/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/convos/'
    translate_path = './corrected/'
    debug = False

    
def replace_convo(convo, original_list, translate_list):
    replaced = False
    #bug with passwords
    for node in convo.nodes:
        if node.nodeType == node.ConversationNodeType_InputKeyboard:
            for branch in node.branches:
                #if len(node.branches[:])>1 and node.branches[:].index(branch) == (len(node.branches[:]) - 1):
                #   print "*Last branch dont must translate"
                #   break 
                if branch.inputBypass :
                    continue
                if not branch.HasField("responseText"):
                    continue
                tr = translate(branch.responseText, original_list, translate_list)
                if tr != "":
                    branch.responseText = tr
                    try:
                        print('pass : %s ' % (branch.responseText) )
                    except:
                        pass
                    replaced = True
    # bug with str_RedOrGreen
    for root in convo.roots:
        from functools import partial
        search_result = walkTsCall(root.conditions.ops, partial(search_fn_comparison_string_specific_predicate, var_name = 'str_RedOrGreen'))
        for arg in search_result:
            if arg.HasField('string_value'):
                tr = translate(arg.string_value, original_list, translate_list, unique = False)
                if tr != "":
                    arg.string_value = tr
                    replaced = True

    return replaced
    
def translate_convo(convo_filename):
    if settings.debug:
        print (u'convo_filename: "%s"' % convo_filename)
    convo = parse_protobuf_file(settings.src_path + convo_filename, convos_pb2.convo())
    #if settings.debug:
    #    print (u'convo.ui_name: "%s"' % convo.ui_name)
    
    translate_filenames = find_translate_file("DragonfallExtended|berlin_campaign", settings.translate_path , regex = True, all = True)
    if settings.debug:
        print (u'translate_filenames: "%s" ' % (translate_filenames) )
    original_list = []
    translate_list = []
    for translate_filename in translate_filenames:
        orig, trans = parse_translate_file(translate_filename, settings.translate_path)
        original_list.extend(orig)
        translate_list.extend(trans)
    
    replaced = replace_convo(convo, original_list, translate_list)
    if replaced:
        assure_path_exists(settings.dst_path + convo_filename)
        write_protobuf_file(settings.dst_path + convo_filename, convo)
    
    
def process():
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        translate_convo(filename)
    
if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    process()