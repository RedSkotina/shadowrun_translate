#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *


class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/shadowrun_core/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/shadowrun_core/data/loc/'
    
class lsettings:
    synopsis = "Shadowrun: Dragonfall - Director's Cut"
    description = "The year is 2054. The promise of opportunity and anonymity draws you to the free city of Berlin: The Flux-State, a grand experiment in social order. Corporations tread carefully here. Even the great dragon Lofwyr only has so much sway in the constantly evolving power structure of Berlin. The perfect place for a savvy shadowrunner to disappear and begin anew.\n"\
                  "\n"\
                  "If only it were that simple..."

def translate_shadowrun_core_mo(root_dir, out_file):
    po = MultiPOFile()
    filenames = filelist_from_directory(root_dir, "dragonfallextended*.po")
    filenames.sort(key=cmp_version)
    for filename in filenames:
        if settings.debug:
            print (u'\tfilename: "%s"' % filename)
        po.add_patch(root_dir, filename)
    
    po.make_unique()
    
    #copy only synopsis and description
    fixed = False
    po_fix = create_po()
    for po_entry in po.po:
        if po_entry.msgid == lsettings.description or po_entry.msgid == lsettings.synopsis:
            po_fix.append(po_entry)
            fixed = True
    if not fixed:
        print (u'\tError: cant find synopsis or title. fix shadowrun_core')
    assure_path_exists(out_file)
    po_fix.save_as_mofile(out_file)

    # we need yet dummy mo's . game bug
    po_fix = create_po()
    po_fix.save_as_mofile(settings.dst_path +   settings.target_language + '/seattle.mo')
    po_fix.save_as_mofile(settings.dst_path +   settings.target_language + '/deadmanswitch.mo')
    po_fix.save_as_mofile(settings.dst_path +   settings.target_language + '/berlin.mo')

def process():
    if settings.debug:
        print (u'translate_shadowrun_core_mo')
    translate_shadowrun_core_mo(settings.localization_path, settings.dst_path +   settings.target_language + '/dragonfallextended.mo')
