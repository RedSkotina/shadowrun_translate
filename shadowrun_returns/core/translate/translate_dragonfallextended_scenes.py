# -*- coding: utf-8 -*-
from translator.utils import *
from translator.counters import *
import sys
sys.path.insert(0, './proto')
import scenes_pb2

class settings:
    src_path = './data/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/scenes/'
    dst_path = './out/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/scenes/'
    translate_path = './corrected/'
    debug = True
    
def replace_scene(scene, original_list, translate_list):
    replaced = False   
    for trigger in scene.triggers:
        #search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        
        # kreizbazar bug with loadscreen . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_set_var_string_synopsis_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_set_var_string_synopsis_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_set_var_string_synopsis_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_set_var_string_synopsis_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for i in search_result:
            for arg in i.args:
                if arg.HasField('string_value'):
                    tr = translate(arg.string_value, original_list, translate_list, unique = False)
                    if tr != "":
                        arg.string_value = tr
                        replaced = True
        # bug with Display Text In Popup . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_display_text_in_popup_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_display_text_in_popup_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                tr = translate(arg.string_value, original_list, translate_list, unique = False)
                if tr != "":
                    arg.string_value = tr
                    replaced = True
        
        
        #bug with Set Screen Label, Set Screen Label Progress Bar (int)
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for arg in search_result:
            if arg.HasField('string_value'):
                tr = translate(arg.string_value, original_list, translate_list, unique = False)
                if tr != "":
                    arg.string_value = tr
                    replaced = True
                   
        from functools import partial
        # bug with a2_lightsout_s1   str_RedOrGreen
        search_result1 = walkTsCall(trigger.events.ops,  partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result3 = walkTsCall(trigger.conditions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result4 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                    
                tr = translate(arg.string_value, original_list, translate_list, unique = False)
                if tr != "":
                    arg.string_value = tr
                    replaced = True
        # inital state of variable
        for variable in scene.variables:
            if variable.HasField('variableref_value'):
                if variable.variableref_value.name == "str_RedOrGreen":
                    tr = translate(variable.string_value, original_list, translate_list, unique = False)
                    if tr != "":
                        variable.string_value = tr
                        replaced = True
        # some bug with string
        
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        search_result3 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        search_result4 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for arg in search_result:
            if arg.HasField('string_value'):
                tr = translate(arg.string_value, original_list, translate_list, unique = False)
                if tr != "":
                    arg.string_value = tr
                    replaced = True
    # hacked object . continue from maps
    idrefs = []
    with open('./char_scene_idref.conf') as f:
        idrefs = f.readlines()
    idrefs = [idref.replace('\n','') for idref in idrefs]
    for character in scene.characters:
        if character.idRef.id in idrefs:
            tr = translate(character.character_instance.char_name, original_list, translate_list, unique = False)
            if tr != "":
                character.character_instance.char_name = tr
                replaced = True
            
    return replaced                    
                        
def translate_scene(scene_filename):
    if settings.debug:
        print (u'scene_filename: "%s"' % scene_filename)
    scene = parse_protobuf_file(settings.src_path + scene_filename, scenes_pb2.scene())
    map_name = scene.mapName
    scene_title =  scene.scene_title.replace(u'\u2019',u'\u0027').strip()
    if settings.debug:
        print (u'scene_title: "%s"' % scene_title)
    translate_filenames = find_translate_file("DragonfallExtended|berlin_campaign", settings.translate_path , regex = True, all = True)
    if settings.debug:
        print (u'translate_filenames: "%s" ' % (translate_filenames) )
    original_list = []
    translate_list = []
    for translate_filename in translate_filenames:
        orig, trans = parse_translate_file(translate_filename, settings.translate_path)
        original_list.extend(orig)
        translate_list.extend(trans)
    
    replaced = replace_scene(scene, original_list, translate_list)
    if replaced:
        assure_path_exists(settings.dst_path + scene_filename)
        write_protobuf_file(settings.dst_path+ scene_filename, scene)
    

def process():
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        translate_scene(filename)
    
if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    process()