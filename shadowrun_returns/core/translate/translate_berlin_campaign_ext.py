#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.convos_pb2 as convos_pb2
import shadowrun_returns.lib.proto.stories_pb2 as stories_pb2
import shadowrun_returns.lib.proto.scenes_pb2 as scenes_pb2
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.maps_pb2 as maps_pb2
import shadowrun_returns.lib.proto.abilities_pb2 as abilities_pb2
import shadowrun_returns.lib.proto.hiringset_pb2 as hiringset_pb2
import shadowrun_returns.lib.proto.modes_pb2 as modes_pb2
import shadowrun_returns.lib.proto.chars_pb2 as chars_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/'
    
g_char_scene_idrefs = []
    
def translate_convo_proto(convo, localization):
    replaced = False
    #bug with passwords
    for node in convo.nodes:
        if node.nodeType == node.ConversationNodeType_InputKeyboard:
            for branch in node.branches:
                #if len(node.branches[:])>1 and node.branches[:].index(branch) == (len(node.branches[:]) - 1):
                #   print "*Last branch dont must translate"
                #   break 
                if branch.inputBypass :
                    continue
                if settings.debug:
                    safeprint('pass[eng] : %s ' % (branch.responseText) )
                branch.responseText = localization[branch.responseText]
                if settings.debug:
                    safeprint('pass[loc] : %s ' % (branch.responseText) )
                replaced = True
    # bug with str_RedOrGreen
    for root in convo.roots:
        from functools import partial
        search_result = walkTsCall(root.conditions.ops, partial(search_fn_comparison_string_specific_predicate, var_name = 'str_RedOrGreen'))
        for arg in search_result:
            if arg.HasField('string_value'):
                arg.string_value = localization[arg.string_value]
                replaced = True
    return replaced
    
    
def translate_map_proto(map, localization):
    replaced = False
    for prop in map.props:
        interactionType = ""
        if prop.HasField('interactionRoot'):
            if prop.interactionRoot.HasField('jackPoint'):
                if prop.interactionRoot.jackPoint.HasField('LANIdentifier') and prop.interactionRoot.jackPoint.LANIdentifier != "":
                    interactionType = "InteractionType_JackPoint"
            else:
                if prop.interactionRoot.HasField('summonPoint'):
                    interactionType = "InteractionType_SummonPoint"
                elif prop.interactionRoot.HasField('alarmPoint'):
                    interactionType = "InteractionType_Alarm"
                elif prop.interactionRoot.HasField('pickupItem'):
                    interactionType = "InteractionType_ItemPickup"
                elif prop.interactionRoot.HasField('conversationPoint'):
                    interactionType = "InteractionType_Conversation"
                elif prop.interactionRoot.HasField('doorPoint'):
                    interactionType = "InteractionType_Door"
                elif prop.interactionRoot.HasField('transitionPoint'):
                    interactionType = "InteractionType_Transition"
                elif prop.interactionRoot.HasField('matrixSpawnPoint'):
                    interactionType = "InteractionType_MatrixSpawnPoint"
                elif prop.interactionRoot.HasField('matrixNode'):
                    interactionType = "InteractionType_MatrixNode"
                elif prop.interactionRoot.HasField('matrixLabel'):
                    interactionType = "InteractionType_DummyMatrixLabel"
                elif prop.interactionRoot.HasField('dummyInteraciton'):
                    interactionType = "InteractionType_Dummy"
                elif prop.interactionRoot.HasField('inspectInteraction'):
                    interactionType = "InteractionType_Inspect"
                elif prop.interactionRoot.HasField('warpInteraction'):
                    interactionType = "InteractionType_Generic"
                elif prop.interactionRoot.HasField('is_interaction_doc_wagon'):
                    interactionType = "InteractionType_DocWagon"
                else:
                    interactionType = "InteractionType_Generic"
                    #original_list.append(prop.displayName)
                    #TextEnabled = False
        if interactionType == "InteractionType_MatrixNode":
            islinked = False
            for hacking_object in prop.interactionRoot.matrixNode.hacking_objects:
                for tprop in map.props:
                    if tprop.HasField('idRef') and tprop.idRef.id == hacking_object:
                        islinked = True
                        tprop.displayName = localization[tprop.displayName]
                        replaced = True
                # save idref for replace scene
                # crossmodule solution
                #f = open('./char_scene_idref.conf','a')
                #f.write(hacking_object + '\n')
                #f.close() 
                # one module - using global variable
                g_char_scene_idrefs.append(hacking_object)
            if not islinked:
                #Didn't link Matrix Node  to any other object
                prop.displayName = localization[prop.displayName]
                replaced = True
        elif interactionType == "InteractionType_DummyMatrixLabel":
            #Dummy Matrix Label for object
            prop.displayName = localization[prop.displayName]
            replaced = True
            pass
        # TEST ONLY: all display name
        #if prop.HasField('displayName'):
        #    prop.displayName = localization[prop.displayName]
        
    return replaced


def translate_text_misc(text, localization):
    text = localization[text]
    return text
    
def translate_scene_proto(scene, localization):
    replaced = False   
    for trigger in scene.triggers:
        #search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        
        # kreizbazar bug with loadscreen . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_set_var_string_synopsis_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_set_var_string_synopsis_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_set_var_string_synopsis_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_set_var_string_synopsis_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for i in search_result:
            for arg in i.args:
                if arg.HasField('string_value'):
                    arg.string_value = localization[arg.string_value]
                    replaced = True
        # bug with Display Text In Popup . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_display_text_in_popup_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_display_text_in_popup_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                arg.string_value = localization[arg.string_value]
                replaced = True
        
        
        #bug with Set Screen Label, Set Screen Label Progress Bar (int)
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for arg in search_result:
            if arg.HasField('string_value'):
                arg.string_value = localization[arg.string_value]
                replaced = True
                   
        from functools import partial
        # bug with a2_lightsout_s1   str_RedOrGreen
        search_result1 = walkTsCall(trigger.events.ops,  partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result3 = walkTsCall(trigger.conditions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result4 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                arg.string_value = localization[arg.string_value]
                replaced = True
        
        # some bug with string
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        search_result3 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        search_result4 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for arg in search_result:
            if arg.HasField('string_value'):
                arg.string_value = localization[arg.string_value]
                replaced = True
    # bug with a2_lightsout_s1   str_RedOrGreen
    # inital state of variable
    for variable in scene.variables:
        if variable.HasField('variableref_value'):
            if variable.variableref_value.name == "str_RedOrGreen":
                variable.string_value = localization[variable.string_value]
                replaced = True    
    # hacked object . continue from maps
    '''
    idrefs = []
    with open('./char_scene_idref.conf') as f:
        idrefs = f.readlines()
    idrefs = [idref.replace('\n','') for idref in idrefs]
    for character in scene.characters:
        if character.idRef.id in idrefs:
            tr = translate(character.character_instance.char_name, original_list, translate_list, unique = False)
            if tr != "":
                character.character_instance.char_name = tr
                replaced = True
    '''
    # instead using global variable
    for character in scene.characters:
        if character.idRef.id in g_char_scene_idrefs:
            character.character_instance.char_name = localization[character.character_instance.char_name]
            replaced = True
            
    return replaced                    
 
#-----------------------------------------------------     
def translate_convos(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'convos/'+ id, convos_pb2.convo())
    localization = get_localization(settings.localization_path, locfilename)
    replaced = translate_convo_proto(proto, localization)
    if replaced:
        write_protobuf_file(settings.dst_path +  'convos/'+ id, proto)
    
def translate_maps(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'maps/'+ id, maps_pb2.map())
    localization = get_localization(settings.localization_path, locfilename)
    replaced = translate_map_proto(proto, localization)
    if replaced:
        write_protobuf_file(settings.dst_path +  'maps/'+ id, proto)


def translate_misc(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    text = parse_text_file(settings.src_path + 'misc/' + id)
    localization = get_localization(settings.localization_path, locfilename)
    text = translate_text_misc(text, localization)
    write_utf8_text_file(settings.dst_path + 'misc/' + id, text)
    
def translate_scenes(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'scenes/'+ id, scenes_pb2.scene())
    localization = get_localization(settings.localization_path, locfilename)
    replaced = translate_scene_proto(proto, localization)
    if replaced:
        write_protobuf_file(settings.dst_path +  'scenes/'+ id, proto)
    
#-------------------------------------------------    
def process():
    if settings.debug:
        print (u'translate_berlin_campaign_ext')

    filenames = filelist_from_directory(settings.src_path + 'convos',"*.bytes")
    for filename in filenames:
        translate_convos(filename,'berlin_campaign')
    filenames = filelist_from_directory(settings.src_path + 'maps',"*.bytes")
    for filename in filenames:
        translate_maps(filename,'berlin_campaign')
    filenames = filelist_from_directory(settings.src_path + 'misc',"*.bytes",ignore='*.*.bytes')
    for filename in filenames:
        translate_misc(filename,'berlin_campaign')
    filenames = filelist_from_directory(settings.src_path + 'scenes',"*.bytes")
    for filename in filenames:
        translate_scenes(filename,'berlin_campaign')
    