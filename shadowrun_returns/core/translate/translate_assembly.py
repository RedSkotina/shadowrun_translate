#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import tempfile
import subprocess
import shutil 
import io
import mmap

class settings(global_settings):
    src_local_path = 'Managed/Assembly-CSharp.dll'
    dst_local_path = 'Managed/Assembly-CSharp.dll'

def ildasm(assembly_filename, il = None, res = None):
    r = b''
    try:
        out_option = '/noil' if (il is None ) else '/out=%s' % il
        #r = subprocess.check_output(['lib\\ildasm\\ildasm.exe','/utf8', '/text', out_option, '%s'%assembly_filename])
        p = subprocess.Popen(['lib\\ildasm\\ildasm.exe','/utf8', '/text', out_option, '%s'%assembly_filename],stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        output,error = p.communicate()
        if (p.returncode != 0):
            raise Exception(output + error)
        
        if il is not None and res is not None:
            il_filename = get_filename_without_path(il)
            base_filename = get_name_without_extension(il_filename)
            il_path = get_path(il)
            res_filename = os.path.join(il_path,base_filename+".res")
            shutil.move(res_filename,res)
        
        with io.open(il,'r',encoding="utf-8-sig") as ilf:
            r = ilf.read()
        
    except Exception as e:
        print(e)
        sys.exit()
    #r = r.decode('utf-8')
    return r


def ilasm(assembly_filename, assembly_code, res_filename):
    r = b''
    try:
        il_filename = os.path.join(tempfile.gettempdir(),get_name_without_extension(get_filename_without_path(res_filename))+'.il')
        with io.open(il_filename,'w',encoding="utf-8-sig") as il:
            il.write(assembly_code)
        assembly_filename = os.path.abspath(assembly_filename)
        assure_path_exists(assembly_filename)
        
        #p = subprocess.check_output(['lib\\ilasm\\ilasm.exe',' /resource=%s'%res_filename,'/dll','/output=%s'%assembly_filename,'%s'%il_filename])
        p = subprocess.Popen(['lib\\ilasm\\ilasm.exe',il_filename,'/resource=%s'%res_filename,'/dll','/output=%s'%assembly_filename],stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        output,error = p.communicate()
        if (p.returncode != 0):
            raise Exception(output + error)
    except Exception as e:
        print(e)
        sys.exit()
    r = r.decode('utf-8')
    return r

def decompile(assembly_filename):
    if os.path.exists(assembly_filename):
        il_filename = os.path.join( tempfile.gettempdir(), get_filename_without_path(assembly_filename) )
        res_filename = os.path.join( tempfile.gettempdir(), get_name_without_extension(get_filename_without_path(assembly_filename)) + ".res" )
        
        assembly_code = ildasm(assembly_filename, il_filename, res_filename)
        return assembly_code, il_filename, res_filename 
    return None
    
def compile(assembly_filename, assembly_code, res_filename):
    if os.path.exists(res_filename):
        ilasm(assembly_filename, assembly_code, res_filename)
    return None 
    
def replace_text(text, pieces):
    pieces.sort(key=lambda x: x['start_pos']) 
    
    import io
    output = io.StringIO()
    cur_pos = 3 # utf-8-sig skip
    for piece in pieces:
        output.write( text[cur_pos:piece['start_pos']].decode("utf-8"))
        output.write(piece['msgstr'])
        cur_pos = piece['end_pos']

    output.write( text[cur_pos:].decode("utf-8"))
    res = output.getvalue()
    output.close()
    return res
    
#ShadowrunAI::AIReload
# "reloaded!" and etc
def replace_aireload(assembly_code, localization):
    pieces = []
    reg = rb'ldstr\s+\"([\w\d\s\!\?\:\.\,\-\{\}\(\)\\]+?)\"\r\n\s+(?:[\w\d\_\:]+\s+\w+\s+valuetype\s+\[UnityEngine\]UnityEngine\.Color\s+[\[\]\.\:\(\)\w\_]+)\r\n\s+(?:[\w\d\_\:]+\s+ldc\.r4\s+\d+\.\d*)\r\n\s+(?:[\w\d\_\:]+\s+ldc.i4.0)\r\n\s+(?:[\d\w\_])+:\s+callvirt\s+instance\s+void\s+Player::AddFloatie'
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s :
        localized_string = localization[match.group(1).decode("utf-8")]
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': localized_string}
        #safeprint(match.group(1))
        #safeprint(localized_string)
        pieces.append(res) 
    if s is None:
        print("\tCant replace aireload")
    return pieces

# AbilityDescriptionPanel::SetCustomDescription
# "Overwatch" 
def replace_custom_description(assembly_code, localization):
    pieces = []
    reg = rb'ldstr\s+(\".+?\")\s+(?:[\w\_\d\:]+)\s+ldstr\s+((?:\".+?\"\s+\+?\s+)+)(?:[\w\_\d\:]+)\s+callvirt\s+instance\s+void\s+AbilityDescriptionPanel\:\:SetCustomDescription'
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s :
        escaped_string = ILmultistring_to_string(match.group(1).decode("utf-8"))
        localized_string = localization[escaped_string]
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': string_to_ILmultistring(localized_string)}
        pieces.append(res)
        escaped_string = ILmultistring_to_string(match.group(2).decode("utf-8"))
        localized_string = localization[escaped_string]
        res = {'start_pos' : match.start(2), 'end_pos' : match.end(2), 'msgstr': string_to_ILmultistring(localized_string)}
        pieces.append(res)
            
    return pieces
    
# TeamAdvancementScreen::OnHover
# add Strings.T()
def replace_teamadvancementscreen_onhover(assembly_code):
    pieces = []
    #reg = b'\\{((?:.|\\n)*?)\\}\\s\\/\\/ end of method TeamAdvancementScreen\\:\\:OnHover'
    #reg = rb'\{((?:.|\n)*?)\}\s\/\/ end of method TeamAdvancementScreen\:\:OnHover'
    reg = rb"\{([\/\{\}\[\]\"\.\,\(\)\:\s\w]*?)\}\s\/\/ end of method TeamAdvancementScreen\:\:OnHover"
    newOnHover = """
    

    // 

    .maxstack  29

    IL_0000:  ldarg.1

    IL_0001:  brfalse    IL_0042



    IL_0006:  ldarg.0

    IL_0007:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionTitle

    IL_000c:  ldstr      "{0} // {1}"

    IL_0011:  ldarg.2

    IL_0012:  callvirt   instance string [ShadowrunDTO]isogame.CrewNode::get_name()

    IL_0017:  ldarg.3

    IL_0018:  callvirt   instance string [ShadowrunDTO]isogame.CharacterVariant::get_name()

    IL_0019:  call       string Localize.Strings::T(string)
    
    IL_001d:  call       string [mscorlib]System.String::Format(string,

                                                                object,

                                                                object)

    IL_0022:  callvirt   instance void UILabel::set_text(string)

    IL_0027:  ldarg.0

    IL_0028:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionText

    IL_002d:  ldarg.3

    IL_002e:  callvirt   instance string [ShadowrunDTO]isogame.CharacterVariant::get_long_desc()

    IL_0033:  call       string Localize.Strings::T(string)

    IL_0038:  callvirt   instance void UILabel::set_text(string)

    IL_003d:  br         IL_00b8



    IL_0042:  ldarg.0

    IL_0043:  ldfld      class [ShadowrunDTO]isogame.CharacterVariant TeamAdvancementScreen::lastClickedVariant

    IL_0048:  brfalse    IL_0098



    IL_004d:  ldarg.0

    IL_004e:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionTitle

    IL_0053:  ldstr      "{0} // {1}"

    IL_0058:  ldarg.0

    IL_0059:  ldfld      class [ShadowrunDTO]isogame.CrewNode TeamAdvancementScreen::lastClickedNode

    IL_005e:  callvirt   instance string [ShadowrunDTO]isogame.CrewNode::get_name()

    IL_0063:  ldarg.0

    IL_0064:  ldfld      class [ShadowrunDTO]isogame.CharacterVariant TeamAdvancementScreen::lastClickedVariant

    IL_0069:  callvirt   instance string [ShadowrunDTO]isogame.CharacterVariant::get_name()
    
    IL_006a:  call       string Localize.Strings::T(string)
    
    IL_006e:  call       string [mscorlib]System.String::Format(string,

                                                                object,

                                                                object)

    IL_0073:  callvirt   instance void UILabel::set_text(string)

    IL_0078:  ldarg.0

    IL_0079:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionText

    IL_007e:  ldarg.0

    IL_007f:  ldfld      class [ShadowrunDTO]isogame.CharacterVariant TeamAdvancementScreen::lastClickedVariant

    IL_0084:  callvirt   instance string [ShadowrunDTO]isogame.CharacterVariant::get_long_desc()

    IL_0089:  call       string Localize.Strings::T(string)

    IL_008e:  callvirt   instance void UILabel::set_text(string)

    IL_0093:  br         IL_00b8



    IL_0098:  ldarg.0

    IL_0099:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionTitle

    IL_009e:  ldsfld     string [mscorlib]System.String::Empty

    IL_00a3:  callvirt   instance void UILabel::set_text(string)

    IL_00a8:  ldarg.0

    IL_00a9:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionText

    IL_00ae:  ldsfld     string [mscorlib]System.String::Empty

    IL_00b3:  callvirt   instance void UILabel::set_text(string)

    IL_00b8:  ret

  """
    
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newOnHover}
        pieces.append(res)   
        
    if not tuple(s):
        print("\tCant replace teamadvancementscreen_onhover")
        
    return pieces
    
# TeamAdvancementScreen::Awake
# add Strings.T()
def replace_teamadvancementscreen_awake(assembly_code):
    pieces = []
    reg = rb"Awake\(\) cil managed\s+\{([\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method TeamAdvancementScreen\:\:Awake"
    newAwake = """
    // 

    .maxstack  116

    .locals init (int32 V_0,

             class BasicButton V_1,

             valuetype [UnityEngine]UnityEngine.Color V_2,

             valuetype [UnityEngine]UnityEngine.Color V_3,

             class UISprite V_4,

             valuetype [mscorlib]System.Collections.Generic.List`1/Enumerator<class UISprite> V_5,

             int32 V_6,

             class [UnityEngine]UnityEngine.GameObject V_7,

             int32 V_8)

    IL_0000:  ldc.i4.0

    IL_0001:  stloc.0

    IL_0002:  br         IL_0058



    IL_0007:  ldarg.0

    IL_0008:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::portraitList

    IL_000d:  ldloc.0

    IL_000e:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class UISprite>::get_Item(int32)

    IL_0013:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_0018:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Transform::get_parent()

    IL_001d:  callvirt   instance !!0 [UnityEngine]UnityEngine.Component::GetComponentInChildren<class BasicButton>()

    IL_0022:  stloc.1

    IL_0023:  ldloc.1

    IL_0024:  ldloc.0

    IL_0025:  stfld      int32 BasicButton::butId

    IL_002a:  ldloc.1

    IL_002b:  ldc.i4.1

    IL_002c:  stfld      bool BasicButton::onClick

    IL_0031:  ldloc.1

    IL_0032:  ldarg.0

    IL_0033:  call       instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_0038:  stfld      class [UnityEngine]UnityEngine.GameObject BasicButton::target

    IL_003d:  ldloc.1

    IL_003e:  ldstr      "OnPortraitClick"

    IL_0043:  callvirt   instance void BasicButton::SetCustomSingleClickMessage(string)

    IL_0048:  ldarg.0

    IL_0049:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class BasicButton> TeamAdvancementScreen::portraitButtonList

    IL_004e:  ldloc.1

    IL_004f:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<class BasicButton>::Add(!0)

    IL_0054:  ldloc.0

    IL_0055:  ldc.i4.1

    IL_0056:  add

    IL_0057:  stloc.0

    IL_0058:  ldloc.0

    IL_0059:  ldarg.0

    IL_005a:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::portraitList

    IL_005f:  callvirt   instance int32 class [mscorlib]System.Collections.Generic.List`1<class UISprite>::get_Count()

    IL_0064:  blt        IL_0007



    IL_0069:  call       void Constants::LoadDefaults()

    IL_006e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::PALE_BLUE_COLOR

    IL_0073:  stloc.2

    IL_0074:  ldloc.2

    IL_0075:  stloc.3

    IL_0076:  ldloca.s   V_3

    IL_0078:  ldc.r4     0.75

    IL_007d:  stfld      float32 [UnityEngine]UnityEngine.Color::a

    IL_0082:  ldarg.0

    IL_0083:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::backgroundElements

    IL_0088:  callvirt   instance valuetype [mscorlib]System.Collections.Generic.List`1/Enumerator<!0> class [mscorlib]System.Collections.Generic.List`1<class UISprite>::GetEnumerator()

    IL_008d:  stloc.s    V_5

    .try

    {

      IL_008f:  br         IL_00a5



      IL_0094:  ldloca.s   V_5

      IL_0096:  call       instance !0 valuetype [mscorlib]System.Collections.Generic.List`1/Enumerator<class UISprite>::get_Current()

      IL_009b:  stloc.s    V_4

      IL_009d:  ldloc.s    V_4

      IL_009f:  ldloc.3

      IL_00a0:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

      IL_00a5:  ldloca.s   V_5

      IL_00a7:  call       instance bool valuetype [mscorlib]System.Collections.Generic.List`1/Enumerator<class UISprite>::MoveNext()

      IL_00ac:  brtrue     IL_0094



      IL_00b1:  leave      IL_00c3



    }  // end .try

    finally

    {

      IL_00b6:  ldloc.s    V_5

      IL_00b8:  box        valuetype [mscorlib]System.Collections.Generic.List`1/Enumerator<class UISprite>

      IL_00bd:  callvirt   instance void [mscorlib]System.IDisposable::Dispose()

      IL_00c2:  endfinally

    }  // end handler

    IL_00c3:  ldarg.0

    IL_00c4:  ldfld      class UILabel TeamAdvancementScreen::characterName

    IL_00c9:  ldloc.2

    IL_00ca:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_00cf:  ldarg.0

    IL_00d0:  ldfld      class UILabel TeamAdvancementScreen::characterTitle

    IL_00d5:  ldloc.2

    IL_00d6:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_00db:  ldarg.0

    IL_00dc:  ldfld      class UISprite TeamAdvancementScreen::prevCharacter

    IL_00e1:  ldloc.3

    IL_00e2:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_00e7:  ldarg.0

    IL_00e8:  ldfld      class UISprite TeamAdvancementScreen::nextCharacter

    IL_00ed:  ldloc.3

    IL_00ee:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_00f3:  ldarg.0

    IL_00f4:  ldfld      class UIButton TeamAdvancementScreen::prevCharacterButton

    IL_00f9:  ldloc.2

    IL_00fa:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_00ff:  ldarg.0

    IL_0100:  ldfld      class UIButton TeamAdvancementScreen::prevCharacterButton

    IL_0105:  ldloc.2

    IL_0106:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_010b:  ldarg.0

    IL_010c:  ldfld      class UIButton TeamAdvancementScreen::prevCharacterButton

    IL_0111:  ldloc.3

    IL_0112:  callvirt   instance void UIButtonColor::set_defaultColor(valuetype [UnityEngine]UnityEngine.Color)

    IL_0117:  ldarg.0

    IL_0118:  ldfld      class UIButton TeamAdvancementScreen::nextCharacterButton

    IL_011d:  ldloc.2

    IL_011e:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0123:  ldarg.0

    IL_0124:  ldfld      class UIButton TeamAdvancementScreen::nextCharacterButton

    IL_0129:  ldloc.2

    IL_012a:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_012f:  ldarg.0

    IL_0130:  ldfld      class UIButton TeamAdvancementScreen::nextCharacterButton

    IL_0135:  ldloc.3

    IL_0136:  callvirt   instance void UIButtonColor::set_defaultColor(valuetype [UnityEngine]UnityEngine.Color)

    IL_013b:  ldarg.0

    IL_013c:  ldfld      class UISprite TeamAdvancementScreen::activeCharacterIcon

    IL_0141:  ldloc.2

    IL_0142:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0147:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_014c:  stloc.2

    IL_014d:  ldloc.2

    IL_014e:  stloc.3

    IL_014f:  ldloca.s   V_3

    IL_0151:  ldc.r4     0.5

    IL_0156:  stfld      float32 [UnityEngine]UnityEngine.Color::a

    IL_015b:  ldarg.0

    IL_015c:  ldfld      class UISlicedSprite TeamAdvancementScreen::frame

    IL_0161:  ldloc.2

    IL_0162:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0167:  ldarg.0

    IL_0168:  ldfld      class UISprite TeamAdvancementScreen::advancementIcon

    IL_016d:  ldloc.2

    IL_016e:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0173:  ldarg.0

    IL_0174:  ldfld      class UISlicedSprite TeamAdvancementScreen::statHeader

    IL_0179:  ldloc.3

    IL_017a:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_017f:  ldarg.0

    IL_0180:  ldfld      class UILabel TeamAdvancementScreen::statHeaderText

    IL_0185:  ldloc.2

    IL_0186:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_018b:  ldarg.0

    IL_018c:  ldfld      class UILabel TeamAdvancementScreen::statDescription

    IL_0191:  ldloc.2

    IL_0192:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0197:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_019c:  stloc.2

    IL_019d:  ldloc.2

    IL_019e:  stloc.3

    IL_019f:  ldloca.s   V_3

    IL_01a1:  ldc.r4     0.5

    IL_01a6:  stfld      float32 [UnityEngine]UnityEngine.Color::a

    IL_01ab:  ldarg.0

    IL_01ac:  ldfld      class UISlicedSprite TeamAdvancementScreen::focusDescriptionBG

    IL_01b1:  ldloc.2

    IL_01b2:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_01b7:  ldarg.0

    IL_01b8:  ldfld      class UISlicedSprite TeamAdvancementScreen::focusDescriptionBar

    IL_01bd:  ldloc.3

    IL_01be:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_01c3:  ldarg.0

    IL_01c4:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionTitle

    IL_01c9:  ldloc.2

    IL_01ca:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_01cf:  ldarg.0

    IL_01d0:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionText

    IL_01d5:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GREY_COLOR

    IL_01da:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_01df:  ldarg.0

    IL_01e0:  ldfld      class UILabel TeamAdvancementScreen::focusDescriptionText

    IL_01e5:  ldc.i4     0x276

    IL_01ea:  callvirt   instance void UILabel::set_lineWidth(int32)

    IL_01ef:  ldarg.0

    IL_01f0:  ldfld      class UIButton TeamAdvancementScreen::confirmButtonUI

    IL_01f5:  ldloc.2

    IL_01f6:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_01fb:  ldarg.0

    IL_01fc:  ldfld      class UIButton TeamAdvancementScreen::confirmButtonUI

    IL_0201:  ldloc.2

    IL_0202:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0207:  ldarg.0

    IL_0208:  ldfld      class UIButton TeamAdvancementScreen::confirmButtonUI

    IL_020d:  ldloc.3

    IL_020e:  callvirt   instance void UIButtonColor::set_defaultColor(valuetype [UnityEngine]UnityEngine.Color)

    IL_0213:  ldarg.0

    IL_0214:  ldfld      class UISprite TeamAdvancementScreen::confirmButton

    IL_0219:  ldloc.3

    IL_021a:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_021f:  ldarg.0

    IL_0220:  ldfld      class UILabel TeamAdvancementScreen::confirmButtonText

    IL_0225:  ldloc.2

    IL_0226:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_022b:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0230:  stloc.2

    IL_0231:  ldloc.2

    IL_0232:  stloc.3

    IL_0233:  ldloca.s   V_3

    IL_0235:  ldc.r4     0.75

    IL_023a:  stfld      float32 [UnityEngine]UnityEngine.Color::a

    IL_023f:  ldarg.0

    IL_0240:  ldfld      class UIButton TeamAdvancementScreen::cancelButtonUI

    IL_0245:  ldloc.2

    IL_0246:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_024b:  ldarg.0

    IL_024c:  ldfld      class UIButton TeamAdvancementScreen::cancelButtonUI

    IL_0251:  ldloc.2

    IL_0252:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0257:  ldarg.0

    IL_0258:  ldfld      class UIButton TeamAdvancementScreen::cancelButtonUI

    IL_025d:  ldloc.3

    IL_025e:  callvirt   instance void UIButtonColor::set_defaultColor(valuetype [UnityEngine]UnityEngine.Color)

    IL_0263:  ldarg.0

    IL_0264:  ldfld      class UISprite TeamAdvancementScreen::cancelButton

    IL_0269:  ldloc.3

    IL_026a:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_026f:  ldarg.0

    IL_0270:  ldfld      class UILabel TeamAdvancementScreen::cancelButtonText

    IL_0275:  ldloc.2

    IL_0276:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_027b:  ldarg.0

    IL_027c:  ldfld      class UIButton TeamAdvancementScreen::clearButtonUI

    IL_0281:  ldloc.2

    IL_0282:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0287:  ldarg.0

    IL_0288:  ldfld      class UIButton TeamAdvancementScreen::clearButtonUI

    IL_028d:  ldloc.2

    IL_028e:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0293:  ldarg.0

    IL_0294:  ldfld      class UIButton TeamAdvancementScreen::clearButtonUI

    IL_0299:  ldloc.3

    IL_029a:  callvirt   instance void UIButtonColor::set_defaultColor(valuetype [UnityEngine]UnityEngine.Color)

    IL_029f:  ldarg.0

    IL_02a0:  ldfld      class UISprite TeamAdvancementScreen::clearButton

    IL_02a5:  ldloc.3

    IL_02a6:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_02ab:  ldarg.0

    IL_02ac:  ldfld      class UILabel TeamAdvancementScreen::clearButtonText

    IL_02b1:  ldloc.2

    IL_02b2:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)
    
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::clearButtonText
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::clearButtonText
    callvirt   instance string UILabel::get_text()
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string) 
    
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::confirmButtonText
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::confirmButtonText
    callvirt   instance string UILabel::get_text()
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string) 
    
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::cancelButtonText
    ldarg.0
    ldfld      class UILabel TeamAdvancementScreen::cancelButtonText
    callvirt   instance string UILabel::get_text()
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string) 
    
    IL_02b7:  ldc.i4.0

    IL_02b8:  stloc.s    V_6

    IL_02ba:  br         IL_0335



    IL_02bf:  ldarg.0

    IL_02c0:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::portraitList

    IL_02c5:  ldloc.s    V_6

    IL_02c7:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class UISprite>::get_Item(int32)

    IL_02cc:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_02d1:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Transform::get_parent()

    IL_02d6:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_02db:  stloc.s    V_7

    IL_02dd:  ldc.i4.0

    IL_02de:  stloc.s    V_8

    IL_02e0:  br         IL_0304



    IL_02e5:  ldloc.s    V_7

    IL_02e7:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.GameObject::get_transform()

    IL_02ec:  ldloc.s    V_8

    IL_02ee:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Transform::GetChild(int32)

    IL_02f3:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_02f8:  ldc.i4.0

    IL_02f9:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_02fe:  ldloc.s    V_8

    IL_0300:  ldc.i4.1

    IL_0301:  add

    IL_0302:  stloc.s    V_8

    IL_0304:  ldloc.s    V_8

    IL_0306:  ldloc.s    V_7

    IL_0308:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.GameObject::get_transform()

    IL_030d:  callvirt   instance int32 [UnityEngine]UnityEngine.Transform::get_childCount()

    IL_0312:  blt        IL_02e5



    IL_0317:  ldarg.0

    IL_0318:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::portraitList

    IL_031d:  ldloc.s    V_6

    IL_031f:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class UISprite>::get_Item(int32)

    IL_0324:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_0329:  ldc.i4.1

    IL_032a:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_032f:  ldloc.s    V_6

    IL_0331:  ldc.i4.1

    IL_0332:  add

    IL_0333:  stloc.s    V_6

    IL_0335:  ldloc.s    V_6

    IL_0337:  ldarg.0

    IL_0338:  ldfld      class [mscorlib]System.Collections.Generic.List`1<class UISprite> TeamAdvancementScreen::portraitList

    IL_033d:  callvirt   instance int32 class [mscorlib]System.Collections.Generic.List`1<class UISprite>::get_Count()

    IL_0342:  blt        IL_02bf



    IL_0347:  ldarg.0

    IL_0348:  ldfld      class UIScrollBar TeamAdvancementScreen::scrollBar

    IL_034d:  callvirt   instance class UISprite UIScrollBar::get_background()

    IL_0352:  ldloc.2

    IL_0353:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0358:  ldarg.0

    IL_0359:  ldfld      class UIScrollBar TeamAdvancementScreen::scrollBar

    IL_035e:  callvirt   instance class UISprite UIScrollBar::get_foreground()

    IL_0363:  ldloc.2

    IL_0364:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0369:  ret  
    
"""
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newAwake}
        pieces.append(res)   
    
    if not tuple(s):
        print("\tCant replace teamadvancementscreen_awake")
    
    return pieces

# PDAAnchor::Awake
# add Format and Strings.T()
def replace_pdaanchor_awake(assembly_code):
    pieces = []
    reg = rb"Awake\(\) cil managed\s+\{([\*\+\\\?\&\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method PDAAnchor\:\:Awake"
    newAwake = """

    // 

    .maxstack  742

    .locals init (string V_0,

             int32 V_1,

             valuetype [UnityEngine]UnityEngine.Vector3 V_2,

             valuetype [UnityEngine]UnityEngine.Vector3 V_3,

             valuetype [UnityEngine]UnityEngine.Vector3 V_4,

             valuetype [UnityEngine]UnityEngine.Vector3 V_5,

             valuetype [UnityEngine]UnityEngine.Vector3 V_6,

             valuetype [UnityEngine]UnityEngine.Vector3 V_7,

             valuetype [UnityEngine]UnityEngine.Vector3 V_8,

             valuetype [UnityEngine]UnityEngine.Vector3 V_9,

             valuetype [UnityEngine]UnityEngine.Vector3 V_10,

             valuetype [UnityEngine]UnityEngine.Vector3 V_11,

             valuetype [UnityEngine]UnityEngine.Vector3 V_12,

             valuetype [UnityEngine]UnityEngine.Vector3 V_13,

             valuetype [UnityEngine]UnityEngine.Vector3 V_14,

             valuetype [UnityEngine]UnityEngine.Vector3 V_15,

             valuetype [UnityEngine]UnityEngine.Vector3 V_16,

             valuetype [UnityEngine]UnityEngine.Vector3 V_17,

             valuetype [UnityEngine]UnityEngine.Vector3 V_18,

             valuetype [UnityEngine]UnityEngine.Vector3 V_19,

             valuetype [UnityEngine]UnityEngine.Vector3 V_20,

             valuetype [UnityEngine]UnityEngine.Vector3 V_21,

             valuetype [UnityEngine]UnityEngine.Vector3 V_22,

             valuetype [UnityEngine]UnityEngine.Vector3 V_23,

             valuetype [UnityEngine]UnityEngine.Vector3 V_24,

             valuetype [UnityEngine]UnityEngine.Vector3 V_25)

    IL_0000:  ldarg.0

    IL_0001:  ldfld      class UILabel PDAAnchor::nuyenValue

    IL_0006:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_000b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0010:  ldarg.0

    IL_0011:  ldfld      class UILabel PDAAnchor::versionText

    IL_0016:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::PALE_BLUE_COLOR

    IL_001b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0020:  ldarg.0

    IL_0021:  ldfld      class UILabel PDAAnchor::versionText

    IL_0026:  ldc.r4     0.5

    IL_002b:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_0030:  ldarg.0

    IL_0031:  ldfld      class UIButton PDAAnchor::helpButtonUI

    IL_0036:  ldc.r4     0.1

    IL_003b:  stfld      float32 UIButtonColor::duration

    IL_0040:  ldarg.0

    IL_0041:  ldfld      class UIButton PDAAnchor::helpButtonUI

    IL_0046:  call       valuetype [UnityEngine]UnityEngine.Color [UnityEngine]UnityEngine.Color::get_white()

    IL_004b:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0050:  ldc.r4     1.

    IL_0055:  ldc.r4     1.

    IL_005a:  ldc.r4     0.80000001

    IL_005f:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0064:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0069:  ldarg.0

    IL_006a:  ldfld      class UIButton PDAAnchor::helpButtonUI

    IL_006f:  call       valuetype [UnityEngine]UnityEngine.Color [UnityEngine]UnityEngine.Color::get_white()

    IL_0074:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0079:  ldc.r4     1.

    IL_007e:  ldc.r4     1.

    IL_0083:  ldc.r4     0.80000001

    IL_0088:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_008d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0092:  ldarg.0

    IL_0093:  ldfld      class UIButton PDAAnchor::objectivesButtonUI

    IL_0098:  ldc.r4     0.1

    IL_009d:  stfld      float32 UIButtonColor::duration

    IL_00a2:  ldarg.0

    IL_00a3:  ldfld      class UIButton PDAAnchor::objectivesButtonUI

    IL_00a8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_00ad:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_00b2:  ldc.r4     1.

    IL_00b7:  ldc.r4     1.

    IL_00bc:  ldc.r4     0.80000001

    IL_00c1:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_00c6:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_00cb:  ldarg.0

    IL_00cc:  ldfld      class UIButton PDAAnchor::objectivesButtonUI

    IL_00d1:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_00d6:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_00db:  ldc.r4     1.

    IL_00e0:  ldc.r4     1.

    IL_00e5:  ldc.r4     0.80000001

    IL_00ea:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_00ef:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_00f4:  ldarg.0

    IL_00f5:  ldfld      class UIButton PDAAnchor::characterButtonUI

    IL_00fa:  ldc.r4     0.1

    IL_00ff:  stfld      float32 UIButtonColor::duration

    IL_0104:  ldarg.0

    IL_0105:  ldfld      class UIButton PDAAnchor::characterButtonUI

    IL_010a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_010f:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0114:  ldc.r4     1.

    IL_0119:  ldc.r4     1.

    IL_011e:  ldc.r4     0.80000001

    IL_0123:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0128:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_012d:  ldarg.0

    IL_012e:  ldfld      class UIButton PDAAnchor::characterButtonUI

    IL_0133:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0138:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_013d:  ldc.r4     1.

    IL_0142:  ldc.r4     1.

    IL_0147:  ldc.r4     0.80000001

    IL_014c:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0151:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0156:  ldarg.0

    IL_0157:  ldfld      class UIButton PDAAnchor::characterWithNotificationButtonUI

    IL_015c:  ldc.r4     0.1

    IL_0161:  stfld      float32 UIButtonColor::duration

    IL_0166:  ldarg.0

    IL_0167:  ldfld      class UIButton PDAAnchor::characterWithNotificationButtonUI

    IL_016c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0171:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0176:  ldc.r4     1.

    IL_017b:  ldc.r4     1.

    IL_0180:  ldc.r4     0.80000001

    IL_0185:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_018a:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_018f:  ldarg.0

    IL_0190:  ldfld      class UIButton PDAAnchor::characterWithNotificationButtonUI

    IL_0195:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_019a:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_019f:  ldc.r4     1.

    IL_01a4:  ldc.r4     1.

    IL_01a9:  ldc.r4     0.80000001

    IL_01ae:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_01b3:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_01b8:  ldarg.0

    IL_01b9:  ldfld      class UILabel PDAAnchor::characterNotification

    IL_01be:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_01c3:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_01c8:  ldarg.0

    IL_01c9:  ldfld      class UIButton PDAAnchor::gearButtonUI

    IL_01ce:  ldc.r4     0.1

    IL_01d3:  stfld      float32 UIButtonColor::duration

    IL_01d8:  ldarg.0

    IL_01d9:  ldfld      class UIButton PDAAnchor::gearButtonUI

    IL_01de:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_01e3:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_01e8:  ldc.r4     1.

    IL_01ed:  ldc.r4     1.

    IL_01f2:  ldc.r4     0.80000001

    IL_01f7:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_01fc:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0201:  ldarg.0

    IL_0202:  ldfld      class UIButton PDAAnchor::gearButtonUI

    IL_0207:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_020c:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0211:  ldc.r4     1.

    IL_0216:  ldc.r4     1.

    IL_021b:  ldc.r4     0.80000001

    IL_0220:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0225:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_022a:  ldarg.0

    IL_022b:  ldfld      class UIButton PDAAnchor::menuButtonUI

    IL_0230:  ldc.r4     0.1

    IL_0235:  stfld      float32 UIButtonColor::duration

    IL_023a:  ldarg.0

    IL_023b:  ldfld      class UIButton PDAAnchor::menuButtonUI

    IL_0240:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0245:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_024a:  ldc.r4     1.

    IL_024f:  ldc.r4     1.

    IL_0254:  ldc.r4     0.80000001

    IL_0259:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_025e:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0263:  ldarg.0

    IL_0264:  ldfld      class UIButton PDAAnchor::menuButtonUI

    IL_0269:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_026e:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0273:  ldc.r4     1.

    IL_0278:  ldc.r4     1.

    IL_027d:  ldc.r4     0.80000001

    IL_0282:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0287:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_028c:  ldarg.0

    IL_028d:  ldfld      class UISprite PDAAnchor::objectivesTabBase

    IL_0292:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0297:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_029c:  ldarg.0

    IL_029d:  ldfld      class UIButton PDAAnchor::objectivesTabUI

    IL_02a2:  ldc.r4     0.1

    IL_02a7:  stfld      float32 UIButtonColor::duration

    IL_02ac:  ldarg.0

    IL_02ad:  ldfld      class UIButton PDAAnchor::objectivesTabUI

    IL_02b2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_02b7:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_02bc:  ldc.r4     1.

    IL_02c1:  ldc.r4     1.

    IL_02c6:  ldc.r4     0.80000001

    IL_02cb:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_02d0:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_02d5:  ldarg.0

    IL_02d6:  ldfld      class UIButton PDAAnchor::objectivesTabUI

    IL_02db:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_02e0:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_02e5:  ldc.r4     1.

    IL_02ea:  ldc.r4     1.

    IL_02ef:  ldc.r4     0.80000001

    IL_02f4:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_02f9:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_02fe:  ldarg.0

    IL_02ff:  ldfld      class UISprite PDAAnchor::objectivesTabSelect

    IL_0304:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0309:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_030e:  ldarg.0

    IL_030f:  ldfld      class UILabel PDAAnchor::objectivesTabText

    IL_0314:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0319:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_031e:  ldarg.0

    IL_031f:  ldfld      class UISprite PDAAnchor::itemsTabBase

    IL_0324:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0329:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_032e:  ldarg.0

    IL_032f:  ldfld      class UIButton PDAAnchor::itemsTabUI

    IL_0334:  ldc.r4     0.1

    IL_0339:  stfld      float32 UIButtonColor::duration

    IL_033e:  ldarg.0

    IL_033f:  ldfld      class UIButton PDAAnchor::itemsTabUI

    IL_0344:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0349:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_034e:  ldc.r4     1.

    IL_0353:  ldc.r4     1.

    IL_0358:  ldc.r4     0.80000001

    IL_035d:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0362:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0367:  ldarg.0

    IL_0368:  ldfld      class UIButton PDAAnchor::itemsTabUI

    IL_036d:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0372:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0377:  ldc.r4     1.

    IL_037c:  ldc.r4     1.

    IL_0381:  ldc.r4     0.80000001

    IL_0386:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_038b:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0390:  ldarg.0

    IL_0391:  ldfld      class UISprite PDAAnchor::itemsTabSelect

    IL_0396:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_039b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03a0:  ldarg.0

    IL_03a1:  ldfld      class UILabel PDAAnchor::itemsTabText

    IL_03a6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03ab:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03b0:  ldarg.0

    IL_03b1:  ldfld      class UISlicedSprite PDAAnchor::objectivesDragScrollbarBG

    IL_03b6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03bb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03c0:  ldarg.0

    IL_03c1:  ldfld      class UISlicedSprite PDAAnchor::objectivesDragScrollbarFG

    IL_03c6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03cb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03d0:  ldarg.0

    IL_03d1:  ldfld      class UISlicedSprite PDAAnchor::itemsDragScrollbarBG

    IL_03d6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03db:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03e0:  ldarg.0

    IL_03e1:  ldfld      class UISlicedSprite PDAAnchor::itemsDragScrollbarFG

    IL_03e6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03eb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_03f0:  ldarg.0

    IL_03f1:  ldfld      class UISprite PDAAnchor::objectivesGlow

    IL_03f6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_03fb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0400:  ldarg.0

    IL_0401:  ldfld      class UILabel PDAAnchor::chapterNameText

    IL_0406:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_040b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0410:  ldarg.0

    IL_0411:  ldfld      class UILabel PDAAnchor::chapterSummaryText

    IL_0416:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_041b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0420:  ldarg.0

    IL_0421:  ldfld      class UILabel PDAAnchor::raceText

    IL_0426:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_042b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0430:  ldarg.0

    IL_0431:  ldfld      class UILabel PDAAnchor::raceValue

    IL_0436:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_043b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0440:  ldarg.0

    IL_0441:  ldfld      class UILabel PDAAnchor::genderText

    IL_0446:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_044b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0450:  ldarg.0

    IL_0451:  ldfld      class UILabel PDAAnchor::genderValue

    IL_0456:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_045b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0460:  ldarg.0

    IL_0461:  ldfld      class UILabel PDAAnchor::roleText

    IL_0466:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_046b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0470:  ldarg.0

    IL_0471:  ldfld      class UILabel PDAAnchor::roleValue

    IL_0476:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_047b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0480:  ldarg.0

    IL_0481:  ldfld      class UILabel PDAAnchor::etiquetteValue

    IL_0486:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_048b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0490:  ldarg.0

    IL_0491:  ldfld      class UILabel PDAAnchor::synopsisValue

    IL_0496:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_049b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04a0:  ldarg.0

    IL_04a1:  ldfld      class UISlicedSprite PDAAnchor::selectPortraitFrame

    IL_04a6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_04ab:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04b0:  ldarg.0

    IL_04b1:  ldfld      class UILabel PDAAnchor::selectNameText

    IL_04b6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_04bb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04c0:  ldarg.0

    IL_04c1:  ldfld      class UISlicedSprite PDAAnchor::selectThumbnailOutline

    IL_04c6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_04cb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04d0:  ldarg.0

    IL_04d1:  ldfld      class UISprite PDAAnchor::characterLeft

    IL_04d6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_04db:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04e0:  ldarg.0

    IL_04e1:  ldfld      class UISprite PDAAnchor::characterRight

    IL_04e6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_04eb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_04f0:  ldarg.0

    IL_04f1:  ldfld      class UIButton PDAAnchor::characterLeftUI

    IL_04f6:  ldc.r4     0.1

    IL_04fb:  stfld      float32 UIButtonColor::duration

    IL_0500:  ldarg.0

    IL_0501:  ldfld      class UIButton PDAAnchor::characterLeftUI

    IL_0506:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_050b:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0510:  ldc.r4     1.

    IL_0515:  ldc.r4     1.

    IL_051a:  ldc.r4     0.80000001

    IL_051f:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0524:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0529:  ldarg.0

    IL_052a:  ldfld      class UIButton PDAAnchor::characterLeftUI

    IL_052f:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0534:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0539:  ldc.r4     1.

    IL_053e:  ldc.r4     1.

    IL_0543:  ldc.r4     0.80000001

    IL_0548:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_054d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0552:  ldarg.0

    IL_0553:  ldfld      class UIButton PDAAnchor::characterLeftUI

    IL_0558:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_055d:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0562:  ldc.r4     1.

    IL_0567:  ldc.r4     1.

    IL_056c:  ldc.r4     1.

    IL_0571:  ldc.r4     0.40000001

    IL_0576:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustRGBAColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32)

    IL_057b:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_0580:  ldarg.0

    IL_0581:  ldfld      class UIButton PDAAnchor::characterRightUI

    IL_0586:  ldc.r4     0.1

    IL_058b:  stfld      float32 UIButtonColor::duration

    IL_0590:  ldarg.0

    IL_0591:  ldfld      class UIButton PDAAnchor::characterRightUI

    IL_0596:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_059b:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_05a0:  ldc.r4     1.

    IL_05a5:  ldc.r4     1.

    IL_05aa:  ldc.r4     0.80000001

    IL_05af:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_05b4:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_05b9:  ldarg.0

    IL_05ba:  ldfld      class UIButton PDAAnchor::characterRightUI

    IL_05bf:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_05c4:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_05c9:  ldc.r4     1.

    IL_05ce:  ldc.r4     1.

    IL_05d3:  ldc.r4     0.80000001

    IL_05d8:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_05dd:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_05e2:  ldarg.0

    IL_05e3:  ldfld      class UIButton PDAAnchor::characterRightUI

    IL_05e8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_05ed:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_05f2:  ldc.r4     1.

    IL_05f7:  ldc.r4     1.

    IL_05fc:  ldc.r4     1.

    IL_0601:  ldc.r4     0.40000001

    IL_0606:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustRGBAColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32)

    IL_060b:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_0610:  ldarg.0

    IL_0611:  ldfld      class UISlicedSprite PDAAnchor::spendButtonBG

    IL_0616:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_061b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0620:  ldarg.0

    IL_0621:  ldfld      class UILabel PDAAnchor::spendButtonText

    IL_0626:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_062b:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0630:  ldarg.0

    IL_0631:  ldfld      class UIButton PDAAnchor::spendButtonUI

    IL_0636:  ldc.r4     0.1

    IL_063b:  stfld      float32 UIButtonColor::duration

    IL_0640:  ldarg.0

    IL_0641:  ldfld      class UIButton PDAAnchor::spendButtonUI

    IL_0646:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_064b:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0650:  ldc.r4     1.

    IL_0655:  ldc.r4     1.

    IL_065a:  ldc.r4     0.80000001

    IL_065f:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0664:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0669:  ldarg.0

    IL_066a:  ldfld      class UIButton PDAAnchor::spendButtonUI

    IL_066f:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0674:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0679:  ldc.r4     1.

    IL_067e:  ldc.r4     1.

    IL_0683:  ldc.r4     0.80000001

    IL_0688:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_068d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0692:  ldarg.0

    IL_0693:  ldfld      class UIButton PDAAnchor::spendButtonUI

    IL_0698:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GREY_COLOR

    IL_069d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_06a2:  ldarg.0

    IL_06a3:  ldfld      class UILabel PDAAnchor::gearSelectNameText

    IL_06a8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_06ad:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_06b2:  ldarg.0

    IL_06b3:  ldfld      class UISlicedSprite PDAAnchor::gearSelectThumbnailOutline

    IL_06b8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_06bd:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_06c2:  ldarg.0

    IL_06c3:  ldfld      class UISprite PDAAnchor::gearCharacterLeft

    IL_06c8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_06cd:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_06d2:  ldarg.0

    IL_06d3:  ldfld      class UISprite PDAAnchor::gearCharacterRight

    IL_06d8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_06dd:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_06e2:  ldarg.0

    IL_06e3:  ldfld      class UIButton PDAAnchor::gearCharacterLeftUI

    IL_06e8:  ldc.r4     0.1

    IL_06ed:  stfld      float32 UIButtonColor::duration

    IL_06f2:  ldarg.0

    IL_06f3:  ldfld      class UIButton PDAAnchor::gearCharacterLeftUI

    IL_06f8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_06fd:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0702:  ldc.r4     1.

    IL_0707:  ldc.r4     1.

    IL_070c:  ldc.r4     0.80000001

    IL_0711:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0716:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_071b:  ldarg.0

    IL_071c:  ldfld      class UIButton PDAAnchor::gearCharacterLeftUI

    IL_0721:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0726:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_072b:  ldc.r4     1.

    IL_0730:  ldc.r4     1.

    IL_0735:  ldc.r4     0.80000001

    IL_073a:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_073f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0744:  ldarg.0

    IL_0745:  ldfld      class UIButton PDAAnchor::gearCharacterLeftUI

    IL_074a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_074f:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0754:  ldc.r4     1.

    IL_0759:  ldc.r4     1.

    IL_075e:  ldc.r4     1.

    IL_0763:  ldc.r4     0.40000001

    IL_0768:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustRGBAColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32)

    IL_076d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_0772:  ldarg.0

    IL_0773:  ldfld      class UIButton PDAAnchor::gearCharacterRightUI

    IL_0778:  ldc.r4     0.1

    IL_077d:  stfld      float32 UIButtonColor::duration

    IL_0782:  ldarg.0

    IL_0783:  ldfld      class UIButton PDAAnchor::gearCharacterRightUI

    IL_0788:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_078d:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0792:  ldc.r4     1.

    IL_0797:  ldc.r4     1.

    IL_079c:  ldc.r4     0.80000001

    IL_07a1:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_07a6:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_07ab:  ldarg.0

    IL_07ac:  ldfld      class UIButton PDAAnchor::gearCharacterRightUI

    IL_07b1:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_07b6:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_07bb:  ldc.r4     1.

    IL_07c0:  ldc.r4     1.

    IL_07c5:  ldc.r4     0.80000001

    IL_07ca:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_07cf:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_07d4:  ldarg.0

    IL_07d5:  ldfld      class UIButton PDAAnchor::gearCharacterRightUI

    IL_07da:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_07df:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_07e4:  ldc.r4     1.

    IL_07e9:  ldc.r4     1.

    IL_07ee:  ldc.r4     1.

    IL_07f3:  ldc.r4     0.40000001

    IL_07f8:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustRGBAColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32)

    IL_07fd:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_0802:  ldarg.0

    IL_0803:  ldfld      class UISprite PDAAnchor::gearGlow

    IL_0808:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_080d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0812:  ldarg.0

    IL_0813:  ldfld      class UISprite PDAAnchor::gearInventoryTab

    IL_0818:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_081d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0822:  ldarg.0

    IL_0823:  ldfld      class UISprite PDAAnchor::gearInventorySelected

    IL_0828:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_082d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0832:  ldarg.0

    IL_0833:  ldfld      class UIButton PDAAnchor::gearInventoryTabUI

    IL_0838:  ldc.r4     0.1

    IL_083d:  stfld      float32 UIButtonColor::duration

    IL_0842:  ldarg.0

    IL_0843:  ldfld      class UIButton PDAAnchor::gearInventoryTabUI

    IL_0848:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_084d:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0852:  ldc.r4     1.

    IL_0857:  ldc.r4     1.

    IL_085c:  ldc.r4     0.80000001

    IL_0861:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0866:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_086b:  ldarg.0

    IL_086c:  ldfld      class UIButton PDAAnchor::gearInventoryTabUI

    IL_0871:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0876:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_087b:  ldc.r4     1.

    IL_0880:  ldc.r4     1.

    IL_0885:  ldc.r4     0.80000001

    IL_088a:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_088f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0894:  ldarg.0

    IL_0895:  ldfld      class UILabel PDAAnchor::gearInventoryText

    IL_089a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_089f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_08a4:  ldarg.0

    IL_08a5:  ldfld      class UISprite PDAAnchor::gearCyberwareTab

    IL_08aa:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_08af:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_08b4:  ldarg.0

    IL_08b5:  ldfld      class UISprite PDAAnchor::gearCyberwareSelected

    IL_08ba:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_08bf:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_08c4:  ldarg.0

    IL_08c5:  ldfld      class UIButton PDAAnchor::gearCyberwareTabUI

    IL_08ca:  ldc.r4     0.1

    IL_08cf:  stfld      float32 UIButtonColor::duration

    IL_08d4:  ldarg.0

    IL_08d5:  ldfld      class UIButton PDAAnchor::gearCyberwareTabUI

    IL_08da:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_08df:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_08e4:  ldc.r4     1.

    IL_08e9:  ldc.r4     1.

    IL_08ee:  ldc.r4     0.80000001

    IL_08f3:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_08f8:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_08fd:  ldarg.0

    IL_08fe:  ldfld      class UIButton PDAAnchor::gearCyberwareTabUI

    IL_0903:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0908:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_090d:  ldc.r4     1.

    IL_0912:  ldc.r4     1.

    IL_0917:  ldc.r4     0.80000001

    IL_091c:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0921:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0926:  ldarg.0

    IL_0927:  ldfld      class UILabel PDAAnchor::gearCyberwareText

    IL_092c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLUE_COLOR

    IL_0931:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0936:  ldarg.0

    IL_0937:  ldfld      class UISlicedSprite PDAAnchor::restartLevelButtonBG

    IL_093c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0941:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0946:  ldarg.0

    IL_0947:  ldfld      class UILabel PDAAnchor::restartLevelButtonText

    IL_094c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_0951:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0956:  ldarg.0

    IL_0957:  ldfld      class UIButton PDAAnchor::restartLevelButtonUI

    IL_095c:  ldc.r4     0.1

    IL_0961:  stfld      float32 UIButtonColor::duration

    IL_0966:  ldarg.0

    IL_0967:  ldfld      class UIButton PDAAnchor::restartLevelButtonUI

    IL_096c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0971:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0976:  ldc.r4     1.

    IL_097b:  ldc.r4     1.

    IL_0980:  ldc.r4     0.80000001

    IL_0985:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_098a:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_098f:  ldarg.0

    IL_0990:  ldfld      class UIButton PDAAnchor::restartLevelButtonUI

    IL_0995:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_099a:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_099f:  ldc.r4     1.

    IL_09a4:  ldc.r4     1.

    IL_09a9:  ldc.r4     0.80000001

    IL_09ae:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_09b3:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_09b8:  ldarg.0

    IL_09b9:  ldfld      class UIButton PDAAnchor::restartLevelButtonUI

    IL_09be:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GREY_COLOR

    IL_09c3:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButton::disabledColor

    IL_09c8:  ldarg.0

    IL_09c9:  ldfld      class UISlicedSprite PDAAnchor::saveGameButtonBG

    IL_09ce:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_09d3:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_09d8:  ldarg.0

    IL_09d9:  ldfld      class UILabel PDAAnchor::saveGameButtonText

    IL_09de:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_09e3:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_09e8:  ldarg.0

    IL_09e9:  ldfld      class UIButton PDAAnchor::saveGameButtonUI

    IL_09ee:  ldc.r4     0.1

    IL_09f3:  stfld      float32 UIButtonColor::duration

    IL_09f8:  ldarg.0

    IL_09f9:  ldfld      class UIButton PDAAnchor::saveGameButtonUI

    IL_09fe:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0a03:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0a08:  ldc.r4     1.

    IL_0a0d:  ldc.r4     1.

    IL_0a12:  ldc.r4     0.80000001

    IL_0a17:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0a1c:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0a21:  ldarg.0

    IL_0a22:  ldfld      class UIButton PDAAnchor::saveGameButtonUI

    IL_0a27:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0a2c:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0a31:  ldc.r4     1.

    IL_0a36:  ldc.r4     1.

    IL_0a3b:  ldc.r4     0.80000001

    IL_0a40:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0a45:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0a4a:  ldarg.0

    IL_0a4b:  ldfld      class UISlicedSprite PDAAnchor::loadGameButtonBG

    IL_0a50:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0a55:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0a5a:  ldarg.0

    IL_0a5b:  ldfld      class UILabel PDAAnchor::loadGameButtonText

    IL_0a60:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_0a65:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0a6a:  ldarg.0

    IL_0a6b:  ldfld      class UIButton PDAAnchor::loadGameButtonUI

    IL_0a70:  ldc.r4     0.1

    IL_0a75:  stfld      float32 UIButtonColor::duration

    IL_0a7a:  ldarg.0

    IL_0a7b:  ldfld      class UIButton PDAAnchor::loadGameButtonUI

    IL_0a80:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0a85:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0a8a:  ldc.r4     1.

    IL_0a8f:  ldc.r4     1.

    IL_0a94:  ldc.r4     0.80000001

    IL_0a99:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0a9e:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0aa3:  ldarg.0

    IL_0aa4:  ldfld      class UIButton PDAAnchor::loadGameButtonUI

    IL_0aa9:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0aae:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0ab3:  ldc.r4     1.

    IL_0ab8:  ldc.r4     1.

    IL_0abd:  ldc.r4     0.80000001

    IL_0ac2:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0ac7:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0acc:  ldarg.0

    IL_0acd:  ldfld      class UISlicedSprite PDAAnchor::mainMenuButtonBG

    IL_0ad2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0ad7:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0adc:  ldarg.0

    IL_0add:  ldfld      class UILabel PDAAnchor::mainMenuButtonText

    IL_0ae2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_0ae7:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0aec:  ldarg.0

    IL_0aed:  ldfld      class UIButton PDAAnchor::mainMenuButtonUI

    IL_0af2:  ldc.r4     0.1

    IL_0af7:  stfld      float32 UIButtonColor::duration

    IL_0afc:  ldarg.0

    IL_0afd:  ldfld      class UIButton PDAAnchor::mainMenuButtonUI

    IL_0b02:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0b07:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b0c:  ldc.r4     1.

    IL_0b11:  ldc.r4     1.

    IL_0b16:  ldc.r4     0.80000001

    IL_0b1b:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0b20:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0b25:  ldarg.0

    IL_0b26:  ldfld      class UIButton PDAAnchor::mainMenuButtonUI

    IL_0b2b:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_0b30:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b35:  ldc.r4     1.

    IL_0b3a:  ldc.r4     1.

    IL_0b3f:  ldc.r4     0.80000001

    IL_0b44:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0b49:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0b4e:  ldarg.0

    IL_0b4f:  ldfld      class UILabel PDAAnchor::globalDifficultyText

    IL_0b54:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_0b59:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b5e:  ldarg.0

    IL_0b5f:  ldfld      class UILabel PDAAnchor::globalDifficultyExplanationText

    IL_0b64:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GREY_COLOR

    IL_0b69:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b6e:  ldc.r4     1.

    IL_0b73:  ldc.r4     1.

    IL_0b78:  ldc.r4     1.

    IL_0b7d:  ldc.r4     0.30000001

    IL_0b82:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustRGBAColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32,

                                                                                             float32)

    IL_0b87:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b8c:  ldarg.0

    IL_0b8d:  ldfld      class UISlicedSprite PDAAnchor::videoBG

    IL_0b92:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_0b97:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0b9c:  ldarg.0

    IL_0b9d:  ldfld      class UISlicedSprite PDAAnchor::videoBG

    IL_0ba2:  ldc.r4     0.55000001

    IL_0ba7:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_0bac:  ldarg.0

    IL_0bad:  ldfld      class UILabel PDAAnchor::texturesText

    IL_0bb2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0bb7:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0bbc:  ldarg.0

    IL_0bbd:  ldfld      class UISprite PDAAnchor::texturesCheckboxBG

    IL_0bc2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0bc7:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0bcc:  ldarg.0

    IL_0bcd:  ldfld      class UISprite PDAAnchor::texturesCheckmark

    IL_0bd2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0bd7:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0bdc:  ldarg.0

    IL_0bdd:  ldfld      class UIButton PDAAnchor::texturesCheckboxUI

    IL_0be2:  ldc.r4     0.1

    IL_0be7:  stfld      float32 UIButtonColor::duration

    IL_0bec:  ldarg.0

    IL_0bed:  ldfld      class UIButton PDAAnchor::texturesCheckboxUI

    IL_0bf2:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0bf7:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0bfc:  ldc.r4     1.

    IL_0c01:  ldc.r4     1.

    IL_0c06:  ldc.r4     0.80000001

    IL_0c0b:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0c10:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0c15:  ldarg.0

    IL_0c16:  ldfld      class UIButton PDAAnchor::texturesCheckboxUI

    IL_0c1b:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0c20:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0c25:  ldc.r4     1.

    IL_0c2a:  ldc.r4     1.

    IL_0c2f:  ldc.r4     0.80000001

    IL_0c34:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0c39:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0c3e:  ldarg.0

    IL_0c3f:  ldfld      class UILabel PDAAnchor::uiScaleText

    IL_0c44:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0c49:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0c4e:  ldarg.0

    IL_0c4f:  ldfld      class UISprite PDAAnchor::uiScaleCheckboxBG

    IL_0c54:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0c59:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0c5e:  ldarg.0

    IL_0c5f:  ldfld      class UISprite PDAAnchor::uiScaleCheckmark

    IL_0c64:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0c69:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0c6e:  ldarg.0

    IL_0c6f:  ldfld      class UIButton PDAAnchor::uiScaleCheckboxUI

    IL_0c74:  ldc.r4     0.1

    IL_0c79:  stfld      float32 UIButtonColor::duration

    IL_0c7e:  ldarg.0

    IL_0c7f:  ldfld      class UIButton PDAAnchor::uiScaleCheckboxUI

    IL_0c84:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0c89:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0c8e:  ldc.r4     1.

    IL_0c93:  ldc.r4     1.

    IL_0c98:  ldc.r4     0.80000001

    IL_0c9d:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0ca2:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0ca7:  ldarg.0

    IL_0ca8:  ldfld      class UIButton PDAAnchor::uiScaleCheckboxUI

    IL_0cad:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0cb2:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0cb7:  ldc.r4     1.

    IL_0cbc:  ldc.r4     1.

    IL_0cc1:  ldc.r4     0.80000001

    IL_0cc6:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0ccb:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0cd0:  ldarg.0

    IL_0cd1:  ldfld      class UILabel PDAAnchor::extraBloodText

    IL_0cd6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0cdb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0ce0:  ldarg.0

    IL_0ce1:  ldfld      class UISprite PDAAnchor::extraBloodCheckboxBG

    IL_0ce6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0ceb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0cf0:  ldarg.0

    IL_0cf1:  ldfld      class UISprite PDAAnchor::extraBloodCheckmark

    IL_0cf6:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0cfb:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d00:  ldarg.0

    IL_0d01:  ldfld      class UIButton PDAAnchor::extraBloodCheckboxUI

    IL_0d06:  ldc.r4     0.1

    IL_0d0b:  stfld      float32 UIButtonColor::duration

    IL_0d10:  ldarg.0

    IL_0d11:  ldfld      class UIButton PDAAnchor::extraBloodCheckboxUI

    IL_0d16:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0d1b:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d20:  ldc.r4     1.

    IL_0d25:  ldc.r4     1.

    IL_0d2a:  ldc.r4     0.80000001

    IL_0d2f:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0d34:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0d39:  ldarg.0

    IL_0d3a:  ldfld      class UIButton PDAAnchor::extraBloodCheckboxUI

    IL_0d3f:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0d44:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d49:  ldc.r4     1.

    IL_0d4e:  ldc.r4     1.

    IL_0d53:  ldc.r4     0.80000001

    IL_0d58:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0d5d:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0d62:  ldarg.0

    IL_0d63:  ldfld      class UILabel PDAAnchor::postProcessText

    IL_0d68:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0d6d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d72:  ldarg.0

    IL_0d73:  ldfld      class UISprite PDAAnchor::postProcessCheckboxBG

    IL_0d78:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0d7d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d82:  ldarg.0

    IL_0d83:  ldfld      class UISprite PDAAnchor::postProcessCheckmark

    IL_0d88:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0d8d:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0d92:  ldarg.0

    IL_0d93:  ldfld      class UIButton PDAAnchor::postProcessCheckboxUI

    IL_0d98:  ldc.r4     0.1

    IL_0d9d:  stfld      float32 UIButtonColor::duration

    IL_0da2:  ldarg.0

    IL_0da3:  ldfld      class UIButton PDAAnchor::postProcessCheckboxUI

    IL_0da8:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0dad:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0db2:  ldc.r4     1.

    IL_0db7:  ldc.r4     1.

    IL_0dbc:  ldc.r4     0.80000001

    IL_0dc1:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0dc6:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0dcb:  ldarg.0

    IL_0dcc:  ldfld      class UIButton PDAAnchor::postProcessCheckboxUI

    IL_0dd1:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0dd6:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0ddb:  ldc.r4     1.

    IL_0de0:  ldc.r4     1.

    IL_0de5:  ldc.r4     0.80000001

    IL_0dea:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0def:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0df4:  ldarg.0

    IL_0df5:  ldfld      class UILabel PDAAnchor::cameraText

    IL_0dfa:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0dff:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e04:  ldarg.0

    IL_0e05:  ldfld      class UISlicedSprite PDAAnchor::cameraPopupBG

    IL_0e0a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0e0f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e14:  ldarg.0

    IL_0e15:  ldfld      class UILabel PDAAnchor::cameraPopupText

    IL_0e1a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0e1f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e24:  ldarg.0

    IL_0e25:  ldfld      class UISprite PDAAnchor::cameraPopupPullDown

    IL_0e2a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0e2f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e34:  ldarg.0

    IL_0e35:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_0e3a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0e3f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::textColor

    IL_0e44:  ldarg.0

    IL_0e45:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_0e4a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_0e4f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::backgroundColor

    IL_0e54:  ldarg.0

    IL_0e55:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_0e5a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0e5f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::highlightColor

    IL_0e64:  ldarg.0

    IL_0e65:  ldfld      class UIButton PDAAnchor::cameraPopupUI

    IL_0e6a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0e6f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0e74:  ldarg.0

    IL_0e75:  ldfld      class UILabel PDAAnchor::projectionText

    IL_0e7a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0e7f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e84:  ldarg.0

    IL_0e85:  ldfld      class UISlicedSprite PDAAnchor::projectionPopupBG

    IL_0e8a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0e8f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0e94:  ldarg.0

    IL_0e95:  ldfld      class UILabel PDAAnchor::projectionPopupText

    IL_0e9a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0e9f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0ea4:  ldarg.0

    IL_0ea5:  ldfld      class UISprite PDAAnchor::projectionPopupPullDown

    IL_0eaa:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0eaf:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0eb4:  ldarg.0

    IL_0eb5:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_0eba:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0ebf:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::textColor

    IL_0ec4:  ldarg.0

    IL_0ec5:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_0eca:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_0ecf:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::backgroundColor

    IL_0ed4:  ldarg.0

    IL_0ed5:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_0eda:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0edf:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::highlightColor

    IL_0ee4:  ldarg.0

    IL_0ee5:  ldfld      class UIButton PDAAnchor::projectionPopupUI

    IL_0eea:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0eef:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0ef4:  ldarg.0

    IL_0ef5:  ldfld      class UILabel PDAAnchor::resolutionText

    IL_0efa:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0eff:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f04:  ldarg.0

    IL_0f05:  ldfld      class UISlicedSprite PDAAnchor::resolutionPopupBG

    IL_0f0a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0f0f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f14:  ldarg.0

    IL_0f15:  ldfld      class UILabel PDAAnchor::resolutionPopupText

    IL_0f1a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0f1f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f24:  ldarg.0

    IL_0f25:  ldfld      class UISprite PDAAnchor::resolutionPopupPullDown

    IL_0f2a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0f2f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f34:  ldarg.0

    IL_0f35:  ldfld      class UIPopupList PDAAnchor::resolutionPopup

    IL_0f3a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0f3f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::textColor

    IL_0f44:  ldarg.0

    IL_0f45:  ldfld      class UIPopupList PDAAnchor::resolutionPopup

    IL_0f4a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_0f4f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::backgroundColor

    IL_0f54:  ldarg.0

    IL_0f55:  ldfld      class UIPopupList PDAAnchor::resolutionPopup

    IL_0f5a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0f5f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::highlightColor

    IL_0f64:  ldarg.0

    IL_0f65:  ldfld      class UIButton PDAAnchor::resolutionPopupUI

    IL_0f6a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0f6f:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_0f74:  ldarg.0

    IL_0f75:  ldfld      class UILabel PDAAnchor::fullscreenText

    IL_0f7a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_0f7f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f84:  ldarg.0

    IL_0f85:  ldfld      class UISprite PDAAnchor::fullscreenCheckboxBG

    IL_0f8a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_0f8f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0f94:  ldarg.0

    IL_0f95:  ldfld      class UISprite PDAAnchor::fullscreenCheckmark

    IL_0f9a:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_0f9f:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_0fa4:  ldarg.0

    IL_0fa5:  ldfld      class UIButton PDAAnchor::fullscreenCheckboxUI

    IL_0faa:  ldc.r4     0.1

    IL_0faf:  stfld      float32 UIButtonColor::duration

    IL_0fb4:  ldarg.0

    IL_0fb5:  ldfld      class UIButton PDAAnchor::fullscreenCheckboxUI

    IL_0fba:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0fbf:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0fc4:  ldc.r4     1.

    IL_0fc9:  ldc.r4     1.

    IL_0fce:  ldc.r4     0.80000001

    IL_0fd3:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_0fd8:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_0fdd:  ldarg.0

    IL_0fde:  ldfld      class UIButton PDAAnchor::fullscreenCheckboxUI

    IL_0fe3:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_0fe8:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_0fed:  ldc.r4     1.

    IL_0ff2:  ldc.r4     1.

    IL_0ff7:  ldc.r4     0.80000001

    IL_0ffc:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1001:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1006:  ldarg.0

    IL_1007:  ldfld      class UISlicedSprite PDAAnchor::audioBG

    IL_100c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_1011:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1016:  ldarg.0

    IL_1017:  ldfld      class UISlicedSprite PDAAnchor::audioBG

    IL_101c:  ldc.r4     0.55000001

    IL_1021:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1026:  ldarg.0

    IL_1027:  ldfld      class UILabel PDAAnchor::masterVolumeText

    IL_102c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1031:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1036:  ldarg.0

    IL_1037:  ldfld      class UISlicedSprite PDAAnchor::masterVolumeSliderBG

    IL_103c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1041:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1046:  ldarg.0

    IL_1047:  ldfld      class UISlicedSprite PDAAnchor::masterVolumeSliderFG

    IL_104c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_1051:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1056:  ldarg.0

    IL_1057:  ldfld      class UISlicedSprite PDAAnchor::masterVolumeSliderFG

    IL_105c:  ldc.r4     0.5

    IL_1061:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1066:  ldarg.0

    IL_1067:  ldfld      class UISprite PDAAnchor::masterVolumeSliderThumb

    IL_106c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1071:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1076:  ldarg.0

    IL_1077:  ldfld      class UILabel PDAAnchor::musicVolumeText

    IL_107c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1081:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1086:  ldarg.0

    IL_1087:  ldfld      class UISlicedSprite PDAAnchor::musicVolumeSliderBG

    IL_108c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1091:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1096:  ldarg.0

    IL_1097:  ldfld      class UISlicedSprite PDAAnchor::musicVolumeSliderFG

    IL_109c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_10a1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_10a6:  ldarg.0

    IL_10a7:  ldfld      class UISlicedSprite PDAAnchor::musicVolumeSliderFG

    IL_10ac:  ldc.r4     0.5

    IL_10b1:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_10b6:  ldarg.0

    IL_10b7:  ldfld      class UISprite PDAAnchor::musicVolumeSliderThumb

    IL_10bc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_10c1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_10c6:  ldarg.0

    IL_10c7:  ldfld      class UILabel PDAAnchor::ambientVolumeText

    IL_10cc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_10d1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_10d6:  ldarg.0

    IL_10d7:  ldfld      class UISlicedSprite PDAAnchor::ambientVolumeSliderBG

    IL_10dc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_10e1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_10e6:  ldarg.0

    IL_10e7:  ldfld      class UISlicedSprite PDAAnchor::ambientVolumeSliderFG

    IL_10ec:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_10f1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_10f6:  ldarg.0

    IL_10f7:  ldfld      class UISlicedSprite PDAAnchor::ambientVolumeSliderFG

    IL_10fc:  ldc.r4     0.5

    IL_1101:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1106:  ldarg.0

    IL_1107:  ldfld      class UISprite PDAAnchor::ambientVolumeSliderThumb

    IL_110c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1111:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1116:  ldarg.0

    IL_1117:  ldfld      class UILabel PDAAnchor::soundVolumeText

    IL_111c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1121:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1126:  ldarg.0

    IL_1127:  ldfld      class UISlicedSprite PDAAnchor::soundVolumeSliderBG

    IL_112c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1131:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1136:  ldarg.0

    IL_1137:  ldfld      class UISlicedSprite PDAAnchor::soundVolumeSliderFG

    IL_113c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_1141:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1146:  ldarg.0

    IL_1147:  ldfld      class UISlicedSprite PDAAnchor::soundVolumeSliderFG

    IL_114c:  ldc.r4     0.5

    IL_1151:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1156:  ldarg.0

    IL_1157:  ldfld      class UISprite PDAAnchor::soundVolumeSliderThumb

    IL_115c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1161:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1166:  ldarg.0

    IL_1167:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_116c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_1171:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1176:  ldarg.0

    IL_1177:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_117c:  ldc.r4     0.55000001

    IL_1181:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1186:  ldarg.0

    IL_1187:  ldfld      class UILabel PDAAnchor::textSpeedText

    IL_118c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1191:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1196:  ldarg.0

    IL_1197:  ldfld      class UISlicedSprite PDAAnchor::textSpeedPopupBG

    IL_119c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_11a1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_11a6:  ldarg.0

    IL_11a7:  ldfld      class UILabel PDAAnchor::textSpeedPopupText

    IL_11ac:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_11b1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_11b6:  ldarg.0

    IL_11b7:  ldfld      class UISprite PDAAnchor::textSpeedPopupPullDown

    IL_11bc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_11c1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_11c6:  ldarg.0

    IL_11c7:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_11cc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_11d1:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::textColor

    IL_11d6:  ldarg.0

    IL_11d7:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_11dc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_11e1:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::backgroundColor

    IL_11e6:  ldarg.0

    IL_11e7:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_11ec:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_11f1:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::highlightColor

    IL_11f6:  ldarg.0

    IL_11f7:  ldfld      class UIButton PDAAnchor::textSpeedPopupUI

    IL_11fc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_1201:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1206:  ldarg.0

    IL_1207:  ldfld      class UILabel PDAAnchor::inputTypeText

    IL_120c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1211:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1216:  ldarg.0

    IL_1217:  ldfld      class UISlicedSprite PDAAnchor::inputTypePopupBG

    IL_121c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1221:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1226:  ldarg.0

    IL_1227:  ldfld      class UILabel PDAAnchor::inputTypePopupText

    IL_122c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1231:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1236:  ldarg.0

    IL_1237:  ldfld      class UISprite PDAAnchor::inputTypePopupPullDown

    IL_123c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1241:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1246:  ldarg.0

    IL_1247:  ldfld      class UIPopupList PDAAnchor::inputTypePopup

    IL_124c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_1251:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::textColor

    IL_1256:  ldarg.0

    IL_1257:  ldfld      class UIPopupList PDAAnchor::inputTypePopup

    IL_125c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_BLUE_COLOR

    IL_1261:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::backgroundColor

    IL_1266:  ldarg.0

    IL_1267:  ldfld      class UIPopupList PDAAnchor::inputTypePopup

    IL_126c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1271:  stfld      valuetype [UnityEngine]UnityEngine.Color UIPopupList::highlightColor

    IL_1276:  ldarg.0

    IL_1277:  ldfld      class UIButton PDAAnchor::inputTypePopupUI

    IL_127c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_1281:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1286:  ldarg.0

    IL_1287:  ldfld      class UILabel PDAAnchor::cameraEdgeText

    IL_128c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::LIGHT_BLUE_COLOR

    IL_1291:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1296:  ldarg.0

    IL_1297:  ldfld      class UISprite PDAAnchor::cameraEdgeCheckboxBG

    IL_129c:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_12a1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_12a6:  ldarg.0

    IL_12a7:  ldfld      class UISprite PDAAnchor::cameraEdgeCheckmark

    IL_12ac:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::GOLD_COLOR

    IL_12b1:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_12b6:  ldarg.0

    IL_12b7:  ldfld      class UIButton PDAAnchor::cameraEdgeCheckboxUI

    IL_12bc:  ldc.r4     0.1

    IL_12c1:  stfld      float32 UIButtonColor::duration

    IL_12c6:  ldarg.0

    IL_12c7:  ldfld      class UIButton PDAAnchor::cameraEdgeCheckboxUI

    IL_12cc:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_12d1:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_12d6:  ldc.r4     1.

    IL_12db:  ldc.r4     1.

    IL_12e0:  ldc.r4     0.80000001

    IL_12e5:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_12ea:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_12ef:  ldarg.0

    IL_12f0:  ldfld      class UIButton PDAAnchor::cameraEdgeCheckboxUI

    IL_12f5:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_12fa:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_12ff:  ldc.r4     1.

    IL_1304:  ldc.r4     1.

    IL_1309:  ldc.r4     0.80000001

    IL_130e:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1313:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1318:  ldarg.0

    IL_1319:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_131e:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1323:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Clear()

    IL_1328:  ldarg.0

    IL_1329:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_132e:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1333:  ldstr      "Free"

    IL_1338:  call       string Localize.Strings::T(string)

    IL_133d:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_1342:  ldarg.0

    IL_1343:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1348:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_134d:  ldstr      "Locked"

    IL_1352:  call       string Localize.Strings::T(string)

    IL_1357:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_135c:  ldarg.0

    IL_135d:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1362:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1367:  ldstr      "Auto"

    IL_136c:  call       string Localize.Strings::T(string)

    IL_1371:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_1376:  ldarg.0

    IL_1377:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_137c:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1381:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Clear()

    IL_1386:  ldarg.0

    IL_1387:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_138c:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1391:  ldstr      "Orthographic"

    IL_1396:  call       string Localize.Strings::T(string)

    IL_139b:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_13a0:  ldarg.0

    IL_13a1:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_13a6:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_13ab:  ldstr      "Perspective"

    IL_13b0:  call       string Localize.Strings::T(string)

    IL_13b5:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_13ba:  ldarg.0

    IL_13bb:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_13c0:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_13c5:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Clear()

    IL_13ca:  ldarg.0

    IL_13cb:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_13d0:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_13d5:  ldstr      "Slowest"

    IL_13da:  call       string Localize.Strings::T(string)

    IL_13df:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_13e4:  ldarg.0

    IL_13e5:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_13ea:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_13ef:  ldstr      "Slow"

    IL_13f4:  call       string Localize.Strings::T(string)

    IL_13f9:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_13fe:  ldarg.0

    IL_13ff:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_1404:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1409:  ldstr      "Normal"

    IL_140e:  call       string Localize.Strings::T(string)

    IL_1413:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_1418:  ldarg.0

    IL_1419:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_141e:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_1423:  ldstr      "Fast"

    IL_1428:  call       string Localize.Strings::T(string)

    IL_142d:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_1432:  ldarg.0

    IL_1433:  ldfld      class UIPopupList PDAAnchor::textSpeedPopup

    IL_1438:  ldfld      class [mscorlib]System.Collections.Generic.List`1<string> UIPopupList::items

    IL_143d:  ldstr      "Fastest"

    IL_1442:  call       string Localize.Strings::T(string)

    IL_1447:  callvirt   instance void class [mscorlib]System.Collections.Generic.List`1<string>::Add(!0)

    IL_144c:  ldarg.0

    IL_144d:  ldfld      class UILabel PDAAnchor::objectivesTabText

    IL_1452:  ldstr      "Objectives"

    IL_1457:  call       string Localize.Strings::T(string)

    IL_145c:  callvirt   instance void UILabel::set_text(string)

    IL_1461:  ldarg.0

    IL_1462:  ldfld      class UILabel PDAAnchor::itemsTabText

    IL_1467:  ldstr      "Mission Items"

    IL_146c:  call       string Localize.Strings::T(string)

    IL_1471:  callvirt   instance void UILabel::set_text(string)

    IL_1476:  ldarg.0

    IL_1477:  ldfld      class UILabel PDAAnchor::gearInventoryText

    IL_147c:  ldstr      "Inventory"

    IL_1481:  call       string Localize.Strings::T(string)

    IL_1486:  callvirt   instance void UILabel::set_text(string)

    IL_148b:  ldarg.0

    IL_148c:  ldfld      class UILabel PDAAnchor::gearCyberwareText

    IL_1491:  ldstr      "Cyberware"

    IL_1496:  call       string Localize.Strings::T(string)

    IL_149b:  callvirt   instance void UILabel::set_text(string)

    IL_14a0:  ldarg.0

    IL_14a1:  ldfld      class UILabel PDAAnchor::raceText

    IL_14a6:  ldstr      "Race:"

    IL_14ab:  call       string Localize.Strings::T(string)

    IL_14b0:  callvirt   instance void UILabel::set_text(string)

    IL_14b5:  ldarg.0

    IL_14b6:  ldfld      class UILabel PDAAnchor::genderText

    IL_14bb:  ldstr      "Gender:"

    IL_14c0:  call       string Localize.Strings::T(string)

    IL_14c5:  callvirt   instance void UILabel::set_text(string)

    IL_14ca:  ldarg.0

    IL_14cb:  ldfld      class UILabel PDAAnchor::roleText

    IL_14d0:  ldstr      "Role:"

    IL_14d5:  call       string Localize.Strings::T(string)

    IL_14da:  callvirt   instance void UILabel::set_text(string)

    IL_14df:  ldarg.0

    IL_14e0:  ldfld      class UILabel PDAAnchor::restartLevelButtonText

    IL_14e5:  ldstr      "Restart Level"

    IL_14ea:  call       string Localize.Strings::T(string)

    IL_14ef:  callvirt   instance void UILabel::set_text(string)

    IL_14f4:  ldarg.0

    IL_14f5:  ldfld      class UILabel PDAAnchor::loadGameButtonText

    IL_14fa:  ldstr      "Load Game"

    IL_14ff:  call       string Localize.Strings::T(string)

    IL_1504:  callvirt   instance void UILabel::set_text(string)

    IL_1509:  ldarg.0

    IL_150a:  ldfld      class UILabel PDAAnchor::saveGameButtonText

    IL_150f:  ldstr      "Save Game"

    IL_1514:  call       string Localize.Strings::T(string)

    IL_1519:  callvirt   instance void UILabel::set_text(string)

    IL_151e:  ldarg.0

    IL_151f:  ldfld      class UILabel PDAAnchor::mainMenuButtonText

    IL_1524:  ldstr      "Exit to Main Menu"

    IL_1529:  call       string Localize.Strings::T(string)

    IL_152e:  callvirt   instance void UILabel::set_text(string)

    IL_1533:  ldarg.0

    IL_1534:  ldfld      class UILabel PDAAnchor::texturesText

    IL_1539:  ldstr      "HD TEXTURES"

    IL_153e:  call       string Localize.Strings::T(string)

    IL_1543:  callvirt   instance void UILabel::set_text(string)

    IL_1548:  ldarg.0

    IL_1549:  ldfld      class UILabel PDAAnchor::uiScaleText

    IL_154e:  ldstr      "HI RES SCALE MODE"

    IL_1553:  call       string Localize.Strings::T(string)

    IL_1558:  callvirt   instance void UILabel::set_text(string)

    IL_155d:  ldarg.0

    IL_155e:  ldfld      class UILabel PDAAnchor::cameraText

    IL_1563:  ldstr      "CAMERA MODE"

    IL_1568:  call       string Localize.Strings::T(string)

    IL_156d:  callvirt   instance void UILabel::set_text(string)

    IL_1572:  ldarg.0

    IL_1573:  ldfld      class UILabel PDAAnchor::projectionText

    IL_1578:  ldstr      "PROJECTION MODE"

    IL_157d:  call       string Localize.Strings::T(string)

    IL_1582:  callvirt   instance void UILabel::set_text(string)

    IL_1587:  ldarg.0

    IL_1588:  ldfld      class UILabel PDAAnchor::resolutionText

    IL_158d:  ldstr      "RESOLUTION"

    IL_1592:  call       string Localize.Strings::T(string)

    IL_1597:  callvirt   instance void UILabel::set_text(string)

    IL_159c:  ldarg.0

    IL_159d:  ldfld      class UILabel PDAAnchor::fullscreenText

    IL_15a2:  ldstr      "FULLSCREEN"

    IL_15a7:  call       string Localize.Strings::T(string)

    IL_15ac:  callvirt   instance void UILabel::set_text(string)
    
    ldarg.0
    ldfld      class UILabel PDAAnchor::postProcessText
    ldstr      "POST PROCESS"
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string)
    
    ldarg.0
    ldfld      class UILabel PDAAnchor::extraBloodText
    ldstr      "EXTRA BLOOD"
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string)

    IL_15b1:  ldarg.0

    IL_15b2:  ldfld      class UILabel PDAAnchor::masterVolumeText

    IL_15b7:  ldstr      "MASTER VOLUME"

    IL_15bc:  call       string Localize.Strings::T(string)

    IL_15c1:  callvirt   instance void UILabel::set_text(string)

    IL_15c6:  ldarg.0

    IL_15c7:  ldfld      class UILabel PDAAnchor::musicVolumeText

    IL_15cc:  ldstr      "MUSIC VOLUME"

    IL_15d1:  call       string Localize.Strings::T(string)

    IL_15d6:  callvirt   instance void UILabel::set_text(string)

    IL_15db:  ldarg.0

    IL_15dc:  ldfld      class UILabel PDAAnchor::ambientVolumeText

    IL_15e1:  ldstr      "AMBIENT VOLUME"

    IL_15e6:  call       string Localize.Strings::T(string)

    IL_15eb:  callvirt   instance void UILabel::set_text(string)

    IL_15f0:  ldarg.0

    IL_15f1:  ldfld      class UILabel PDAAnchor::soundVolumeText

    IL_15f6:  ldstr      "SOUND VOLUME"

    IL_15fb:  call       string Localize.Strings::T(string)

    IL_1600:  callvirt   instance void UILabel::set_text(string)

    IL_1605:  ldarg.0

    IL_1606:  ldfld      class UILabel PDAAnchor::textSpeedText

    IL_160b:  ldstr      "TEXT SPEED"

    IL_1610:  call       string Localize.Strings::T(string)

    IL_1615:  callvirt   instance void UILabel::set_text(string)

    IL_161a:  ldarg.0

    IL_161b:  ldfld      class UILabel PDAAnchor::inputTypeText

    IL_1620:  ldstr      "INPUT TYPE"

    IL_1625:  call       string Localize.Strings::T(string)

    IL_162a:  callvirt   instance void UILabel::set_text(string)

    IL_162f:  ldarg.0

    IL_1630:  ldfld      class UILabel PDAAnchor::cameraEdgeText

    IL_1635:  ldstr      "CAMERA EDGE PAN"

    IL_163a:  call       string Localize.Strings::T(string)

    IL_163f:  callvirt   instance void UILabel::set_text(string)

    IL_1644:  ldarg.0

    IL_1645:  ldfld      class UILabel PDAAnchor::changeResolutionPopupTitleText

    IL_164a:  ldstr      "Change Resolution"

    IL_164f:  call       string Localize.Strings::T(string)

    IL_1654:  callvirt   instance void UILabel::set_text(string)

    IL_1659:  ldarg.0

    IL_165a:  ldfld      class UILabel PDAAnchor::changeResolutionPopupMessageText

    IL_165f:  ldstr      "Are you sure you want to change to the selected re"

    + "solution\? The resolution will revert back in 20 seconds if not confirmed."

    IL_1664:  call       string Localize.Strings::T(string)

    IL_1669:  callvirt   instance void UILabel::set_text(string)

    IL_166e:  ldarg.0

    IL_166f:  ldfld      class UILabel PDAAnchor::changeResolutionPopupCancelText

    IL_1674:  ldstr      "Cancel"

    IL_1679:  call       string Localize.Strings::T(string)

    IL_167e:  callvirt   instance void UILabel::set_text(string)

    IL_1683:  ldarg.0

    IL_1684:  ldfld      class UILabel PDAAnchor::changeResolutionPopupConfirmText

    IL_1689:  ldstr      "Confirm"

    IL_168e:  call       string Localize.Strings::T(string)

    IL_1693:  callvirt   instance void UILabel::set_text(string)

    IL_1698:  ldsfld     string [mscorlib]System.String::Empty

    IL_169d:  stloc.0

    IL_169e:  ldsfld     int32 Settings::GameDifficulty

    IL_16a3:  stloc.1

    IL_16a4:  ldloc.1

    IL_16a5:  ldc.i4.1

    IL_16a6:  add

    IL_16a7:  switch     ( 

                          IL_16c1,

                          IL_16d1,

                          IL_16e1,

                          IL_16f1)

    IL_16bc:  br         IL_1701



    IL_16c1:  ldstr      "Easy"

    IL_16c6:  call       string Localize.Strings::T(string)

    IL_16cb:  stloc.0

    IL_16cc:  br         IL_1701



    IL_16d1:  ldstr      "Normal"

    IL_16d6:  call       string Localize.Strings::T(string)

    IL_16db:  stloc.0

    IL_16dc:  br         IL_1701



    IL_16e1:  ldstr      "Hard"

    IL_16e6:  call       string Localize.Strings::T(string)

    IL_16eb:  stloc.0

    IL_16ec:  br         IL_1701



    IL_16f1:  ldstr      "Very Hard"

    IL_16f6:  call       string Localize.Strings::T(string)

    IL_16fb:  stloc.0

    IL_16fc:  br         IL_1701



    IL_1701:  ldarg.0

    IL_1702:  ldfld      class UILabel PDAAnchor::globalDifficultyText

    //IL_1707:  ldstr      "Game Difficulty: {0}"

    //IL_170c:  ldc.i4.1

    //IL_170d:  newarr     [mscorlib]System.Object

    //IL_1712:  dup

    //IL_1713:  ldc.i4.0

    //IL_1714:  ldloc.0

    //IL_1715:  stelem.ref

    //IL_1716:  call       string Localize.Strings::T(string,

    //                                                object[])
                                                    
    ldstr      "{0}: {1}" 
    ldstr      "Game Difficulty"
    call       string Localize.Strings::T(string)
    ldloc.0
    call       string Localize.Strings::T(string)    
    call       string [mscorlib]System.String::Format(string,
                                                       object,
                                                       object)

    IL_171b:  callvirt   instance void UILabel::set_text(string)

    IL_1720:  ldarg.0

    IL_1721:  ldfld      class UILabel PDAAnchor::globalDifficultyExplanationText

    IL_1726:  ldstr      "This option can be changed on the main menu."

    IL_172b:  call       string Localize.Strings::T(string)

    IL_1730:  callvirt   instance void UILabel::set_text(string)

    IL_1735:  ldarg.0

    IL_1736:  ldflda     class UILabel PDAAnchor::objectivesTabText

    IL_173b:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1740:  ldarg.0

    IL_1741:  ldflda     class UILabel PDAAnchor::itemsTabText

    IL_1746:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_174b:  ldarg.0

    IL_174c:  ldflda     class UILabel PDAAnchor::gearInventoryText

    IL_1751:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1756:  ldarg.0

    IL_1757:  ldflda     class UILabel PDAAnchor::gearCyberwareText

    IL_175c:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1761:  ldarg.0

    IL_1762:  ldflda     class UILabel PDAAnchor::restartLevelButtonText

    IL_1767:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_176c:  ldarg.0

    IL_176d:  ldflda     class UILabel PDAAnchor::loadGameButtonText

    IL_1772:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1777:  ldarg.0

    IL_1778:  ldflda     class UILabel PDAAnchor::saveGameButtonText

    IL_177d:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1782:  ldarg.0

    IL_1783:  ldflda     class UILabel PDAAnchor::mainMenuButtonText

    IL_1788:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_178d:  ldarg.0

    IL_178e:  ldflda     class UILabel PDAAnchor::texturesText

    IL_1793:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1798:  ldarg.0

    IL_1799:  ldflda     class UILabel PDAAnchor::uiScaleText

    IL_179e:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17a3:  ldarg.0

    IL_17a4:  ldflda     class UILabel PDAAnchor::cameraText

    IL_17a9:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17ae:  ldarg.0

    IL_17af:  ldflda     class UILabel PDAAnchor::projectionText

    IL_17b4:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17b9:  ldarg.0

    IL_17ba:  ldflda     class UILabel PDAAnchor::resolutionText

    IL_17bf:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17c4:  ldarg.0

    IL_17c5:  ldflda     class UILabel PDAAnchor::fullscreenText

    IL_17ca:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17cf:  ldarg.0

    IL_17d0:  ldflda     class UILabel PDAAnchor::masterVolumeText

    IL_17d5:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17da:  ldarg.0

    IL_17db:  ldflda     class UILabel PDAAnchor::musicVolumeText

    IL_17e0:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17e5:  ldarg.0

    IL_17e6:  ldflda     class UILabel PDAAnchor::ambientVolumeText

    IL_17eb:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17f0:  ldarg.0

    IL_17f1:  ldflda     class UILabel PDAAnchor::soundVolumeText

    IL_17f6:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_17fb:  ldarg.0

    IL_17fc:  ldflda     class UILabel PDAAnchor::textSpeedText

    IL_1801:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1806:  ldarg.0

    IL_1807:  ldflda     class UILabel PDAAnchor::inputTypePopupText

    IL_180c:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1811:  ldarg.0

    IL_1812:  ldflda     class UILabel PDAAnchor::cameraEdgeText

    IL_1817:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_181c:  ldarg.0

    IL_181d:  ldflda     class UILabel PDAAnchor::changeResolutionPopupCancelText

    IL_1822:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1827:  ldarg.0

    IL_1828:  ldflda     class UILabel PDAAnchor::changeResolutionPopupConfirmText

    IL_182d:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1832:  ldarg.0

    IL_1833:  ldflda     class UILabel PDAAnchor::globalDifficultyText

    IL_1838:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_183d:  ldarg.0

    IL_183e:  ldflda     class UILabel PDAAnchor::globalDifficultyExplanationText

    IL_1843:  call       void Utilities::AutoScaleUILabel(class UILabel&)

    IL_1848:  call       valuetype [UnityEngine]UnityEngine.RuntimePlatform [UnityEngine]UnityEngine.Application::get_platform()

    IL_184d:  ldc.i4.s   11

    IL_184f:  beq        IL_185f



    IL_1854:  call       valuetype [UnityEngine]UnityEngine.RuntimePlatform [UnityEngine]UnityEngine.Application::get_platform()

    IL_1859:  ldc.i4.8

    IL_185a:  bne.un     IL_1d08



    IL_185f:  ldarg.0

    IL_1860:  ldfld      class UILabel PDAAnchor::inputTypeText

    IL_1865:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_186a:  ldc.i4.0

    IL_186b:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1870:  ldarg.0

    IL_1871:  ldfld      class UIPopupList PDAAnchor::inputTypePopup

    IL_1876:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_187b:  ldc.i4.0

    IL_187c:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1881:  ldarg.0

    IL_1882:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_1887:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_188c:  ldarg.0

    IL_188d:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_1892:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1897:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_189c:  stloc.2

    IL_189d:  ldloca.s   V_2

    IL_189f:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_18a4:  ldarg.0

    IL_18a5:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_18aa:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_18af:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_18b4:  stloc.3

    IL_18b5:  ldloca.s   V_3

    IL_18b7:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_18bc:  ldc.r4     40.

    IL_18c1:  sub

    IL_18c2:  ldc.r4     1.

    IL_18c7:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_18cc:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localScale(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_18d1:  ldarg.0

    IL_18d2:  ldfld      class UILabel PDAAnchor::cameraEdgeText

    IL_18d7:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_18dc:  ldc.i4.0

    IL_18dd:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_18e2:  ldarg.0

    IL_18e3:  ldfld      class UICheckbox PDAAnchor::cameraEdgeCheckbox

    IL_18e8:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_18ed:  ldc.i4.0

    IL_18ee:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_18f3:  ldarg.0

    IL_18f4:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_18f9:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_18fe:  ldarg.0

    IL_18ff:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_1904:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1909:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_190e:  stloc.s    V_4

    IL_1910:  ldloca.s   V_4

    IL_1912:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1917:  ldarg.0

    IL_1918:  ldfld      class UISlicedSprite PDAAnchor::gameplayBG

    IL_191d:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1922:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_1927:  stloc.s    V_5

    IL_1929:  ldloca.s   V_5

    IL_192b:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1930:  ldc.r4     40.

    IL_1935:  sub

    IL_1936:  ldc.r4     1.

    IL_193b:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1940:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localScale(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1945:  ldarg.0

    IL_1946:  ldfld      class UIPopupList PDAAnchor::resolutionPopup

    IL_194b:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1950:  ldc.i4.0

    IL_1951:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1956:  ldarg.0

    IL_1957:  ldfld      class UILabel PDAAnchor::resolutionText

    IL_195c:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1961:  ldc.i4.0

    IL_1962:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1967:  ldarg.0

    IL_1968:  ldfld      class UISprite PDAAnchor::texturesCheckboxBG

    IL_196d:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1972:  ldc.i4.0

    IL_1973:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1978:  ldarg.0

    IL_1979:  ldfld      class UICheckbox PDAAnchor::texturesCheckbox

    IL_197e:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1983:  ldc.i4.0

    IL_1984:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1989:  ldarg.0

    IL_198a:  ldfld      class UILabel PDAAnchor::texturesText

    IL_198f:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1994:  ldc.i4.0

    IL_1995:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_199a:  ldarg.0

    IL_199b:  ldfld      class UISprite PDAAnchor::uiScaleCheckboxBG

    IL_19a0:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19a5:  ldc.i4.0

    IL_19a6:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_19ab:  ldarg.0

    IL_19ac:  ldfld      class UICheckbox PDAAnchor::uiScaleCheckbox

    IL_19b1:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19b6:  ldc.i4.0

    IL_19b7:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_19bc:  ldarg.0

    IL_19bd:  ldfld      class UILabel PDAAnchor::uiScaleText

    IL_19c2:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19c7:  ldc.i4.0

    IL_19c8:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_19cd:  ldarg.0

    IL_19ce:  ldfld      class UICheckbox PDAAnchor::fullscreenCheckbox

    IL_19d3:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19d8:  ldc.i4.0

    IL_19d9:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_19de:  ldarg.0

    IL_19df:  ldfld      class UISprite PDAAnchor::fullscreenCheckboxBG

    IL_19e4:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19e9:  ldc.i4.0

    IL_19ea:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_19ef:  ldarg.0

    IL_19f0:  ldfld      class UILabel PDAAnchor::fullscreenText

    IL_19f5:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_19fa:  ldc.i4.0

    IL_19fb:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a00:  ldarg.0

    IL_1a01:  ldfld      class UICheckbox PDAAnchor::extraBloodCheckbox

    IL_1a06:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a0b:  ldc.i4.0

    IL_1a0c:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a11:  ldarg.0

    IL_1a12:  ldfld      class UISprite PDAAnchor::extraBloodCheckboxBG

    IL_1a17:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a1c:  ldc.i4.0

    IL_1a1d:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a22:  ldarg.0

    IL_1a23:  ldfld      class UILabel PDAAnchor::extraBloodText

    IL_1a28:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a2d:  ldc.i4.0

    IL_1a2e:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a33:  ldarg.0

    IL_1a34:  ldfld      class UICheckbox PDAAnchor::postProcessCheckbox

    IL_1a39:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a3e:  ldc.i4.0

    IL_1a3f:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a44:  ldarg.0

    IL_1a45:  ldfld      class UISprite PDAAnchor::postProcessCheckboxBG

    IL_1a4a:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a4f:  ldc.i4.0

    IL_1a50:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a55:  ldarg.0

    IL_1a56:  ldfld      class UILabel PDAAnchor::postProcessText

    IL_1a5b:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_1a60:  ldc.i4.0

    IL_1a61:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1a66:  ldarg.0

    IL_1a67:  ldfld      class UILabel PDAAnchor::cameraText

    IL_1a6c:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1a71:  ldarg.0

    IL_1a72:  ldfld      class UILabel PDAAnchor::cameraText

    IL_1a77:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1a7c:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1a81:  stloc.s    V_6

    IL_1a83:  ldloca.s   V_6

    IL_1a85:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1a8a:  ldarg.0

    IL_1a8b:  ldfld      class UILabel PDAAnchor::cameraText

    IL_1a90:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1a95:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1a9a:  stloc.s    V_7

    IL_1a9c:  ldloca.s   V_7

    IL_1a9e:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1aa3:  ldc.r4     40.

    IL_1aa8:  add

    IL_1aa9:  ldarg.0

    IL_1aaa:  ldfld      class UILabel PDAAnchor::cameraText

    IL_1aaf:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1ab4:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1ab9:  stloc.s    V_8

    IL_1abb:  ldloca.s   V_8

    IL_1abd:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1ac2:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1ac7:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1acc:  ldarg.0

    IL_1acd:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1ad2:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1ad7:  ldarg.0

    IL_1ad8:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1add:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1ae2:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1ae7:  stloc.s    V_9

    IL_1ae9:  ldloca.s   V_9

    IL_1aeb:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1af0:  ldarg.0

    IL_1af1:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1af6:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1afb:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1b00:  stloc.s    V_10

    IL_1b02:  ldloca.s   V_10

    IL_1b04:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1b09:  ldc.r4     40.

    IL_1b0e:  add

    IL_1b0f:  ldarg.0

    IL_1b10:  ldfld      class UIPopupList PDAAnchor::cameraPopup

    IL_1b15:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1b1a:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1b1f:  stloc.s    V_11

    IL_1b21:  ldloca.s   V_11

    IL_1b23:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1b28:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1b2d:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1b32:  ldarg.0

    IL_1b33:  ldfld      class UILabel PDAAnchor::projectionText

    IL_1b38:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1b3d:  ldarg.0

    IL_1b3e:  ldfld      class UILabel PDAAnchor::projectionText

    IL_1b43:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1b48:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1b4d:  stloc.s    V_12

    IL_1b4f:  ldloca.s   V_12

    IL_1b51:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1b56:  ldarg.0

    IL_1b57:  ldfld      class UILabel PDAAnchor::projectionText

    IL_1b5c:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1b61:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1b66:  stloc.s    V_13

    IL_1b68:  ldloca.s   V_13

    IL_1b6a:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1b6f:  ldc.r4     40.

    IL_1b74:  add

    IL_1b75:  ldarg.0

    IL_1b76:  ldfld      class UILabel PDAAnchor::projectionText

    IL_1b7b:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1b80:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1b85:  stloc.s    V_14

    IL_1b87:  ldloca.s   V_14

    IL_1b89:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1b8e:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1b93:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1b98:  ldarg.0

    IL_1b99:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_1b9e:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1ba3:  ldarg.0

    IL_1ba4:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_1ba9:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1bae:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1bb3:  stloc.s    V_15

    IL_1bb5:  ldloca.s   V_15

    IL_1bb7:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1bbc:  ldarg.0

    IL_1bbd:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_1bc2:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1bc7:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1bcc:  stloc.s    V_16

    IL_1bce:  ldloca.s   V_16

    IL_1bd0:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1bd5:  ldc.r4     40.

    IL_1bda:  add

    IL_1bdb:  ldarg.0

    IL_1bdc:  ldfld      class UIPopupList PDAAnchor::projectionPopup

    IL_1be1:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1be6:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1beb:  stloc.s    V_17

    IL_1bed:  ldloca.s   V_17

    IL_1bef:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1bf4:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1bf9:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1bfe:  ldarg.0

    IL_1bff:  ldfld      class UISlicedSprite PDAAnchor::videoBG

    IL_1c04:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1c09:  ldarg.0

    IL_1c0a:  ldfld      class UISlicedSprite PDAAnchor::videoBG

    IL_1c0f:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1c14:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_1c19:  stloc.s    V_18

    IL_1c1b:  ldloca.s   V_18

    IL_1c1d:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1c22:  ldarg.0

    IL_1c23:  ldfld      class UISlicedSprite PDAAnchor::videoBG

    IL_1c28:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1c2d:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localScale()

    IL_1c32:  stloc.s    V_19

    IL_1c34:  ldloca.s   V_19

    IL_1c36:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1c3b:  ldc.r4     120.

    IL_1c40:  sub

    IL_1c41:  ldc.r4     1.

    IL_1c46:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1c4b:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localScale(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1c50:  ldarg.0

    IL_1c51:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::audioWidgetTransform

    IL_1c56:  ldarg.0

    IL_1c57:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::audioWidgetTransform

    IL_1c5c:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1c61:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1c66:  stloc.s    V_20

    IL_1c68:  ldloca.s   V_20

    IL_1c6a:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1c6f:  ldarg.0

    IL_1c70:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::audioWidgetTransform

    IL_1c75:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1c7a:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1c7f:  stloc.s    V_21

    IL_1c81:  ldloca.s   V_21

    IL_1c83:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1c88:  ldc.r4     120.

    IL_1c8d:  add

    IL_1c8e:  ldarg.0

    IL_1c8f:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::audioWidgetTransform

    IL_1c94:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1c99:  stloc.s    V_22

    IL_1c9b:  ldloca.s   V_22

    IL_1c9d:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1ca2:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1ca7:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1cac:  ldarg.0

    IL_1cad:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::gameplayWidgetTransform

    IL_1cb2:  ldarg.0

    IL_1cb3:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::gameplayWidgetTransform

    IL_1cb8:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1cbd:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1cc2:  stloc.s    V_23

    IL_1cc4:  ldloca.s   V_23

    IL_1cc6:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::x

    IL_1ccb:  ldarg.0

    IL_1ccc:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::gameplayWidgetTransform

    IL_1cd1:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_1cd6:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1cdb:  stloc.s    V_24

    IL_1cdd:  ldloca.s   V_24

    IL_1cdf:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::y

    IL_1ce4:  ldc.r4     120.

    IL_1ce9:  add

    IL_1cea:  ldarg.0

    IL_1ceb:  ldfld      class [UnityEngine]UnityEngine.Transform PDAAnchor::gameplayWidgetTransform

    IL_1cf0:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_1cf5:  stloc.s    V_25

    IL_1cf7:  ldloca.s   V_25

    IL_1cf9:  ldfld      float32 [UnityEngine]UnityEngine.Vector3::z

    IL_1cfe:  newobj     instance void [UnityEngine]UnityEngine.Vector3::.ctor(float32,

                                                                               float32,

                                                                               float32)

    IL_1d03:  callvirt   instance void [UnityEngine]UnityEngine.Transform::set_localPosition(valuetype [UnityEngine]UnityEngine.Vector3)

    IL_1d08:  ldarg.0

    IL_1d09:  ldfld      class UISlicedSprite PDAAnchor::changeResolutionPopupBackground

    IL_1d0e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::BLACK_COLOR

    IL_1d13:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d18:  ldarg.0

    IL_1d19:  ldfld      class UISlicedSprite PDAAnchor::changeResolutionPopupBackground

    IL_1d1e:  ldc.r4     0.5

    IL_1d23:  callvirt   instance void UIWidget::set_alpha(float32)

    IL_1d28:  ldarg.0

    IL_1d29:  ldfld      class UISprite PDAAnchor::changeResolutionPopupBG

    IL_1d2e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1d33:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d38:  ldarg.0

    IL_1d39:  ldfld      class UILabel PDAAnchor::changeResolutionPopupTitleText

    IL_1d3e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_1d43:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d48:  ldarg.0

    IL_1d49:  ldfld      class UILabel PDAAnchor::changeResolutionPopupMessageText

    IL_1d4e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1d53:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d58:  ldarg.0

    IL_1d59:  ldfld      class UILabel PDAAnchor::changeResolutionPopupTimerText

    IL_1d5e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_1d63:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d68:  ldarg.0

    IL_1d69:  ldfld      class UISprite PDAAnchor::changeResolutionPopupCancelBG

    IL_1d6e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1d73:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d78:  ldarg.0

    IL_1d79:  ldfld      class UIButton PDAAnchor::changeResolutionPopupCancelUI

    IL_1d7e:  ldc.r4     0.1

    IL_1d83:  stfld      float32 UIButtonColor::duration

    IL_1d88:  ldarg.0

    IL_1d89:  ldfld      class UIButton PDAAnchor::changeResolutionPopupCancelUI

    IL_1d8e:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1d93:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_1d98:  ldc.r4     1.

    IL_1d9d:  ldc.r4     1.

    IL_1da2:  ldc.r4     0.80000001

    IL_1da7:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1dac:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_1db1:  ldarg.0

    IL_1db2:  ldfld      class UIButton PDAAnchor::changeResolutionPopupCancelUI

    IL_1db7:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DEEP_BLUE_COLOR

    IL_1dbc:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_1dc1:  ldc.r4     1.

    IL_1dc6:  ldc.r4     1.

    IL_1dcb:  ldc.r4     0.80000001

    IL_1dd0:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1dd5:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1dda:  ldarg.0

    IL_1ddb:  ldfld      class UILabel PDAAnchor::changeResolutionPopupCancelText

    IL_1de0:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::MED_BLUE_COLOR

    IL_1de5:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1dea:  ldarg.0

    IL_1deb:  ldfld      class UISprite PDAAnchor::changeResolutionPopupConfirmBG

    IL_1df0:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_1df5:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1dfa:  ldarg.0

    IL_1dfb:  ldfld      class UIButton PDAAnchor::changeResolutionPopupConfirmUI

    IL_1e00:  ldc.r4     0.1

    IL_1e05:  stfld      float32 UIButtonColor::duration

    IL_1e0a:  ldarg.0

    IL_1e0b:  ldfld      class UIButton PDAAnchor::changeResolutionPopupConfirmUI

    IL_1e10:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_1e15:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_1e1a:  ldc.r4     1.

    IL_1e1f:  ldc.r4     1.

    IL_1e24:  ldc.r4     0.80000001

    IL_1e29:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1e2e:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::pressed

    IL_1e33:  ldarg.0

    IL_1e34:  ldfld      class UIButton PDAAnchor::changeResolutionPopupConfirmUI

    IL_1e39:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::DARK_GOLD_COLOR

    IL_1e3e:  call       valuetype [UnityEngine]UnityEngine.Color32 [UnityEngine]UnityEngine.Color32::op_Implicit(valuetype [UnityEngine]UnityEngine.Color)

    IL_1e43:  ldc.r4     1.

    IL_1e48:  ldc.r4     1.

    IL_1e4d:  ldc.r4     0.80000001

    IL_1e52:  call       valuetype [UnityEngine]UnityEngine.Color Utilities::AdjustHSBColor(valuetype [UnityEngine]UnityEngine.Color32,

                                                                                            float32,

                                                                                            float32,

                                                                                            float32)

    IL_1e57:  stfld      valuetype [UnityEngine]UnityEngine.Color UIButtonColor::hover

    IL_1e5c:  ldarg.0

    IL_1e5d:  ldfld      class UILabel PDAAnchor::changeResolutionPopupConfirmText

    IL_1e62:  ldsfld     valuetype [UnityEngine]UnityEngine.Color Constants::YELLOW_COLOR

    IL_1e67:  callvirt   instance void UIWidget::set_color(valuetype [UnityEngine]UnityEngine.Color)

    IL_1e6c:  ldarg.0

    IL_1e6d:  ldfld      class [UnityEngine]UnityEngine.GameObject PDAAnchor::changeResolutionPopup

    IL_1e72:  ldc.i4.0

    IL_1e73:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_1e78:  ret

"""
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newAwake}
        pieces.append(res)   
        
    if not tuple(s):
        print("\tCant replace pdaanchor_awake")
        
    return pieces 

# WeaponPanel::Init
# add Strings.T()
def replace_weaponpanel_init(assembly_code):
    pieces = []
    reg = rb"Init\(\) cil managed\s+\{([\*\+\\\?\&\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method WeaponPanel\:\:Init"
    newAwake = """
        // 

    .maxstack  19

    IL_0000:  ldarg.0

    IL_0001:  ldfld      bool WeaponPanel::isInit

    IL_0006:  brfalse    IL_000c



    IL_000b:  ret



    IL_000c:  ldarg.0

    IL_000d:  ldarg.0

    IL_000e:  ldfld      class [UnityEngine]UnityEngine.Transform WeaponPanel::selectionTopRoot

    IL_0013:  callvirt   instance class [UnityEngine]UnityEngine.Transform [UnityEngine]UnityEngine.Component::get_transform()

    IL_0018:  callvirt   instance valuetype [UnityEngine]UnityEngine.Vector3 [UnityEngine]UnityEngine.Transform::get_localPosition()

    IL_001d:  stfld      valuetype [UnityEngine]UnityEngine.Vector3 WeaponPanel::topAnchorPos

    IL_0022:  ldarg.0

    IL_0023:  ldc.i4.1

    IL_0024:  stfld      bool WeaponPanel::isInit

    IL_0029:  ldarg.0

    IL_002a:  ldfld      class [UnityEngine]UnityEngine.GameObject WeaponPanel::leftButtonRoot

    IL_002f:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.GameObject::get_gameObject()

    IL_0034:  ldc.i4.0

    IL_0035:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_003a:  ldarg.0

    IL_003b:  ldfld      class [UnityEngine]UnityEngine.GameObject WeaponPanel::rightButtonRoot

    IL_0040:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.GameObject::get_gameObject()

    IL_0045:  ldc.i4.0

    IL_0046:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_004b:  ldarg.0

    IL_004c:  ldc.i4.1

    IL_004d:  call       instance void WeaponPanel::Position(bool)
    
    ldarg.0
    ldfld      class UILabel WeaponPanel::reloadConfirmText
    ldarg.0
    ldfld      class UILabel WeaponPanel::reloadConfirmText
    callvirt   instance string UILabel::get_text()
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string)     

    ldarg.0
    ldfld      class UILabel WeaponPanel::reloadCancelText
    ldarg.0
    ldfld      class UILabel WeaponPanel::reloadCancelText
    callvirt   instance string UILabel::get_text()
    call       string Localize.Strings::T(string)
    callvirt   instance void UILabel::set_text(string) 
    

    IL_0052:  ldsfld     bool Settings::UseKeyboardShortcuts

    IL_0057:  brfalse    IL_0073



    IL_005c:  ldsfld     valuetype [UnityEngine]UnityEngine.KeyCode Settings::KEY_RELOAD

    IL_0061:  ldc.i4.1

    IL_0062:  ldarg.0

    IL_0063:  ldftn      instance void WeaponPanel::InputReload()

    IL_0069:  newobj     instance void InputManager/InputDelegate::.ctor(object,

                                                                         native int)

    IL_006e:  call       void InputManager::Bind(valuetype [UnityEngine]UnityEngine.KeyCode,

                                                 bool,

                                                 class InputManager/InputDelegate)
    
    IL_0073:  ret 
""" 
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newAwake}
        pieces.append(res)   
    
    if not tuple(s):
        print("\tCant replace weaponpanel_init")
    
    return pieces

# AOEMarker::Initialize
# add Strings.T()
def replace_aoemarker_initialize(assembly_code):
    pieces = []
    reg = rb"Initialize\(\) cil managed\s+\{([\*\+\%\\\?\&\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method AOEMarker\:\:Initialize"
    newAwake = r"""

    // 

    .maxstack  98

    .locals init (class TileGrid V_0,

             class Player V_1,

             float32 V_2,

             class BetterList`1<class Tile> V_3,

             class Tile[] V_4,

             bool V_5,

             bool V_6,

             class Tile V_7,

             int32 V_8,

             bool V_9,

             int32 V_10,

             class [mscorlib]System.Collections.Generic.List`1<class Player> V_11,

             int32 V_12)

    IL_0000:  ldarg.0

    IL_0001:  ldfld      class Tile AOEMarker::activeTile

    IL_0006:  brtrue     IL_000c



    IL_000b:  ret



    IL_000c:  ldarg.0

    IL_000d:  ldfld      class CameraControl AOEMarker::thisCC

    IL_0012:  ldnull

    IL_0013:  call       bool [UnityEngine]UnityEngine.Object::op_Equality(class [UnityEngine]UnityEngine.Object,

                                                                           class [UnityEngine]UnityEngine.Object)

    IL_0018:  brfalse    IL_002d



    IL_001d:  ldarg.0

    IL_001e:  call       class RunManager RunManager::get_Instance()

    IL_0023:  ldfld      class CameraControl RunManager::cameraControl

    IL_0028:  stfld      class CameraControl AOEMarker::thisCC

    IL_002d:  call       class RunManager RunManager::get_Instance()

    IL_0032:  callvirt   instance class TileGrid RunManager::get_grid()

    IL_0037:  stloc.0

    IL_0038:  ldarg.0

    IL_0039:  call       instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_003e:  ldc.i4.1

    IL_003f:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_0044:  ldarg.0

    IL_0045:  ldfld      class UILabel AOEMarker::toHitLabel

    IL_004a:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_004f:  ldc.i4.1

    IL_0050:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_0055:  call       class Player TurnDirector::get_ActivePlayer()

    IL_005a:  stloc.1

    IL_005b:  ldloc.1

    IL_005c:  ldarg.0

    IL_005d:  ldfld      class Tile AOEMarker::activeTile

    IL_0062:  ldfld      valuetype GridPoint Tile::gridPos

    IL_0067:  call       class [ShadowrunDTO]isogame.AbilityDef HUDManager::get_ActiveAbility()

    IL_006c:  call       class Item HUDManager::get_ActiveItem()

    IL_0071:  ldfld      class [ShadowrunDTO]isogame.ItemDef Item::def

    IL_0076:  call       float32 GameEffect::locationToHit(class Player,

                                                           valuetype GridPoint,

                                                           class [ShadowrunDTO]isogame.AbilityDef,

                                                           class [ShadowrunDTO]isogame.ItemDef)

    IL_007b:  stloc.2

    IL_007c:  ldloc.0

    IL_007d:  ldloc.1

    IL_007e:  callvirt   instance class Tile Player::get_ActiveTile()

    IL_0083:  ldfld      valuetype GridPoint Tile::gridPos

    IL_0088:  ldarg.0

    IL_0089:  ldfld      class Tile AOEMarker::activeTile

    IL_008e:  ldfld      valuetype GridPoint Tile::gridPos

    IL_0093:  callvirt   instance class BetterList`1<class Tile> TileGrid::GetTilesInPath(valuetype GridPoint,

                                                                                          valuetype GridPoint)

    IL_0098:  stloc.3

    IL_0099:  ldloc.3

    IL_009a:  callvirt   instance !0[] class BetterList`1<class Tile>::ToArray()

    IL_009f:  stloc.s    V_4

    IL_00a1:  ldc.i4.1

    IL_00a2:  stloc.s    V_5

    IL_00a4:  call       class Item HUDManager::get_ActiveItem()

    IL_00a9:  ldfld      class [ShadowrunDTO]isogame.ItemDef Item::def

    IL_00ae:  callvirt   instance bool [ShadowrunDTO]isogame.ItemDef::get_isMagic()

    IL_00b3:  stloc.s    V_6

    IL_00b5:  ldloc.s    V_4

    IL_00b7:  ldloc.s    V_5

    IL_00b9:  ldloc.s    V_6

    IL_00bb:  call       class Tile GameEffect::GetBestCoverTile(class Tile[],

                                                                 bool,

                                                                 bool)

    IL_00c0:  stloc.s    V_7

    IL_00c2:  ldloc.2

    IL_00c3:  ldc.r4     100.

    IL_00c8:  mul

    IL_00c9:  stloc.2

    IL_00ca:  ldloc.s    V_7

    IL_00cc:  brfalse    IL_012f



    IL_00d1:  ldarg.0

    IL_00d2:  ldfld      class UILabel AOEMarker::coverWarning

    IL_00d7:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_00dc:  ldc.i4.1

    IL_00dd:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_00e2:  ldarg.0

    IL_00e3:  ldfld      class UILabel AOEMarker::toHitLabel

    IL_00e8:  ldstr      "{0}%"

    IL_00ed:  ldloc.2

    IL_00ee:  call       int32 [UnityEngine]UnityEngine.Mathf::RoundToInt(float32)

    IL_00f3:  box        [mscorlib]System.Int32

    IL_00f8:  call       string [mscorlib]System.String::Format(string,

                                                                object)

    IL_00fd:  callvirt   instance void UILabel::set_text(string)

    IL_0102:  ldarg.0

    IL_0103:  ldfld      class UILabel AOEMarker::coverWarning

    //IL_0108:  ldstr      "{0} COVER\nINTERVENING"

    ldstr      "{0} {1}"

    IL_010d:  ldloc.s    V_7

    IL_010f:  ldloc.s    V_6

    IL_0111:  call       valuetype StatsUtil/Cover GameEffect::GetCoverOfTile(class Tile,

                                                                              bool)

    IL_0116:  box        StatsUtil/Cover

    IL_011b:  callvirt   instance string [mscorlib]System.Enum::ToString()
    
    call       string Localize.Strings::T(string)
    ldstr      "COVER\nINTERVENING"
    call       string Localize.Strings::T(string)
    call       string [mscorlib]System.String::Format(string,
                                                                object,
                                                                object)
    //IL_0120:  call       string [mscorlib]System.String::Format(string,

    //                                                            object)

    
    IL_0125:  callvirt   instance void UILabel::set_text(string)

    IL_012a:  br         IL_0160



    IL_012f:  ldarg.0

    IL_0130:  ldfld      class UILabel AOEMarker::coverWarning

    IL_0135:  callvirt   instance class [UnityEngine]UnityEngine.GameObject [UnityEngine]UnityEngine.Component::get_gameObject()

    IL_013a:  ldc.i4.0

    IL_013b:  callvirt   instance void [UnityEngine]UnityEngine.GameObject::SetActive(bool)

    IL_0140:  ldarg.0

    IL_0141:  ldfld      class UILabel AOEMarker::toHitLabel

    IL_0146:  ldstr      "{0}%"

    IL_014b:  ldloc.2

    IL_014c:  call       int32 [UnityEngine]UnityEngine.Mathf::RoundToInt(float32)

    IL_0151:  box        [mscorlib]System.Int32

    IL_0156:  call       string [mscorlib]System.String::Format(string,

                                                                object)

    IL_015b:  callvirt   instance void UILabel::set_text(string)

    IL_0160:  call       class Item HUDManager::get_ActiveItem()

    IL_0165:  ldfld      class [ShadowrunDTO]isogame.ItemDef Item::def

    IL_016a:  callvirt   instance class [mscorlib]System.Collections.Generic.List`1<float32> [ShadowrunDTO]isogame.ItemDef::get_effectModTable()

    IL_016f:  callvirt   instance int32 class [mscorlib]System.Collections.Generic.List`1<float32>::get_Count()

    IL_0174:  call       class [ShadowrunDTO]isogame.AbilityDef HUDManager::get_ActiveAbility()

    IL_0179:  callvirt   instance class [mscorlib]System.Collections.Generic.List`1<float32> [ShadowrunDTO]isogame.AbilityDef::get_effectModTable()

    IL_017e:  callvirt   instance int32 class [mscorlib]System.Collections.Generic.List`1<float32>::get_Count()

    IL_0183:  call       int32 [UnityEngine]UnityEngine.Mathf::Max(int32,

                                                                   int32)

    IL_0188:  call       class Item HUDManager::get_ActiveItem()

    IL_018d:  ldfld      class [ShadowrunDTO]isogame.ItemDef Item::def

    IL_0192:  callvirt   instance int32 [ShadowrunDTO]isogame.ItemDef::get_effectRadius()

    IL_0197:  call       int32 [UnityEngine]UnityEngine.Mathf::Max(int32,

                                                                   int32)

    IL_019c:  stloc.s    V_8

    IL_019e:  ldc.i4.0

    IL_019f:  stloc.s    V_9

    IL_01a1:  ldc.i4.0

    IL_01a2:  stloc.s    V_10

    IL_01a4:  ldloc.s    V_8

    IL_01a6:  ldc.i4.0

    IL_01a7:  ble        IL_0273



    IL_01ac:  ldloc.0

    IL_01ad:  ldarg.0

    IL_01ae:  ldfld      class Tile AOEMarker::activeTile

    IL_01b3:  ldfld      valuetype GridPoint Tile::gridPos

    IL_01b8:  ldloc.s    V_8

    IL_01ba:  callvirt   instance class [mscorlib]System.Collections.Generic.List`1<class Player> TileGrid::getNearbyPlayers(valuetype GridPoint,

                                                                                                                             int32)

    IL_01bf:  stloc.s    V_11

    IL_01c1:  ldc.i4.0

    IL_01c2:  stloc.s    V_12

    IL_01c4:  br         IL_0244



    IL_01c9:  call       class Item HUDManager::get_ActiveItem()

    IL_01ce:  call       class [ShadowrunDTO]isogame.AbilityDef HUDManager::get_ActiveAbility()

    IL_01d3:  callvirt   instance bool Item::AffectsFriendly(class [ShadowrunDTO]isogame.AbilityDef)

    IL_01d8:  brfalse    IL_0210



    IL_01dd:  ldloc.s    V_11

    IL_01df:  ldloc.s    V_12

    IL_01e1:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class Player>::get_Item(int32)

    IL_01e6:  ldloc.1

    IL_01e7:  call       bool [UnityEngine]UnityEngine.Object::op_Equality(class [UnityEngine]UnityEngine.Object,

                                                                           class [UnityEngine]UnityEngine.Object)

    IL_01ec:  brtrue     IL_0205



    IL_01f1:  ldloc.1

    IL_01f2:  ldloc.s    V_11

    IL_01f4:  ldloc.s    V_12

    IL_01f6:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class Player>::get_Item(int32)

    IL_01fb:  callvirt   instance bool Player::IsPlayerAlly(class Player)

    IL_0200:  brfalse    IL_0210



    IL_0205:  ldloc.s    V_10

    IL_0207:  ldc.i4.1

    IL_0208:  add

    IL_0209:  stloc.s    V_10

    IL_020b:  br         IL_023e



    IL_0210:  call       class Item HUDManager::get_ActiveItem()

    IL_0215:  call       class [ShadowrunDTO]isogame.AbilityDef HUDManager::get_ActiveAbility()

    IL_021a:  callvirt   instance bool Item::AffectsEnemy(class [ShadowrunDTO]isogame.AbilityDef)

    IL_021f:  brfalse    IL_023e



    IL_0224:  ldloc.1

    IL_0225:  ldloc.s    V_11

    IL_0227:  ldloc.s    V_12

    IL_0229:  callvirt   instance !0 class [mscorlib]System.Collections.Generic.List`1<class Player>::get_Item(int32)

    IL_022e:  callvirt   instance bool Player::IsPlayerEnemy(class Player)

    IL_0233:  brfalse    IL_023e



    IL_0238:  ldloc.s    V_10

    IL_023a:  ldc.i4.1

    IL_023b:  add

    IL_023c:  stloc.s    V_10

    IL_023e:  ldloc.s    V_12

    IL_0240:  ldc.i4.1

    IL_0241:  add

    IL_0242:  stloc.s    V_12

    IL_0244:  ldloc.s    V_12

    IL_0246:  ldloc.s    V_11

    IL_0248:  callvirt   instance int32 class [mscorlib]System.Collections.Generic.List`1<class Player>::get_Count()

    IL_024d:  blt        IL_01c9



    IL_0252:  ldarg.0

    IL_0253:  ldfld      class UILabel AOEMarker::numTargetsLabel

    IL_0258:  ldstr      "{0}"

    IL_025d:  ldloc.s    V_10

    IL_025f:  box        [mscorlib]System.Int32

    IL_0264:  call       string [mscorlib]System.String::Format(string,

                                                                object)

    IL_0269:  callvirt   instance void UILabel::set_text(string)

    IL_026e:  br         IL_0283



    IL_0273:  ldarg.0

    IL_0274:  ldfld      class UILabel AOEMarker::numTargetsLabel

    IL_0279:  ldsfld     string [mscorlib]System.String::Empty

    IL_027e:  callvirt   instance void UILabel::set_text(string)

    IL_0283:  call       !0 class LazySingletonBehavior`1<class MoveMeshManager>::get_Instance()

    IL_0288:  ldarg.0

    IL_0289:  ldfld      class Tile AOEMarker::activeTile

    IL_028e:  ldloc.s    V_8

    IL_0290:  ldloc.s    V_9

    IL_0292:  callvirt   instance void MoveMeshManager::SetAOETile(class Tile,

                                                                   int32,

                                                                   bool)

    IL_0297:  ldarg.0

    IL_0298:  ldc.i4.1

    IL_0299:  stfld      bool AOEMarker::isDirty

    IL_029e:  ret
    
"""
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newAwake}
        pieces.append(res) 
        
    if not tuple(s):
        print("\tCant replace aoemarker_initialize")
        
    return pieces

# Localize.Strings::T
# Change resourcesPath
# from "Loc"
# to "Dragonfall_Data\Resource\Loc"
def replace_strings_initialize(assembly_code):
    pieces = []
    reg = rb"Initialize\(\) cil managed\s+\{([\*\+\%\\\?\&\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method Strings\:\:Initialize"
    newInitialize = r"""
    // 
    .maxstack  8
    IL_0000:  ldc.i4.1
    IL_0001:  stsfld     bool Localize.Strings::initialized
    IL_0006:  ldstr      "Dragonfall_Data/Resources/Loc/"
    IL_000b:  stsfld     string Localize.Strings::resourcesPath
    IL_0010:  ret
  
    """
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newInitialize}
        pieces.append(res)   
    
    if not tuple(s):
        print("\tCant replace strings_initialize")
        
    return pieces


# Localize.Strings::T
# Change link to interface localization
# from internal asset "Loc\(Language)\code"
# to file at disk "Loc\(Language)\interface.mo"
def replace_strings_t(assembly_code):
    pieces = []
    reg = rb"T\(string msg\) cil managed\s+\{([\*\+\%\\\?\&\!\`\'\<\>\/\{\}\[\]\"\.\,\(\)\:\s\w]*)\}\s\/\/ end of method Strings\:\:T"
    newT = r"""
    // 
    .maxstack  14
    .locals init (string V_0,
             string V_1,
             class [NGettext]NGettext.ICatalog V_2,
             string V_3,
             int32 V_4)
    IL_0000:  ldsfld     bool Localize.Strings::initialized
    IL_0005:  brtrue     IL_000f

    IL_000a:  call       void Localize.Strings::Initialize()
    IL_000f:  ldsfld     string [mscorlib]System.String::Empty
    IL_0014:  stloc.0
    IL_0015:  ldsfld     valuetype Localize.Strings/Culture Localize.Strings::culture
    IL_001a:  brfalse    IL_0048

    IL_001f:  ldsfld     string Localize.Strings::resourcesPath
    IL_0024:  ldsfld     valuetype Localize.Strings/Culture Localize.Strings::culture
    IL_0029:  call       string Localize.Strings::CultureToString(valuetype Localize.Strings/Culture)
    IL_002e:  ldstr      "/interface.mo"
    IL_0033:  call       string [mscorlib]System.String::Concat(string,
                                                                string,
                                                                string)
    IL_0038:  stloc.1
    IL_0039:  ldloc.1
    IL_003a:  call       class [NGettext]NGettext.ICatalog GettextInterface.Strings::LoadMOFileFromPath(string)
    IL_003f:  stloc.2
    IL_0040:  ldloc.2
    IL_0041:  ldarg.0
    IL_0042:  call       string GettextInterface.Strings::T(class [NGettext]NGettext.ICatalog,
                                                            string)
    IL_0047:  stloc.0
    IL_0048:  ldloc.0
    IL_0049:  ldsfld     string [mscorlib]System.String::Empty
    IL_004e:  call       bool [mscorlib]System.String::op_Equality(string,
                                                                   string)
    IL_0053:  brfalse    IL_005a

    IL_0058:  ldarg.0
    IL_0059:  stloc.0
    IL_005a:  ldsfld     bool Settings::LOCALIZATION_DEBUG_CODE
    IL_005f:  brfalse    IL_00b3

    IL_0064:  ldloc.0
    IL_0065:  stloc.3
    IL_0066:  ldsfld     string [mscorlib]System.String::Empty
    IL_006b:  stloc.0
    IL_006c:  ldc.i4.0
    IL_006d:  stloc.s    V_4
    IL_006f:  br         IL_00a6

    IL_0074:  ldloc.3
    IL_0075:  ldloc.s    V_4
    IL_0077:  callvirt   instance char [mscorlib]System.String::get_Chars(int32)
    IL_007c:  ldc.i4.s   10
    IL_007e:  bne.un     IL_0094

    IL_0083:  ldloc.0
    IL_0084:  ldstr      "\n"
    IL_0089:  call       string [mscorlib]System.String::Concat(string,
                                                                string)
    IL_008e:  stloc.0
    IL_008f:  br         IL_00a0

    IL_0094:  ldloc.0
    IL_0095:  ldstr      "*"
    IL_009a:  call       string [mscorlib]System.String::Concat(string,
                                                                string)
    IL_009f:  stloc.0
    IL_00a0:  ldloc.s    V_4
    IL_00a2:  ldc.i4.1
    IL_00a3:  add
    IL_00a4:  stloc.s    V_4
    IL_00a6:  ldloc.s    V_4
    IL_00a8:  ldloc.3
    IL_00a9:  callvirt   instance int32 [mscorlib]System.String::get_Length()
    IL_00ae:  blt        IL_0074

    IL_00b3:  ldloc.0
    IL_00b4:  ret
  
    """
    r = re.compile(reg)
    s = r.finditer(assembly_code)
    s = tuple(s)
    for match in s:
        res = {'start_pos' : match.start(1), 'end_pos' : match.end(1), 'msgstr': newT}
        pieces.append(res)   
        
    if not tuple(s):
        print("\tCant replace strings_t") 
        
    return pieces
    
class MemMap:
    f = None
    mm = None
    def __init__(self,il_filename):
        self.open(il_filename)
    def open(self, il_filename):
        self.f = io.open(il_filename, "r")
        self.mm = mmap.mmap(self.f.fileno(), 0, access = mmap.ACCESS_READ)
        return self.mm
    def close(self):
        self.mm.close()
        self.f.close()
    
def translate_assembly(il_mm):
        
    localization = get_localization(settings.localization_path, 'interface')
    
    pieces = []
    pieces += replace_aireload(il_mm, localization)
    pieces += replace_custom_description(il_mm, localization)
    pieces += replace_teamadvancementscreen_onhover(il_mm)
    pieces += replace_teamadvancementscreen_awake(il_mm)
    pieces += replace_pdaanchor_awake(il_mm)
    pieces += replace_weaponpanel_init(il_mm)
    pieces += replace_aoemarker_initialize(il_mm)
    pieces += replace_strings_initialize(il_mm)
    pieces += replace_strings_t(il_mm)
    assembly_code = replace_text(il_mm, pieces)
    
    return assembly_code
	
def process():
    if settings.debug:
        print (u'translate_assembly')
    
    assembly_filename = settings.src_path
    assembly_code, il_filename, res_filename = decompile(assembly_filename)
    il_mm = MemMap(il_filename)
    assembly_code = translate_assembly(il_mm.mm)
    il_mm.close()
    compile(settings.dst_path,assembly_code,res_filename)
    
    