#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import polib
import re
import shutil
class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''
        
def translate_interface_mo(root_dir, out_file):
    po = MultiPOFile()
    filenames = filelist_from_directory(root_dir, "interface*.po")
    # BECAUSE 2.0.9 BUG: Abilities search using by 'Strings.T' in resource.assets instead ContentPacks
    filenames += filelist_from_directory(root_dir, "dragonfallextended*.po")
    filenames += filelist_from_directory(root_dir, "berlin*.po")     
    filenames += filelist_from_directory(root_dir, "seattle*.po") 
    if filenames == []:
        print('\tCant find fetched interface*.po files')
        return None
    filenames.sort(key=cmp_version)
    for filename in filenames:
        if settings.debug:
            print (u'\tfilename: "%s"' % filename)
        po.add_patch(root_dir, filename)
    
    po.make_unique()
    assure_path_exists(out_file)
    po.save_as_mofile(out_file)
    return out_file

def pack_mo(filename, textasset_filename):
    print("\tpack mo...")
    buffer = parse_bin_file(filename)
    size = os.path.getsize(filename)
    mo_length = struct.pack("I",size) 
    outbuffer = bytearray()
    outbuffer.extend(mo_length)
    outbuffer.extend(buffer)
    pad_byte_num = mod_addition(size,4)
    for i in range (0,pad_byte_num):
        outbuffer.extend([0x0])
    ext_pad = struct.pack("I",0)
    outbuffer.extend(ext_pad)    
    
    textasset_file = os.path.join(tempfile.gettempdir(), textasset_filename)
    write_bin_file( textasset_file , outbuffer)
    return textasset_file


'''  Not need because we now not pack localization in resouce.assets  
def pack_interface(textasset_file, assets_filename, fid):
    print("\tpack localization TextAsset...")
    loc_resource = ""
    unity = UnityDll("/lib/unity/", "unity.dll")
    unity.OpenArchive(assets_filename)
    unity_index = unity.get_index()
    unity.CloseArchive()
    print("\tfrom %s" % textasset_file)
    print("\tto fid: %d"%fid)
    dst_textasset_filename = ''
    try:
        dst_textasset_filename = unity_index[fid].filename
    except KeyError as e:
        print("\tError: Cant find file with fid = %d" %fid)
        return
    if dst_textasset_filename != "":
        textasset_file = shutil.copy(textasset_file, os.path.join(tempfile.gettempdir(), dst_textasset_filename))
        res = unity.pack(assets_filename, dst_textasset_filename, textasset_file)
    return loc_resource       
'''
def inject_interface(mo_file):
    target_interface_file = settings.dst_path + "Resources/Loc/" + settings.target_language.upper() + "/" + "interface.mo"
    assure_path_exists(target_interface_file)
    shutil.copyfile(mo_file, target_interface_file)
    
def process():
    if settings.debug:
        print (u'translate_interface')
    main_assets_filename = settings.src_path + "mainData"
    resource_assets_filename = settings.src_path + "resources.assets"
    
    mo_file = translate_interface_mo(settings.localization_path, os.path.join(tempfile.gettempdir(), 'interface.mo'))
    if mo_file is None:
        print ("\tCant create interface.mo")
        return
        
    inject_interface(mo_file)
    
    ''' Not need because we now not pack localization in resouce.assets
    textasset_filename = "interface.mov.xml.bcg"
    textasset_file = pack_mo(mo_file,textasset_filename)
     
    print('\tTrying pack file to assets')
    if os.path.exists(main_assets_filename) and os.path.exists(resource_assets_filename):
        fs_dict_file = extract_fs_dict_file(main_assets_filename)
        loc_resources = search_localization_resource(fs_dict_file)
        loc_resource_fid = None
        print('\tdetected localizations: %s' % loc_resources)
        for key in loc_resources.keys():
            if key.startswith('loc/'+settings.target_language):
                loc_resource_fid = loc_resources[key]
        if loc_resource_fid is not None: # GOG version dont have localization resource
            pack_interface(textasset_file, settings.dst_path + "resources.assets", loc_resource_fid)
        
    else:
        print('\tCant find maindata or assets file')
        print('\t' + main_assets_filename)
        print('\t' + resource_assets_filename)
        assure_path_exists(settings.dst_path + "resources/" + textasset_filename)
        shutil.copyfile(textasset_file, settings.dst_path + "resources/" + textasset_filename)
    '''
    
    
        
     
    
