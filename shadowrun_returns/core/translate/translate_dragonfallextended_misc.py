# -*- coding: utf-8 -*-
from translator.utils import *
from translator.counters import *

class settings:
    src_path = './data/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/misc/'
    dst_path = './out/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/misc/'
    translate_path = './corrected/'
    debug = True

def replace_escape(text):
    text = text.replace(u"\\n",u"\n")
    return text
def replace_misc(misc, original_list, translate_list):
    t = misc
    if t != "":
        tr = translate(t, original_list, translate_list)
        
        if tr != "":
            tr = replace_escape(tr)
            return tr
    return misc    
def translate_misc(misc_filename):
    if settings.debug:
        print (u'misc_filename: "%s"' % misc_filename)
    misc = parse_utf8_text_file(settings.src_path + misc_filename)
        
    translate_filenames = find_translate_file("DragonfallExtended|berlin_campaign", settings.translate_path , regex = True, all = True)
    if settings.debug:
        print (u'translate_filenames: "%s" ' % (translate_filenames) )
    original_list = []
    translate_list = []
    for translate_filename in translate_filenames:
        orig, trans = parse_translate_file(translate_filename, settings.translate_path)
        original_list.extend(orig)
        translate_list.extend(trans)
        
    misc = replace_misc(misc, original_list, translate_list)
    
    assure_path_exists(settings.dst_path + misc_filename)
    write_utf8_text_file(settings.dst_path + misc_filename,misc)
    
    #flush_replace_count(translate_filename)
    
def process():
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    #filenames.remove("DragonfallExtended.pflib.bytes")
    for filename in filenames:
        translate_misc(filename)
    
if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    process()