# -*- coding: utf-8 -*-
from translator.utils import *
from translator.counters import *
import sys
sys.path.insert(0, './proto')
import maps_pb2

class settings:
    src_path = './data/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/maps/'
    dst_path = './out/pc/2.0.3/StreamingAssets/ContentPacks/DragonfallExtended/data/maps/'
    translate_path = './corrected/'
    debug = True

def replace_escape(text):
    text = text.replace(u"\\n",u"\n")
    return text
def replace_map_props(map, original_list, translate_list):
    replaced = False
    # we need this code because ingame bug: dont using ContentStrings.T() in InteractableObject.setupInteractionLinks()
    
    for prop in map.props:
        interactionType = ""
        if prop.HasField('interactionRoot'):
            if prop.interactionRoot.HasField('jackPoint'):
                if prop.interactionRoot.jackPoint.HasField('LANIdentifier') and prop.interactionRoot.jackPoint.LANIdentifier != "":
                    interactionType = "InteractionType_JackPoint"
            else:
                if prop.interactionRoot.HasField('summonPoint'):
                    interactionType = "InteractionType_SummonPoint"
                elif prop.interactionRoot.HasField('alarmPoint'):
                    interactionType = "InteractionType_Alarm"
                elif prop.interactionRoot.HasField('pickupItem'):
                    interactionType = "InteractionType_ItemPickup"
                elif prop.interactionRoot.HasField('conversationPoint'):
                    interactionType = "InteractionType_Conversation"
                elif prop.interactionRoot.HasField('doorPoint'):
                    interactionType = "InteractionType_Door"
                elif prop.interactionRoot.HasField('transitionPoint'):
                    interactionType = "InteractionType_Transition"
                elif prop.interactionRoot.HasField('matrixSpawnPoint'):
                    interactionType = "InteractionType_MatrixSpawnPoint"
                elif prop.interactionRoot.HasField('matrixNode'):
                    interactionType = "InteractionType_MatrixNode"
                elif prop.interactionRoot.HasField('matrixLabel'):
                    interactionType = "InteractionType_DummyMatrixLabel"
                elif prop.interactionRoot.HasField('dummyInteraciton'):
                    interactionType = "InteractionType_Dummy"
                elif prop.interactionRoot.HasField('inspectInteraction'):
                    interactionType = "InteractionType_Inspect"
                elif prop.interactionRoot.HasField('warpInteraction'):
                    interactionType = "InteractionType_Generic"
                elif prop.interactionRoot.HasField('is_interaction_doc_wagon'):
                    interactionType = "InteractionType_DocWagon"
                else:
                    interactionType = "InteractionType_Generic"
                    #original_list.append(prop.displayName)
                    #TextEnabled = False
        if interactionType == "InteractionType_MatrixNode":
            flag = False
            for hacking_object in prop.interactionRoot.matrixNode.hacking_objects:
                for tprop in map.props:
                    if tprop.HasField('idRef') and tprop.idRef.id == hacking_object:
                        flag = True
                        tr = translate(tprop.displayName, original_list, translate_list)
                        if tr != "":
                            tprop.displayName = tr
                            replaced = True
                # save idref for replace scene
                f = open('./char_scene_idref.conf','a')
                f.write(hacking_object + '\n')
                f.close()    
            if flag :
                #original_list.append( hacking_objects[0].displayName)
                # not need. we yet take it
                pass
            else:
                #Didn't link Matrix Node  to any other object
                tr = translate(prop.displayName, original_list, translate_list)
                if tr != "":
                    prop.displayName = tr
                    replaced = True
        elif interactionType == "InteractionType_DummyMatrixLabel":
            #Dummy Matrix Label for object
            tr = translate(prop.displayName, original_list, translate_list)
            if tr != "":
                prop.displayName = tr
                replaced = True
            
    return replaced    
def translate_map(map_filename):
    if settings.debug:
        print (u'map_filename: "%s"' % map_filename)
    map = parse_protobuf_file(settings.src_path + map_filename, maps_pb2.map())
    map_name = map.name
    #scene_title =  scene.scene_title.replace(u'\u2019',u'\u0027').strip()
    if settings.debug:
        print (u'map_name: "%s"' % map_name)
    translate_filenames = find_translate_file("berlin_campaign_props", settings.translate_path , regex = True, all = True)
    if settings.debug:
        print (u'translate_filenames: "%s" ' % (translate_filenames) )
    original_list = []
    translate_list = []
    for translate_filename in translate_filenames:
        orig, trans = parse_translate_file(translate_filename, settings.translate_path)
        original_list.extend(orig)
        translate_list.extend(trans)
    
    replaced = replace_map_props(map, original_list, translate_list)
    if replaced:
        assure_path_exists(settings.dst_path + map_filename)
        write_protobuf_file(settings.dst_path+ map_filename, map)
       
def process():
    f = open('./char_scene_idref.conf','w')
    f.close()
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        translate_map(filename)
    
if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    process()