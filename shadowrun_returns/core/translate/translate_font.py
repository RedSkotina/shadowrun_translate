#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
import ctypes
import struct
import tempfile

class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''
class main_desc_entry:
    unknown1 = None
    unknown2 = None
    unknown3 = None
    fid1     = None
    unknown5 = None
    unknown6 = None
    fid2     = None
    unknown8 = None
    name_size = None
    name     = ""
    unknown11 = None
    unknown12 = None
    
def parse_font_main_desc(filename):
    print("\tparse main font descriptor...")
    main_desc = main_desc_entry()
    with io.open(filename , "rb") as f:
        buffer = bytearray()
        buffer.extend(f.read())
        cur_ptr = 0x0
        main_desc.unknown1 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.unknown2 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.unknown3 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.fid1 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.unknown5 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.unknown6 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.fid2 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.unknown8 = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4

        main_desc.name_size = int(struct.unpack("I",buffer[cur_ptr:cur_ptr+0x4])[0])
        cur_ptr = cur_ptr + 0x4
        main_desc.name = struct.unpack(str( main_desc.name_size)+"s",buffer[cur_ptr:cur_ptr+ main_desc.name_size])[0].decode('utf-8')
        cur_ptr = cur_ptr +  main_desc.name_size
        name_padding_size = 4 - ( main_desc.name_size % 4) if ( main_desc.name_size % 4) != 0 else 0
        #skip padding
        cur_ptr = cur_ptr + name_padding_size
        main_desc.unknown11 = int(struct.unpack("H",buffer[cur_ptr:cur_ptr+0x2])[0])
        cur_ptr = cur_ptr + 0x2
        main_desc.unknown12 = int(struct.unpack("B",buffer[cur_ptr:cur_ptr+0x1])[0])
        cur_ptr = cur_ptr + 0x1
    return main_desc
def process():
    if settings.debug:
        print (u'translate_font')
    #settings.auto_path = True # Autogenerate path
    main_assets_filename = settings.dst_path + "mainData"
    resource_assets_filename = settings.dst_path + "resources.assets"

    if os.path.exists(main_assets_filename):
        fs_dict_file = extract_fs_dict_file(main_assets_filename)
        entries = search_font_sharedassets(fs_dict_file)
        print('\tdetected fonts: %s' % entries)
        loc_file = extract_font_sharedassets(resource_assets_filename,entries, "./" )
        for entry in entries:
            main_desc = parse_font_main_desc("./"+entry.filename)
            print (main_desc.fid1,main_desc.fid2)
            
        #po_resource = extract_interface(file)
        
        #print('\ttarget_language: %s' % target_language)
        #return  target_language       
    else:
        print('\tWarning: Cant find maindata file %s' % main_assets_filename)
        print('\tProbably localization cant be imported in game')
        print('\tusing localization: %s' % settings.language)
        #target_language = settings.language
        # return target_language
    sys.exit()