#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin/data/loc/'
    
            
def translate_seattle_mo(root_dir, out_file):
    po = MultiPOFile()
    filenames = filelist_from_directory(root_dir, "berlin*.po", ignore="berlin_campaign*.po")
    filenames.sort(key=cmp_version)
    for filename in filenames:
        if settings.debug:
            print (u'\tfilename: "%s"' % filename)
        po.add_patch(root_dir, filename)
    po.make_unique()
    assure_path_exists(out_file)
    po.save_as_mofile(out_file)
    

def process():
    if settings.debug:
        print (u'translate_berlin_mo')
    translate_seattle_mo(settings.localization_path, settings.dst_path + settings.target_language + '/berlin.mo')
    
