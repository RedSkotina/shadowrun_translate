#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.totems_pb2 as totems_pb2
import shadowrun_returns.lib.proto.scenes_pb2 as scenes_pb2
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.maps_pb2 as maps_pb2
import shadowrun_returns.lib.proto.abilities_pb2 as abilities_pb2
import shadowrun_returns.lib.proto.hiringset_pb2 as hiringset_pb2
import shadowrun_returns.lib.proto.modes_pb2 as modes_pb2
import shadowrun_returns.lib.proto.chars_pb2 as chars_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/seattle/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/seattle/data/'
    
def translate_ability_proto(ability, localization):
    if ability.HasField("uirep"):
        if ability.uirep.HasField("name"):
            ability.uirep.name = localization[ability.uirep.name]
        if ability.uirep.HasField("description"):
            ability.uirep.description = localization[ability.uirep.description]
    
def translate_char_proto(char, localization):
    char.char_name = localization[char.char_name]
    char.description_bio = localization[char.description_bio]
    char.hiring_tag = localization[char.hiring_tag]
    
def translate_hiring_proto(hiring, localization):
    for description in hiring.description:
        if description.HasField('name'):
            description.name = localization[description.name]
        if description.HasField('description'):
            description.description = localization[description.description]    
     
    
def translate_item_proto(item, localization):
    if item.uirep.HasField('name'): # name
        item.uirep.name = localization[item.uirep.name]
    if item.uirep.HasField('description'): #description
        item.uirep.description = localization[item.uirep.description]
    if item.HasField('activationStatusEffect'):
        if item.activationStatusEffect.uirep.HasField('name'): 
            item.activationStatusEffect.uirep.name = localization[item.activationStatusEffect.uirep.name]
        if item.activationStatusEffect.uirep.HasField('description'): 
            item.activationStatusEffect.uirep.description = localization[item.activationStatusEffect.uirep.description]
    if item.HasField('character_ui_name'):
        item.character_ui_name = localization[item.character_ui_name]

def translate_map_proto(map, localization):
    for props in map.props:
        if props.HasField('interactionRoot'):
            if props.interactionRoot.HasField('prerequisites'):
                search_result = walkTsCall(props.interactionRoot.prerequisites.ops, search_function_name_predicate)
                for i in search_result:
                    i.string_value = localization[i.string_value]
            
            if props.interactionRoot.HasField('inspectInteraction'):
                props.interactionRoot.inspectInteraction.inspectText = localization[props.interactionRoot.inspectInteraction.inspectText]
            if props.interactionRoot.HasField('transitionPoint'):
                props.interactionRoot.transitionPoint.confirmation_text = localization[props.interactionRoot.transitionPoint.confirmation_text]
        for property in props.properties: 
            if property.property_id == 'NewsTickerText':
                property.string_value = localization[property.string_value]        
    
    for prop in map.props:
        interactionType = ""
        if prop.HasField('interactionRoot'):
            if prop.interactionRoot.HasField('jackPoint'):
                if prop.interactionRoot.jackPoint.HasField('LANIdentifier') and prop.interactionRoot.jackPoint.LANIdentifier != "":
                    interactionType = "InteractionType_JackPoint"
            else:
                if prop.interactionRoot.HasField('summonPoint'):
                    interactionType = "InteractionType_SummonPoint"
                elif prop.interactionRoot.HasField('alarmPoint'):
                    interactionType = "InteractionType_Alarm"
                elif prop.interactionRoot.HasField('pickupItem'):
                    interactionType = "InteractionType_ItemPickup"
                elif prop.interactionRoot.HasField('conversationPoint'):
                    interactionType = "InteractionType_Conversation"
                elif prop.interactionRoot.HasField('doorPoint'):
                    interactionType = "InteractionType_Door"
                elif prop.interactionRoot.HasField('transitionPoint'):
                    interactionType = "InteractionType_Transition"
                elif prop.interactionRoot.HasField('matrixSpawnPoint'):
                    interactionType = "InteractionType_MatrixSpawnPoint"
                elif prop.interactionRoot.HasField('matrixNode'):
                    interactionType = "InteractionType_MatrixNode"
                elif prop.interactionRoot.HasField('matrixLabel'):
                    interactionType = "InteractionType_DummyMatrixLabel"
                elif prop.interactionRoot.HasField('dummyInteraciton'):
                    interactionType = "InteractionType_Dummy"
                elif prop.interactionRoot.HasField('inspectInteraction'):
                    interactionType = "InteractionType_Inspect"
                elif prop.interactionRoot.HasField('warpInteraction'):
                    interactionType = "InteractionType_Generic"
                elif prop.interactionRoot.HasField('is_interaction_doc_wagon'):
                    interactionType = "InteractionType_DocWagon"
                else:
                    interactionType = "InteractionType_Generic"
                    #original_list.append(prop.displayName)
                    #TextEnabled = False
        if interactionType == "InteractionType_MatrixNode":
            islinked = False
            for hacking_object in prop.interactionRoot.matrixNode.hacking_objects:
                for tprop in map.props:
                    if tprop.HasField('idRef') and tprop.idRef.id == hacking_object:
                        islinked = True
                        tprop.displayName = localization[tprop.displayName]
            if not islinked:
                #Didn't link Matrix Node  to any other object
                prop.displayName = localization[prop.displayName]
        elif interactionType == "InteractionType_DummyMatrixLabel":
            #Dummy Matrix Label for object
            prop.displayName = localization[prop.displayName]
            pass
        # TEST ONLY: all display name
        #if prop.HasField('displayName'):
        #    prop.displayName = localization[prop.displayName]

    
def translate_mode_proto(mode, localization):
    if mode.HasField("uirep"):
        if mode.uirep.HasField("name"):
            mode.uirep.name = localization[mode.uirep.name]
        if mode.uirep.HasField("description"):
            mode.uirep.description = localization[mode.uirep.description] 
    
def translate_scene_proto(scene, localization):
    scene.scene_title = localization[scene.scene_title]
    scene.scene_synopsis = localization[scene.scene_synopsis]
    
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            i.string_value = localization[i.string_value]
                    
    for i in scene.goals:
        i.name = localization[i.name]
        
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.events.ops, search_function_name_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_function_name_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_function_name_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_function_name_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for i in search_result:
            i.string_value = localization[i.string_value]
        
    # some replacement string
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_fn_set_string_predicate)
        search_result2 = walkTsCall(trigger.elseActions.ops, search_fn_set_string_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            i.string_value = localization[i.string_value]  
    
    # set screen label
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            i.string_value = localization[i.string_value] 
            
    # npc names
    for character in scene.characters:
        character.character_instance.char_name = localization[character.character_instance.char_name] 
    
def translate_totem_proto(totem, localization):
    for ctotem in totem.ctotems:
        ctotem.name = localization[ctotem.name]
        ctotem.description = localization[ctotem.description]
 
#-----------------------------------------------------    
def translate_abilities(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'abilities/'+ id, abilities_pb2.ability())
    localization = get_localization(settings.localization_path, locfilename)
    translate_ability_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'abilities/'+ id, proto)
    
def translate_chars(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'chars/'+ id, chars_pb2.ch_inst())
    localization = get_localization(settings.localization_path, locfilename)
    translate_char_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'chars/'+ id, proto)
    
def translate_hiring(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'hiring/'+ id, hiringset_pb2.hiring())
    localization = get_localization(settings.localization_path, locfilename)
    translate_hiring_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'hiring/'+ id, proto)

def translate_items(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'items/'+ id, items_pb2.item())
    localization = get_localization(settings.localization_path, locfilename)
    translate_item_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'items/'+ id, proto)

def translate_maps(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'maps/'+ id, maps_pb2.map())
    localization = get_localization(settings.localization_path, locfilename)
    translate_map_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'maps/'+ id, proto)

def translate_modes(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'modes/'+ id, modes_pb2.mode())
    localization = get_localization(settings.localization_path, locfilename)
    translate_mode_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'modes/'+ id, proto)


def translate_scenes(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'scenes/'+ id, scenes_pb2.scene())
    localization = get_localization(settings.localization_path, locfilename)
    translate_scene_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'scenes/'+ id, proto)

    
def translate_totems(id, locfilename):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'totems/'+ id, totems_pb2.totem())
    localization = get_localization(settings.localization_path, locfilename)
    translate_totem_proto(proto, localization)
    write_protobuf_file(settings.dst_path +  'totems/'+ id, proto)
    
#-------------------------------------------------    
def process():
    if settings.debug:
        print (u'translate_seattle')

    filenames = filelist_from_directory(settings.src_path + 'abilities',"*.bytes", ignore="readme.bytes")
    for filename in filenames:
        translate_abilities(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'chars',"*.ch_inst.bytes", ignore="readme.bytes")
    for filename in filenames:
        translate_chars(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'hiring',"*.bytes")
    for filename in filenames:
        translate_hiring(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'items',"*.bytes", ignore="readme.bytes")
    for filename in filenames:
        translate_items(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'maps',"*.bytes")
    for filename in filenames:
        translate_maps(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'modes',"*.bytes", ignore="readme.bytes")
    for filename in filenames:
        translate_modes(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'scenes',"*.bytes")
    for filename in filenames:
        translate_scenes(filename,'seattle')
    filenames = filelist_from_directory(settings.src_path + 'totems',"*.bytes")
    for filename in filenames:
        translate_totems(filename,'seattle')
    