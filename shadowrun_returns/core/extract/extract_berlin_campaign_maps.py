#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.maps_pb2 as maps_pb2
#import builtins 
#builtins.g_char_scene_idrefs = []
class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/maps/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/maps/'

pot = LocalizationCatalog() 
    
def extract_map_proto(map):
    for props in map.props:
        if props.HasField('interactionRoot'):
            if props.interactionRoot.HasField('prerequisites'):
                search_result = walkTsCall(props.interactionRoot.prerequisites.ops, search_function_name_predicate)
                for i in search_result:
                    pot['map'] += i.string_value
                
            if props.interactionRoot.HasField('inspectInteraction'):
                pot['map_inspect'] += props.interactionRoot.inspectInteraction.inspectText
    
            if props.interactionRoot.HasField('transitionPoint'):
                pot['map_transition'] += props.interactionRoot.transitionPoint.confirmation_text
        
    for props in map.props:
        for property in props.properties: 
            if property.property_id == 'NewsTickerText':
                pot['map_news'] += property.string_value
    
    for prop in map.props:
        interactionType = ""
        if prop.HasField('interactionRoot'):
            if prop.interactionRoot.HasField('jackPoint'):
                if prop.interactionRoot.jackPoint.HasField('LANIdentifier') and prop.interactionRoot.jackPoint.LANIdentifier != "":
                    interactionType = "InteractionType_JackPoint"
            else:
                if prop.interactionRoot.HasField('summonPoint'):
                    interactionType = "InteractionType_SummonPoint"
                elif prop.interactionRoot.HasField('alarmPoint'):
                    interactionType = "InteractionType_Alarm"
                elif prop.interactionRoot.HasField('pickupItem'):
                    interactionType = "InteractionType_ItemPickup"
                elif prop.interactionRoot.HasField('conversationPoint'):
                    interactionType = "InteractionType_Conversation"
                elif prop.interactionRoot.HasField('doorPoint'):
                    interactionType = "InteractionType_Door"
                elif prop.interactionRoot.HasField('transitionPoint'):
                    interactionType = "InteractionType_Transition"
                elif prop.interactionRoot.HasField('matrixSpawnPoint'):
                    interactionType = "InteractionType_MatrixSpawnPoint"
                elif prop.interactionRoot.HasField('matrixNode'):
                    interactionType = "InteractionType_MatrixNode"
                elif prop.interactionRoot.HasField('matrixLabel'):
                    interactionType = "InteractionType_DummyMatrixLabel"
                elif prop.interactionRoot.HasField('dummyInteraciton'):
                    interactionType = "InteractionType_Dummy"
                elif prop.interactionRoot.HasField('inspectInteraction'):
                    interactionType = "InteractionType_Inspect"
                elif prop.interactionRoot.HasField('warpInteraction'):
                    interactionType = "InteractionType_Generic"
                elif prop.interactionRoot.HasField('is_interaction_doc_wagon'):
                    interactionType = "InteractionType_DocWagon"
                else:
                    interactionType = "InteractionType_Generic"
                    #original_list.append(prop.displayName)
                    #TextEnabled = False
        if interactionType == "InteractionType_MatrixNode":
            islinked = False
            for hacking_object in prop.interactionRoot.matrixNode.hacking_objects:
                for tprop in map.props:
                    if tprop.HasField('idRef') and tprop.idRef.id == hacking_object:
                        islinked = True
                        pot['map_hackingobject'] += tprop.displayName
            if not islinked:
                #Didn't link Matrix Node  to any other object
                pot['map_hackingobject'] += prop.displayName
        elif interactionType == "InteractionType_DummyMatrixLabel":
            #Dummy Matrix Label for object
            pot['map_hackingobject'] += prop.displayName
            pass
        # TEST ONLY: all display name
        #if prop.HasField('displayName'):
        #    original_list.append(prop.displayName)
    
def extract_map(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, maps_pb2.map())
    extract_map_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_campaign_maps')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_map(filename)
    pot.save(settings.dst_path + "maps.pot")