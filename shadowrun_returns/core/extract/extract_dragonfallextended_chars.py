#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import sys
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.chars_pb2 as chars_pb2
import shadowrun_returns.lib.proto.isogame_pb2 as isogame_pb2
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/chars/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/chars/'
    
pot = LocalizationCatalog() 

def extract_char_proto(char):
    pot['char'] += char.char_name
    pot['char'] += char.description_bio
    pot['char'] += char.hiring_tag
    
def extract_cvf_proto(char_variant):
    pot['char_variant'] += char_variant.name
    pot['char_variant'] += char_variant.short_desc
    pot['char_variant'] += char_variant.long_desc
    
def extract_char(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, chars_pb2.ch_inst())
    extract_char_proto(proto)   

def extract_cvf(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, isogame_pb2.CharacterVariant())
    extract_cvf_proto(proto)   

def process():
    if settings.debug:
        print (u'extract_dragonfall_extended_chars')
    filenames = filelist_from_directory(settings.src_path,"*.ch_inst.bytes")
    for filename in filenames:
        extract_char(filename)
    filenames = filelist_from_directory(settings.src_path,"*.cvf.bytes")
    for filename in filenames:
        extract_cvf(filename)
    pot.save(settings.dst_path + "chars.pot")

