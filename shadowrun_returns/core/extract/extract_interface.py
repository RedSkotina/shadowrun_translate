#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
import ctypes
import struct
import tempfile
import subprocess

class settings(global_settings):
    src_local_path = ''
    dst_local_path = 'resources/'
    dst_filename = 'interface.pot'
    
def clear_msgstr(mo):
    for entry in mo:
        entry.msgstr = ''
def extract_interface(loc_file):
    print("\textract po...")
    mo = polib.mofile(loc_file)
    clear_msgstr(mo)
    print("\tto %s" % settings.dst_path + settings.dst_filename)
    assure_path_exists(settings.dst_path + settings.dst_filename)
    mo.save_as_pofile(settings.dst_path + settings.dst_filename)
    po = polib.pofile(settings.dst_path + settings.dst_filename)
    return po
def extract_mo(filename):
    print("\textract mo...")
    buffer = parse_bin_file(filename) # TextAsset
    offset = 0x0
    mo_length = buffer[offset:offset + 0x4]
    mo_length = int(struct.unpack("I",mo_length)[0])
    outbuffer = buffer[0x4:0x4 + mo_length ] 
    mofile = os.path.join(tempfile.gettempdir(), get_name_without_extension(get_filename_without_path(filename))+".mo")
    print("\tto %s" % mofile)
    write_bin_file(mofile, outbuffer)
    return mofile
    
def ildasm(assembly):
    r = b''
    try:
        r = subprocess.check_output(['lib\\ildasm\\ildasm.exe','/utf8', '/text','%s'%assembly])
    except Exception as e:
        print(e)
        sys.exit()
    r = r.decode('utf-8')
    return r

# TeamAdvancementScreen::Awake hardcoded here,because extract this strings from resources too hard. 
def extract_hardcoded_teamadvancementscreeen_labels():
    po = create_po()
    po.append(polib.POEntry( msgid = u'Cancel', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'Clear All', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'Confirm All', msgstr = u'' ))
    return po

# PDAAnchor::Awake hardcoded here,because extract this strings from resources too hard. 
def extract_hardcoded_pdaanchor_awake_labels():
    po = create_po()
    po.append(polib.POEntry( msgid = u'Game Difficulty', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'POST PROCESS', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'EXTRA BLOOD', msgstr = u'' ))
    return po    
# WeaponPanel::Init hardcoded here,because extract this strings from resources too hard. 
def extract_hardcoded_weaponpanel_init_labels():
    po = create_po()
    po.append(polib.POEntry( msgid = u'Confirm', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'Cancel', msgstr = u'' ))
    return po 
# AOEMarker::Initialize hardcoded here,because extract this strings from resources too hard. 
def extract_hardcoded_aoemarker_initialize_labels():
    po = create_po()
    po.append(polib.POEntry( msgid = u'LIGHT', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'IN_LIGHT', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'MED', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'IN_MED', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'HEAVY', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'IN_HEAVY', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'OPEN', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'FLANKED', msgstr = u'' ))
    po.append(polib.POEntry( msgid = u'COVER\nINTERVENING', msgstr = u'' ))
    return po
def extract_gettext_strings(assembly):
    if os.path.exists(assembly):
        assembly_code = ildasm(assembly)
        #print(assembly_code)
        #m = re.findall(r'ldstr\s+\"([\w\d\s\!\?\:\.\,\-\{\}\(\)\\]+?)\"\s+(?:[\d\w])+:\s+call\s+string Localize\.Strings::T\(string\)', assembly_code)
        #find il mutistring
        reg = r'ldstr\s+((?:\".+?\"\s+\+?\s+)+)(?:[\w\_\d\:]+)\s+call\s+string\sLocalize\.Strings\:\:T\(string\)'
        m = re.findall(reg, assembly_code)
        if m != []:
            #print("AS:")
            po = create_po()
            for text in m:
                #safeprint(text)
                str = ILmultistring_to_string(text)
                #safeprint(str)
                entry = polib.POEntry(
                    msgid = str,
                    msgstr = u''
                    )
                po.append(entry)
            return po
    return None
    
def extract_gettext_variable_strings(assembly):
    if os.path.exists(assembly):
        assembly_code = ildasm(assembly)
        # search ldloc.s variable name
        m = re.findall(r'ldloc.s\s+([\w\d\s\!\?\:\.\,\-\{\}\(\)\\]+?)\s+(?:[\d\w])+:\s+call\s+string Localize\.Strings::T\(string\)', assembly_code)
        if m != []:
            for string_var_name in m:
                #search stloc.s variable name
                m2 = re.findall(r'ldstr\s+\"([\w\d\s\!\?\:\.\,\-\{\}\(\)\\]+?)\"\s+(?:[\d\w])+:\s+stloc.s\s+'+string_var_name, assembly_code)
                if m2 != []:
                    po = create_po()
                    for string_var_text in m2:
                        entry = polib.POEntry(
                            msgid = string_var_text,
                            msgstr = u''
                            )
                        po.append(entry)
                    return po
    return None

def extract_hardcoded_floatie_strings(assembly):
    if os.path.exists(assembly):
        assembly_code = ildasm(assembly)
        m = re.findall(r'ldstr\s+\"([\w\d\s\!\?\:\.\,\-\{\}\(\)\\]+?)\"\r\n\s+(?:[\w\d\_\:]+\s+\w+\s+valuetype\s+\[UnityEngine\]UnityEngine\.Color\s+[\[\]\.\:\(\)\w\_]+)\r\n\s+(?:[\w\d\_\:]+\s+ldc\.r4\s+\d+\.\d*)\r\n\s+(?:[\w\d\_\:]+\s+ldc.i4.0)\r\n\s+(?:[\d\w\_])+:\s+callvirt\s+instance\s+void\s+Player::AddFloatie', assembly_code)
        if m != []:
            po = create_po()
            for text in m:
                entry = polib.POEntry(
                    msgid = text,
                    msgstr = u''
                    )
                po.append(entry)
            return po
    return None
    
def extract_hardcoded_custom_description(assembly):
    if os.path.exists(assembly):
        assembly_code = ildasm(assembly)
        reg = r'ldstr\s+(\".+?\")\s+(?:[\w\_\d\:]+)\s+ldstr\s+((?:\".+?\"\s+\+?\s+)+)(?:[\w\_\d\:]+)\s+callvirt\s+instance\s+void\s+AbilityDescriptionPanel\:\:SetCustomDescription'
        m = re.findall(reg, assembly_code)
        if m != []:
            po = create_po()
            for text_list in m:
                for text in text_list:
                    #safeprint(text)
                    str = ILmultistring_to_string(text)
                    #safeprint(str)
                    entry = polib.POEntry(
                        msgid = str,
                        msgstr = u''
                        )
                    po.append(entry)
            return po
    return None
    
def extract_harcoded_difficulty_description(assembly):
    text = "Your current global difficulty setting is {0}.\n\nNORMAL difficulty provides a balanced combat experience and is suitable for most new players. We recommend HARD difficulty for turn-based combat veterans looking for a tactical challenge.\n\nNote: This difficulty setting applies to all Dragonfall and user-created content that you play. You can change your difficulty setting below, or at any time from the Main Menu."
    game_difficulties = ['EASY','HARD','VERY HARD','NORMAL',]
    po = create_po()
    for game_difficulty in game_difficulties:
        entry = polib.POEntry(
            msgid = text.format(game_difficulty),
            msgstr = u''
            )
        po.append(entry)
    return po

def extract_gettext_dumb_strings(assembly):
    text = 'Ar-P'  # too hard parse AbilityLabel.SetLabel
    po = create_po()
    entry = polib.POEntry(
            msgid = text,
            msgstr = u''
            )
    po.append(entry)
    return po
    
def merge_po(po, po_patch):
    allow_empty_patch = False
    for po_patch_entry in po_patch:
        is_entry_patched = False
        for po_entry in po:
            if po_entry.msgid == po_patch_entry.msgid and (allow_empty_patch or po_patch_entry.msgstr != ''):
                po_entry.msgid = po_patch_entry.msgid
                po_entry.msgstr = po_patch_entry.msgstr
                is_entry_patched = True    
        if not is_entry_patched:
            po.append(po_patch_entry)

            
def process():
    if settings.debug:
        print (u'extract_interface')
    target_localisation = "ru"
    main_assets_filename = settings.src_path + "mainData"
    resource_assets_filename = settings.src_path + "resources.assets"
    po = create_po()
    po_resource = None
    if os.path.exists(main_assets_filename) and os.path.exists(resource_assets_filename):
        fs_dict_file = extract_fs_dict_file(main_assets_filename)
        loc_resources = search_localization_resource(fs_dict_file)
        loc_resource_fid = None
        print('\tdetected localizations: %s' % loc_resources)
        for key in loc_resources.keys():
            if key.startswith('loc/'+target_localisation):
               loc_resource_fid = loc_resources[key]
        
            
        if loc_resource_fid is not None: # GOG version dont have localization resources
            loc_file = extract_loc_resource(resource_assets_filename,loc_resource_fid)
            file = extract_mo(loc_file)
            po_resource = extract_interface(file)   
        else:
            print('\tCant find localization resources. It is GOG version?')
            print('\tUse emergency localization file from 2.0.9')
            loc_file = u"lib/emergency/2.0.9/code.mov.xml.bcg"
            file = extract_mo(loc_file)
            po_resource = extract_interface(file)
         
    else:
        print('\tCant find maindata or assets file')
        print('\t' + main_assets_filename)
        print('\t' + resource_assets_filename)
    ''' teamadvancementscreen
    if os.path.exists(main_assets_filename) and os.path.exists(resource_assets_filename):
        fs_dict_file = extract_fs_dict_file(main_assets_filename)
        ui_resources = search_resource(fs_dict_file,'ui/prefabs/teamadvancementscreen')
        ui_resource_fid = None
        print('\tdetected ui: %s' % ui_resources)
        for key in ui_resources.keys():
            ui_resource_fid = ui_resources[key]
        resource_assets_filename = settings.src_path + "sharedassets2.assets"
        loc_file = extract_loc_resource(resource_assets_filename,ui_resource_fid)
        
        #file = extract_mo(loc_file)
        #po_resource = extract_interface(file)
    else:
        print('\tCant find maindata or assets file')
        print('\t' + main_assets_filename)
        print('\t' + resource_assets_filename)
    '''
    po_assembly = extract_gettext_strings(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_assembly_ext = extract_gettext_variable_strings(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_assembly_ext2 = extract_hardcoded_floatie_strings(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_assembly_difficulty_desc = extract_harcoded_difficulty_description(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_assembly_description = extract_hardcoded_custom_description(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_assembly_dumb = extract_gettext_dumb_strings(settings.src_path + "Managed/Assembly-CSharp.dll")
    po_hardcode1 = extract_hardcoded_teamadvancementscreeen_labels()
    po_hardcode2 = extract_hardcoded_pdaanchor_awake_labels()
    po_hardcode3 = extract_hardcoded_weaponpanel_init_labels()
    po_hardcode4 = extract_hardcoded_aoemarker_initialize_labels()
    
    if po_resource is not None :
        merge_po(po, po_resource)
    if po_assembly is not None:
        merge_po(po, po_assembly)
    if po_assembly_ext is not None:
        merge_po(po, po_assembly_ext)
    if po_assembly_ext2 is not None:
        merge_po(po, po_assembly_ext2)
    if po_assembly_difficulty_desc is not None:
        merge_po(po, po_assembly_difficulty_desc)
    if po_assembly_description is not None:
        merge_po(po, po_assembly_description) 
    if po_assembly_dumb is not None:
        merge_po(po, po_assembly_dumb)
    if po_hardcode1 is not None:
        merge_po(po, po_hardcode1)
    if po_hardcode2 is not None:
        merge_po(po, po_hardcode2)
    if po_hardcode3 is not None:
        merge_po(po, po_hardcode3)
    if po_hardcode4 is not None:
        merge_po(po, po_hardcode4)
        
    po.make_unique()
    assure_path_exists(settings.dst_path + settings.dst_filename)
    po.save(settings.dst_path + settings.dst_filename)
    