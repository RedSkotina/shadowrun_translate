#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import sys
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.convos_pb2 as convos_pb2
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/convos/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/convos/'
    
pot = LocalizationCatalog() 
    
def sub_extract_convo_goal(convo): 
    for node in convo.nodes:
        search_result = walkTsCall(node.actions.ops, search_function_name_predicate)
        for i in search_result:
            pot['convo_goal'] += i.string_value
            
        for branche in node.branches:
            search_result = walkTsCall(branche.actions.ops, search_function_name_predicate)
            for i in search_result:
                pot['convo_goal'] += i.string_value
    for root in convo.roots:
        search_result = walkTsCall(root.actions.ops, search_function_name_predicate)
        for i in search_result:
            pot['convo_goal'] += i.string_value
    
def sub_extract_convo(convo):
    for i in convo.nodes:
        pot['convo'] += i.text
        for r in i.branches:
            pot['convo'] += r.responseText
    for i in convo.roots:
        pot['convo'] += i.responseText
    for node in convo.nodes:
        for branch in node.branches:
            search_result = walkTsCall(branch.actions.ops, search_display_text_predicate)
            for i in search_result:
                pot['convo'] += i.string_value
                
def extract_convo_proto(convo):
    sub_extract_convo_goal(convo)        
    sub_extract_convo(convo)  
    
def extract_convo(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, convos_pb2.convo())
    extract_convo_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_dragonfall_extended_convos')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_convo(filename)
    pot.save(settings.dst_path + "convos.pot")
