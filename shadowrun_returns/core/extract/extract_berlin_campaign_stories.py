#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.stories_pb2 as stories_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/stories/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/stories/'

pot = LocalizationCatalog() 
 
def extract_story_proto(story):
    pot['story'] += story.name
    pot['story'] += story.c3 
    
def extract_story(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, stories_pb2.story())
    extract_story_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_campaign_stories')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_story(filename)
    pot.save(settings.dst_path + "stories.pot")