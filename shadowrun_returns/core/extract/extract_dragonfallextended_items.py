#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import sys
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/items/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/items/'

pot = LocalizationCatalog()
 
def extract_item_proto(item):
    if item.uirep.HasField('name'): 
        pot['item'] += item.uirep.name
    if item.uirep.HasField('description'): 
        pot['item'] += item.uirep.description 

    if item.HasField('activationStatusEffect'):
        if item.activationStatusEffect.uirep.HasField('name'):
            pot['item'] += item.activationStatusEffect.uirep.name
        if item.activationStatusEffect.uirep.HasField('description'): 
            pot['item'] += item.activationStatusEffect.uirep.description
        
    if item.HasField('character_ui_name'):
        pot['item'] += item.character_ui_name
    if item.HasField("cannotTargetMessage"):
        pot['item'] += item.cannotTargetMessage
        
def extract_item(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, items_pb2.item())
    extract_item_proto(proto)

def process():
    if settings.debug:
        print (u'extract_dragonfall_extended_items')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_item(filename)
    pot.save(settings.dst_path + "items.pot")
    
