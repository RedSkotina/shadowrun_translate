#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.convos_pb2 as convos_pb2
import shadowrun_returns.lib.proto.stories_pb2 as stories_pb2
import shadowrun_returns.lib.proto.scenes_pb2 as scenes_pb2
import shadowrun_returns.lib.proto.items_pb2 as items_pb2
import shadowrun_returns.lib.proto.maps_pb2 as maps_pb2
import shadowrun_returns.lib.proto.abilities_pb2 as abilities_pb2
import shadowrun_returns.lib.proto.hiringset_pb2 as hiringset_pb2
import shadowrun_returns.lib.proto.modes_pb2 as modes_pb2
import shadowrun_returns.lib.proto.chars_pb2 as chars_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/dead_man_switch/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/dead_man_switch/data/'

pot = LocalizationCatalog() 

def extract_ability_proto(ability):
    if ability.HasField("uirep"):
        if ability.uirep.HasField("name"):
            pot['ability'] += ability.uirep.name
        if ability.uirep.HasField("description"):
            pot['ability'] += ability.uirep.description
    
    if ability.HasField("cannotTargetMessage"):
        pot['ability'] += ability.cannotTargetMessage
        
def extract_char_proto(char):
    pot['char'] += char.char_name
    pot['char'] += char.description_bio
    pot['char'] += char.hiring_tag
    
def sub_extract_convo_goal(convo): 
    for node in convo.nodes:
        search_result = walkTsCall(node.actions.ops, search_function_name_predicate)
        for i in search_result:
            pot['convo_goal'] += i.string_value
            
        for branche in node.branches:
            search_result = walkTsCall(branche.actions.ops, search_function_name_predicate)
            for i in search_result:
                pot['convo_goal'] += i.string_value
    for root in convo.roots:
        search_result = walkTsCall(root.actions.ops, search_function_name_predicate)
        for i in search_result:
            pot['convo_goal'] += i.string_value
    
def sub_extract_convo(convo):
    for i in convo.nodes:
        pot['convo'] += i.text
        for r in i.branches:
            pot['convo'] += r.responseText
    for i in convo.roots:
        pot['convo'] += i.responseText
    for node in convo.nodes:
        for branch in node.branches:
            search_result = walkTsCall(branch.actions.ops, search_display_text_predicate)
            for i in search_result:
                pot['convo'] += i.string_value
    
    
def extract_convo_proto(convo):
    sub_extract_convo_goal(convo)        
    sub_extract_convo(convo)            
    
def extract_item_proto(item):
    if item.uirep.HasField('name'): 
        pot['item'] += item.uirep.name
    if item.uirep.HasField('description'): 
        pot['item'] += item.uirep.description 

    if item.HasField('activationStatusEffect'):
        if item.activationStatusEffect.uirep.HasField('name'):
            pot['item'] += item.activationStatusEffect.uirep.name
        if item.activationStatusEffect.uirep.HasField('description'): 
            pot['item'] += item.activationStatusEffect.uirep.description
        
    if item.HasField('character_ui_name'):
        pot['item'] += item.character_ui_name
    if item.HasField("cannotTargetMessage"):
        pot['item'] += item.cannotTargetMessage
    
def extract_map_proto(map):
    for props in map.props:
        if props.HasField('interactionRoot'):
            if props.interactionRoot.HasField('prerequisites'):
                search_result = walkTsCall(props.interactionRoot.prerequisites.ops, search_function_name_predicate)
                for i in search_result:
                    pot['map'] += i.string_value
                
            if props.interactionRoot.HasField('inspectInteraction'):
                pot['map_inspect'] += props.interactionRoot.inspectInteraction.inspectText
    
            if props.interactionRoot.HasField('transitionPoint'):
                pot['map_transition'] += props.interactionRoot.transitionPoint.confirmation_text
        
    for props in map.props:
        for property in props.properties: 
            if property.property_id == 'NewsTickerText':
                pot['map_news'] += property.string_value
                
    
    
    for prop in map.props:
        interactionType = ""
        if prop.HasField('interactionRoot'):
            if prop.interactionRoot.HasField('jackPoint'):
                if prop.interactionRoot.jackPoint.HasField('LANIdentifier') and prop.interactionRoot.jackPoint.LANIdentifier != "":
                    interactionType = "InteractionType_JackPoint"
            else:
                if prop.interactionRoot.HasField('summonPoint'):
                    interactionType = "InteractionType_SummonPoint"
                elif prop.interactionRoot.HasField('alarmPoint'):
                    interactionType = "InteractionType_Alarm"
                elif prop.interactionRoot.HasField('pickupItem'):
                    interactionType = "InteractionType_ItemPickup"
                elif prop.interactionRoot.HasField('conversationPoint'):
                    interactionType = "InteractionType_Conversation"
                elif prop.interactionRoot.HasField('doorPoint'):
                    interactionType = "InteractionType_Door"
                elif prop.interactionRoot.HasField('transitionPoint'):
                    interactionType = "InteractionType_Transition"
                elif prop.interactionRoot.HasField('matrixSpawnPoint'):
                    interactionType = "InteractionType_MatrixSpawnPoint"
                elif prop.interactionRoot.HasField('matrixNode'):
                    interactionType = "InteractionType_MatrixNode"
                elif prop.interactionRoot.HasField('matrixLabel'):
                    interactionType = "InteractionType_DummyMatrixLabel"
                elif prop.interactionRoot.HasField('dummyInteraciton'):
                    interactionType = "InteractionType_Dummy"
                elif prop.interactionRoot.HasField('inspectInteraction'):
                    interactionType = "InteractionType_Inspect"
                elif prop.interactionRoot.HasField('warpInteraction'):
                    interactionType = "InteractionType_Generic"
                elif prop.interactionRoot.HasField('is_interaction_doc_wagon'):
                    interactionType = "InteractionType_DocWagon"
                else:
                    interactionType = "InteractionType_Generic"
                    #original_list.append(prop.displayName)
                    #TextEnabled = False
        if interactionType == "InteractionType_MatrixNode":
            islinked = False
            for hacking_object in prop.interactionRoot.matrixNode.hacking_objects:
                for tprop in map.props:
                    if tprop.HasField('idRef') and tprop.idRef.id == hacking_object:
                        islinked = True
                        pot['map_hackingobject'] += tprop.displayName
            if not islinked:
                #Didn't link Matrix Node  to any other object
                pot['map_hackingobject'] += prop.displayName
        elif interactionType == "InteractionType_DummyMatrixLabel":
            #Dummy Matrix Label for object
            pot['map_hackingobject'] += prop.displayName
            pass
        # TEST ONLY: all display name
        #if prop.HasField('displayName'):
        #    original_list.append(prop.displayName)
            
    


def extract_misc_file(text_file):
    pot['misc'] += text_file
    
def extract_mode_proto(mode):
    if mode.HasField("uirep"):
        if mode.uirep.HasField("name"):
            pot['mode'] += mode.uirep.name
        if mode.uirep.HasField("description"):
            pot['mode'] += mode.uirep.description 
    

def extract_scene_proto(scene):
    
    pot['scene'] += scene.scene_title
    pot['scene'] += scene.scene_synopsis
    
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            pot['scene_triggers'] += i.string_value
                    
    for i in scene.goals:
        pot['scene_goals'] += i.name
        
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.events.ops, search_function_name_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_function_name_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_function_name_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_function_name_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for i in search_result:
            pot['scene_triggers'] += i.string_value
        
    # some replacement string
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_fn_set_string_predicate)
        search_result2 = walkTsCall(trigger.elseActions.ops, search_fn_set_string_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            pot['scene_triggers'] += i.string_value
         
    
    # set screen label
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            pot['scene_triggers'] += i.string_value
        
    # npc names
    for character in scene.characters:
        pot['scene_characters'] += character.character_instance.char_name
    
    from functools import partial
    
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        search_result3 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        search_result4 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        #search_result5 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        #search_result6 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        #search_result7 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        #search_result8 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        #search_result9 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        #search_result10 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        #search_result11 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        #search_result12 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        
        search_result = search_result1 + search_result2 + search_result3 + search_result4 #+ search_result5 + search_result6 + search_result7 + search_result8 + search_result9 + search_result10 + search_result11 + search_result12
        
        for i in search_result:
            pot['scene_nodes'] += i.string_value
            
    
def extract_story_proto(story):
    pot['story'] += story.name
    pot['story'] += story.c3  
      
#-----------------------------------------------------    
def extract_abilities(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'abilities/'+ id, abilities_pb2.ability())
    extract_ability_proto(proto)
    
def extract_chars(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'chars/'+ id, chars_pb2.ch_inst())
    extract_char_proto(proto)
    
def extract_convos(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'convos/'+ id, convos_pb2.convo())
    extract_convo_proto(proto)
    
def extract_items(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'items/'+ id, items_pb2.item())
    extract_item_proto(proto)
    
def extract_maps(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'maps/'+ id, maps_pb2.map())
    extract_map_proto(proto)
    
def extract_misc(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    text_file = parse_utf8_text_file(settings.src_path + 'misc/' + id)
    extract_misc_file(text_file)
    
def extract_modes(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'modes/'+ id, modes_pb2.mode())
    extract_mode_proto(proto)
    
def extract_scenes(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'scenes/'+ id, scenes_pb2.scene())
    extract_scene_proto(proto)
    
def extract_stories(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    proto =  parse_protobuf_file(settings.src_path + 'stories/'+ id, stories_pb2.story())
    extract_story_proto(proto)
    
#-------------------------------------------------    
def process():
    if settings.debug:
        print (u'extract_deadman')

    filenames = filelist_from_directory(settings.src_path + 'abilities',"*.bytes")
    for filename in filenames:
        extract_abilities(filename)
    filenames = filelist_from_directory(settings.src_path + 'chars',"*.ch_inst.bytes")
    for filename in filenames:
        extract_chars(filename)
    filenames = filelist_from_directory(settings.src_path + 'convos',"*.bytes")
    for filename in filenames:
        extract_convos(filename)
    filenames = filelist_from_directory(settings.src_path + 'items',"*.bytes")
    for filename in filenames:
        extract_items(filename)
    filenames = filelist_from_directory(settings.src_path + 'maps',"*.bytes")
    for filename in filenames:
        extract_maps(filename)
    filenames = filelist_from_directory(settings.src_path + 'misc',"*.bytes",ignore='*.*.bytes')
    for filename in filenames:
        extract_misc(filename)
    filenames = filelist_from_directory(settings.src_path + 'modes',"*.bytes")
    for filename in filenames:
        extract_modes(filename)
    filenames = filelist_from_directory(settings.src_path + 'scenes',"*.bytes")
    for filename in filenames:
        extract_scenes(filename)
    filenames = filelist_from_directory(settings.src_path + 'stories',"*.bytes")
    for filename in filenames:
        extract_stories(filename)
    
    #po.make_unique()
    pot.save(settings.dst_path + "deadmanswitch.pot")
    
    