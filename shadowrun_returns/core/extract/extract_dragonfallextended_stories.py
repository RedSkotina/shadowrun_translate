#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import sys
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.stories_pb2 as stories_pb2
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/stories/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/stories/'

pot = LocalizationCatalog() 
 
def extract_story_proto(story):
    pot['story'] += story.id
    pot['story'] += story.description 
    
    for variable in story.variables:
        if variable.HasField('defaultValue'):
            if variable.defaultValue.HasField('string_value'):
                pot['story_var'] += variable.defaultValue.string_value
    #TeamAdvancement Crew Title
    for crew_entry in story.crew_list:
        if crew_entry.HasField('title'):
            pot['story_crew_list'] += crew_entry.title
    #TeamAdvancement Stat Header
    for crew_entry in story.crew_list:
        for crew_node in crew_entry.active_nodes:
            if crew_node.HasField('name'):
                pot['story_crew_node'] += "Gained at %s" % crew_node.name
        for crew_node in crew_entry.completed_nodes:
            if crew_node.HasField('name'):
                pot['story_crew_node'] += "Gained at %s" % crew_node.name
            
def extract_story(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, stories_pb2.story())
    extract_story_proto(proto)

def process():
    if settings.debug:
        print (u'extract_dragonfall_extended_stories')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_story(filename)
    pot.save(settings.dst_path + "stories.pot")
