#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.modes_pb2 as modes_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/modes/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/modes/'
    
pot = LocalizationCatalog()

def extract_mode_proto(mode):
    if mode.HasField("uirep"):
        if mode.uirep.HasField("name"):
            pot['mode'] += mode.uirep.name
        if mode.uirep.HasField("description"):
            pot['mode'] += mode.uirep.description 
    
def extract_mode(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, modes_pb2.mode())
    extract_mode_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_campaign_modes')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_mode(filename)
    pot.save(settings.dst_path + "modes.pot")