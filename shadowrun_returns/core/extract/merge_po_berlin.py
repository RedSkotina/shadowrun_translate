#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin/data/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin/data/'

class lsettings:
    dst_po_file = 'berlin.pot'
    
pot = create_po()

def load_and_append_po(dir, fname):
    global pot
    po_file_t = polib.pofile(os.path.join(dir, fname))
    for po_entry in po_file_t:
        pot.append(po_entry)

def merge_po_berlin(root_dir, out_file):
    traverse_directory(root_dir, pattern = '*.pot', ignore_dir = '*loc', func = load_and_append_po)
    pot.make_unique()
    assure_path_exists(out_file)
    pot.save(out_file)
        
def process():
    if settings.debug:
        print (u'merge_po_berlin')
    root_dir = settings.dst_path
    out_file = os.path.join(settings.dst_path, 'loc',lsettings.dst_po_file)
    merge_po_berlin(root_dir, out_file)
    