#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.scenes_pb2 as scenes_pb2
import builtins 

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/scenes/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin_campaign/data/scenes/'
    
pot = LocalizationCatalog() 
 
def extract_scene_proto(scene):
    pot['scene'] += scene.scene_title
    pot['scene'] += scene.scene_synopsis
    
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            pot['scene_triggers'] += i.string_value
                    
    for i in scene.goals:
        pot['scene_goals'] += i.name
        
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.events.ops, search_function_name_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_function_name_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_function_name_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_function_name_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for i in search_result:
            pot['scene_triggers'] += i.string_value
        
    # some replacement string
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_fn_set_string_predicate)
        search_result2 = walkTsCall(trigger.elseActions.ops, search_fn_set_string_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            pot['scene_triggers'] += i.string_value
         
    
    # set screen label
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            pot['scene_triggers'] += i.string_value
            
    # npc names
    # contain hacked object . continue from maps
    for character in scene.characters:
        pot['scene_characters'] += character.character_instance.char_name
    
    
    
    # look translate_berlin_campaign_ext
    
    for trigger in scene.triggers:
        #search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        
        # kreizbazar bug with loadscreen . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_set_var_string_synopsis_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_set_var_string_synopsis_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_set_var_string_synopsis_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_set_var_string_synopsis_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for i in search_result:
            for arg in i.args:
                if arg.HasField('string_value'):
                    pot['scene'] += arg.string_value
        # bug with Display Text In Popup . work only after restart
        search_result1 = walkTsCall(trigger.events.ops, search_display_text_in_popup_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_display_text_in_popup_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_display_text_in_popup_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                pot['scene_popup'] += arg.string_value
        
        #bug with Set Screen Label, Set Screen Label Progress Bar (int)
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for arg in search_result:
            if arg.HasField('string_value'):
                pot['scene_progress'] += arg.string_value
                   
        from functools import partial
        # bug with a2_lightsout_s1   str_RedOrGreen
        search_result1 = walkTsCall(trigger.events.ops,  partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result3 = walkTsCall(trigger.conditions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result4 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'str_RedOrGreen'))
        search_result = search_result1 + search_result2 + search_result3 + search_result4 
        for arg in search_result:
            if arg.HasField('string_value'):
                pot['scene_progress'] += arg.string_value
        
        # some bug with string
        
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        search_result2 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        search_result3 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        search_result4 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        #search_result5 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        #search_result6 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        #search_result7 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        #search_result8 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        #search_result9 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode1UIString'))
        #search_result10 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNode2UIString'))
        #search_result11 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusActive'))
        #search_result12 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'sNodeUIStatusDisabled'))
        
        search_result = search_result1 + search_result2 + search_result3 + search_result4 #+ search_result5 + search_result6 + search_result7 + search_result8 + search_result9 + search_result10 + search_result11 + search_result12
        
        for i in search_result:
            pot['scene_nodes'] += i.string_value
            
    # hacked object . continue from maps
    # we yet extract ALL names
    #for character in scene.characters:
    #    if character.idRef.id in builtins.g_char_scene_idrefs:
    #        original_list.append(character.character_instance.char_name)
    
    # bug with a2_lightsout_s1   str_RedOrGreen
    # inital state of variable
    for variable in scene.variables:
        if variable.HasField('variableref_value'):
            if variable.variableref_value.name == "str_RedOrGreen":
                 pot['scene_progress'] += variable.string_value         
    
    
def extract_scene(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, scenes_pb2.scene())
    extract_scene_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_campaign_scenes')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_scene(filename)
    pot.save(settings.dst_path + "scenes.pot")
