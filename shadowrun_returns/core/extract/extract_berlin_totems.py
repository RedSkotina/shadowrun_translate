#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.totems_pb2 as totems_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin/data/totems/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin/data/totems/'

pot = LocalizationCatalog()
    
def extract_totem_proto(totem):
    for t in totem.ctotems:
        pot['totem'] += t.name
        pot['totem'] += t.description
    
def extract_totem(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, totems_pb2.totem())
    extract_totem_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_totems')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_totem(filename)
    pot.save(settings.dst_path + "totems.pot")