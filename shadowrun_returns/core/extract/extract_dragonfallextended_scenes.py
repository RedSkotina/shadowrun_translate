#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import sys
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.scenes_pb2 as scenes_pb2
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/scenes/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/scenes/'

pot = LocalizationCatalog() 
 
def extract_scene_proto(scene):
    pot['scene'] += scene.scene_title
    pot['scene'] += scene.scene_synopsis
    
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_display_text_predicate)
        search_result2 = walkTsCall(trigger.elseActions.ops, search_display_text_predicate)
        search_result3 = walkTsCall(trigger.actions.ops, search_display_text_in_popup_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_display_text_in_popup_predicate)
        
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for i in search_result:
            pot['scene_triggers'] += i.string_value
                    
    for i in scene.goals:
        pot['scene_goals'] += i.name
        
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.events.ops, search_function_name_predicate)
        search_result2 = walkTsCall(trigger.actions.ops, search_function_name_predicate)
        search_result3 = walkTsCall(trigger.conditions.ops, search_function_name_predicate)
        search_result4 = walkTsCall(trigger.elseActions.ops, search_function_name_predicate)
        search_result = search_result1 + search_result2 + search_result3 + search_result4
        for i in search_result:
            pot['scene_triggers'] += i.string_value
        
    # some replacement string
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_fn_set_string_predicate)
        search_result2 = walkTsCall(trigger.elseActions.ops, search_fn_set_string_predicate)
        search_result = search_result1 + search_result2
        for i in search_result:
            pot['scene_triggers'] += i.string_value
         
    
    # set screen label
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, search_set_screen_label_predicate)
        search_result2 = walkTsCall(trigger.events.ops, search_set_screen_label_predicate)
        search_result3 = walkTsCall(trigger.elseActions.ops, search_set_screen_label_predicate)
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            pot['scene_triggers'] += i.string_value
            
    # npc names
    # contain hacked object . continue from maps
    for character in scene.characters:
        pot['scene_characters'] += character.character_instance.char_name
    
    # scene.badfantasy
    from functools import partial
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'badfantasy'))
        search_result2 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'badfantasy'))
        search_result3 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'badfantasy'))
        
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            pot['scene_triggers'] += i.string_value
    # scene.CafeSpecial
    for trigger in scene.triggers:
        search_result1 = walkTsCall(trigger.actions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'CafeSpecial'))
        search_result2 = walkTsCall(trigger.elseActions.ops, partial(search_fn_set_string_specific_predicate, var_name = 'CafeSpecial'))
        search_result3 = walkTsCall(trigger.events.ops, partial(search_fn_set_string_specific_predicate, var_name = 'CafeSpecial'))
        
        search_result = search_result1 + search_result2 + search_result3
        for i in search_result:
            pot['scene_triggers'] += i.string_value
    
def extract_scene(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, scenes_pb2.scene())
    extract_scene_proto(proto)
    
   

def process():
    if settings.debug:
        print (u'extract_dragonfall_extended_scenes')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_scene(filename)
    pot.save(settings.dst_path + "scenes.pot")
    