#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
#sys.path.insert(0, 'lib/proto')
import shadowrun_returns.lib.proto.abilities_pb2 as abilities_pb2

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/berlin/data/abilities/'
    dst_local_path = 'StreamingAssets/ContentPacks/berlin/data/abilities/'

pot = LocalizationCatalog() 
    
def extract_ability_proto(ability):
    if ability.HasField("uirep"):
        if ability.uirep.HasField("name"):
            pot['ability'] += ability.uirep.name
        if ability.uirep.HasField("description"):
            pot['ability'] += ability.uirep.description
    if ability.HasField("cannotTargetMessage"):
        pot['ability'] += ability.cannotTargetMessage
        
def extract_ability(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
        
    proto =  parse_protobuf_file(settings.src_path + id, abilities_pb2.ability())
    extract_ability_proto(proto)
    
def process():
    if settings.debug:
        print (u'extract_berlin_abilities')
    filenames = filelist_from_directory(settings.src_path,"*.bytes")
    for filename in filenames:
        extract_ability(filename)
    pot.save(settings.dst_path + "abilities.pot")
 