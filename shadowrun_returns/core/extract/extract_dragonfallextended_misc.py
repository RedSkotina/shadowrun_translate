#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import polib

class settings(global_settings):
    src_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/misc/'
    dst_local_path = 'StreamingAssets/ContentPacks/DragonfallExtended/data/misc/'
   
pot = LocalizationCatalog() 

def extract_misc_file(text_file):
    pot['misc'] += text_file
    
def extract_misc(id):
    if settings.debug:
        print (u'\tid: "%s"' % id)
    text_file = parse_text_file(settings.src_path + id)
    extract_misc_file(text_file)
    
def process():
    filenames = filelist_from_directory(settings.src_path,"*.bytes",ignore='berlin_campaign.pflib.bytes')
    for filename in filenames:
        extract_misc(filename)
    pot.save(settings.dst_path + "misc.pot")
