#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import polib
import re
import shutil
class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''

def make_environment():
    print('\tmake_environment')
    readme_text = """\
Folder / Директория 'overwrite': 
    Localized game files / Локализованые файлы игры
    Just 'copy and replace' content this folder in the Game root folder / Просто скопируйте с заменой содержимое директории в корневую директорию Игры
"""
    assure_path_exists(os.path.normpath(settings.dst_path + '../../readme.txt'))
    with io.open(os.path.normpath(settings.dst_path + '../../readme.txt'), 'w', encoding = 'utf-8') as f:
        f.write(readme_text) 

    src_langswitch_name = os.path.normpath('./lib/langswitch/language_switcher.exe')
    dst_langswitch_name = os.path.normpath(settings.dst_path + '../language_switcher.exe')
        
    assure_path_exists(dst_langswitch_name)
    shutil.copy(src_langswitch_name, dst_langswitch_name) 
    
def make_pack_interface(textasset_file, assets_filename, fid):
    print('\tmake_interface')
    dst_uu_name = os.path.normpath(settings.dst_path + '../../inject/uu')
    assure_path_exists(dst_uu_name + '/plugins/dummy.log')  # WORKAROUND BUG IN COPYTREE
    copytree('./lib/uu', dst_uu_name, copy_function = shutil.copy)
    #print (get_filename_without_path(textasset_file))
    # a
    assets_filename = os.path.normpath(settings.src_path + 'resources.assets')
    unity = UnityDll("/lib/unity/", "unity.dll")
    unity.OpenArchive(assets_filename)
    unity_index = unity.get_index()
    unity.CloseArchive()
    print("\tfrom %s" % textasset_file)
    print("\tto fid: %d"%fid)
    dst_textasset_filename = ''
    try:
        dst_textasset_filename = unity_index[fid].filename
    except KeyError as e:
        print("\tError: Cant find file with fid = %d" %fid)
        sys.exit(0)
    dst_res_name = os.path.normpath(settings.dst_path + '../../inject/resources/'+dst_textasset_filename)
        
    assure_path_exists(dst_res_name)
    shutil.copy(textasset_file,dst_res_name)   
    inject_bat_text = """\
rem INJECT TO GAME. KEEP SLASH!
set default_game_path="e:/games/SteamLibrary/SteamApps/common/Shadowrun Dragonfall Director's Cut"

IF "%1" == "" (set game_path=%default_game_path%) ELSE ( set game_path="%~1")

	
rem DONT EDIT. MAGIC! 
set dst_resources=%game_path%\\Dragonfall_Data\\resources.assets
set src_resources=resources

set uu=uu\\uu.exe

for %%f in (%src_resources%/*) do %uu%  -p --src_path %src_resources%\ -f %dst_resources% %%f
"""
    assure_path_exists(os.path.normpath(settings.dst_path + '../../inject/inject.bat'))
    with io.open(os.path.normpath(settings.dst_path + '../../inject/inject.bat'), 'w') as f:
        f.write(inject_bat_text)
    
    readme_text = """\
Folder / Директория 'overwrite': 
    Localized game files / Локализованые файлы игры
    Just 'copy and replace' content this folder in the Game root folder / Просто скопируйте с заменой содержимое директории в корневую директорию Игры
Folder / Директория 'inject': 
    Inject some files in resource.assets / Встраивает некоторые файлы в resource.assets
    Execute/Выполните:   inject.bat \"FULL_PATH_TO_GAME/ПОЛНЫЙ_ПУТЬ_К_ИГРЕ\"
"""
    assure_path_exists(os.path.normpath(settings.dst_path + '../../readme.txt'))
    with io.open(os.path.normpath(settings.dst_path + '../../readme.txt'), 'w', encoding = 'utf-8') as f:
        f.write(readme_text)
    
    src_langswitch_name = os.path.normpath('./lib/langswitch/language_switcher.exe')
    dst_langswitch_name = os.path.normpath(settings.dst_path + '../language_switcher.exe')
        
    assure_path_exists(dst_langswitch_name)
    shutil.copy(src_langswitch_name, dst_langswitch_name) 
   
def process():
    if settings.debug:
        print (u'make_environment')    
    make_environment()
