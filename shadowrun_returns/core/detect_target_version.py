#!/usr/bin/env python3
from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from shadowrun_returns.lib.counters import *
import os, sys
import polib
import ctypes
import struct
import tempfile
import subprocess
import re
class settings(global_settings):
    src_local_path = ''
    dst_local_path = ''

def ildasm(assembly, item):
    r = b''
    try:
        r = subprocess.check_output(['lib\\ildasm\\ildasm.exe','/utf8', '/item:"%s"'%item, '%s'%assembly])
    except Exception as e:
        print(e)
        sys.exit()
    r = r.decode('utf-8') 
    return r
def parse_version(text):
    version = None
    m = re.search('\"([\d|\.]+)\"(?:\\|\n|:|\w|\s)+Constants::VERSION_NUMBER',text, re.M)
    if m != None:
        version = m.groups(1)[0]
        print("\tdetected version %s" % version)
        
    return version
    
def process():
    if settings.debug:
        print (u'detect_target_version')
    version = settings.game_version
    if settings.auto_path == False and os.path.isfile(settings.src_path+'Managed/Assembly-CSharp.dll'):
        r = ildasm(settings.src_path+'Managed/Assembly-CSharp.dll','Constants::.cctor')
        version = parse_version(r)
    if version is None:
        print('\tsrc_root_path %s' % settings.src_root_path)
        print('\tgame_version not detected. Try choose manually --version')
        sys.exit()
    print('\tDetected game_version %s' % version)
    return version