from shadowrun_returns.core.global_settings import global_settings
from shadowrun_returns.lib.utils import *
from .notabenoid import *
#import polib

class settings(global_settings):
    src_local_path = 'book/56516'
    dst_local_path = 'extended/'
 
def process():
    if settings.debug:
        print (u'fetch_extended')
    settings.auto_path = False
    print(settings.src_path)
    print(settings.dst_path)
    
    nota = Notabenoid()
    nota.user = settings.user
    nota.password = settings.password
    nota.debug = settings.debug
    nota.fetch_context = settings.fetch_context
    nota.connect()
    
    content_index = nota.get_content_index(settings.src_path)
    content_index = nota.filter(content_index)
    
    base_url = settings.src_path + '/'
    base_dir = settings.dst_path 
    for name,chapter_url in content_index.items():
        po = nota.get_content( base_url + chapter_url, base_dir + name)
        #get_content(site, 'http://notabenoid.org/book/42395/242762', base_dir + 'tt')
        assure_path_exists(base_dir + name + '.po')
        po.save(base_dir + name + '.po')
        