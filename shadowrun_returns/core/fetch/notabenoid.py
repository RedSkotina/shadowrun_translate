from shadowrun_returns.lib.utils import *
#http://my.opera.com/RockBomber/blog/show.dml/36916422
import bs4
from bs4 import BeautifulSoup as Soup
from soupselect import select
#Tuning bs4 for preserve whitespaces in tags
bs4.builder.HTMLTreeBuilder.preserve_whitespace_tags |= set(["p","br"])

class Notabenoid(Site):
    fetch_context = False
    def __init__(self):
        Site.__init__(self,'notabenoid.org')
        
    def get_content_index(self, index_url):
        content_index = {}
        r = self.open_url(index_url)
        soup_text = Soup(r.data,"html.parser") ##Soup.PRESERVE_WHITESPACE_TAGS |= set(["p"])    class HTMLTreeBuilder(TreeBuilder): preserve_whitespace_tags = set(['p','br','pre', 'textarea'])
        if self.debug:
            print ('Fetch index from %s' % (index_url))
        content_index_html = select(soup_text, 'table#Chapters tr td.t a')
        for i in range(len(content_index_html)):
            soup = Soup(content_index_html[i].encode('utf-8'),"html.parser")
            for k in soup.find_all('a'):
                href = k['href']
                if href.startswith('/'):
                    href = href[1:]  # remove leading slash
                href_splited = href.split('/')
                content_index[k.text] = href_splited[-1]
        return content_index

    def filter(self, content_index):
        r = {}
        for key,value in content_index.items():
            if not key.startswith('-ignore-'):
                r[key] = value
        return r
    '''    
    def safe_open_url(req, url):
        while (True):
            try:
                page_html = req.open(url).read()
            except urllib.error.HTTPError as error:
                if error.code == 500:
                    print ("ERROR: http respond code 500 ... trying again after 3 sec\n")
                    time.sleep(3)
                    continue
                else:
                    raise
            break
      
        return page_html
    '''
    def parse_comment(self, txt):
        # notabenoid bug : using \u00bb instead left-"
        # notabenoid bug : using \u00ab instead right-"
        context = re.search('#msgctxt \u00ab(\w+)\u00bb',txt, re.U)
        comment = re.search('#comment \u00ab(\w+)\u00ab',txt, re.U)
        if context != None:
            context = context.group(1)
            #safeprint(context)
        if comment != None:
            comment = comment.group(1)
            #safeprint(comment)        
        return (context,comment)
        
    def get_content(self, url, filename):
        if self.debug:
            print ("url: %s" % url)
            print ("file: %s" % filename)
        chapter_url = url
        page = 1
        r = self.open_url(url)
        main_soup = Soup(r.data,"html.parser")
      
        po = create_po() 
        
        while True :
            print ('Fetch %s?Orig_page=%d' % (url, page))
            tr_rows = select(main_soup, 'table.translator tr')
            for tr_row in tr_rows:
                context_text = None
                comment_text = None
                #record_id
                if not tr_row.has_attr('id'):
                    continue
                record_id = tr_row['id'][1:]
                #print(record_id)
                #origin
                origin_p_tag = select(tr_row, 'td.o p.text')[0]
                origin_text = origin_p_tag.text
                #safeprint(origin_text)
                # translate
                translate_td_tag = select(tr_row, 'td.t')[0] # div.best p.text
                best_translates = select(translate_td_tag, 'div.best p.text')
                if  best_translates == []:
                    best_translates = select(translate_td_tag, 'div p.text')
                    if best_translates == []:
                        best_translates = [Soup('',"html.parser")]
                translate_text = best_translates[0].text 
                #safeprint(translate_text)
                # comment
                if self.fetch_context:
                    c = self.open_ajax_url(chapter_url+'/'+record_id+'/comments?ajax=1')
                    comments_response = Soup(c.data,"html.parser")
                    comments = select(comments_response, 'div.comments div.text p')
                    if comments != []:
                        first_comment = comments[0].text
                        context_text, comment_text = parse_comment(first_comment)
                    
                entry = polib.POEntry(
                    msgctxt = context_text,
                    comment = comment_text,
                    msgid  = origin_text,
                    msgstr = translate_text
                    )
                po.append(entry)
                
            if not select(main_soup, 'div#pages-bottom p.n'):  # statement: link on next page
                break
              
            time.sleep(1)  
            page = page + 1
            r = self.open_url(url+'?Orig_page='+str(page))
            main_soup = Soup(r.data,"html.parser")
        
        return po
        

     