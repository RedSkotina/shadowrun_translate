#!/usr/bin/env python3
def process():
    if settings.game_version == "1.1.0":
        process(translate_assembly)
        process(translate_credits)
        process(translate_deadman_convos)
        process(translate_deadman_items)
        process(translate_deadman_maps)
        process(translate_deadman_misc)
        process(translate_deadman_scenes)
        process(translate_deadman_stories)
        process(translate_seattle_abilities)
        process(translate_seattle_hirings)
        process(translate_seattle_items)
        process(translate_seattle_modes)
    elif settings.game_version == "1.2.3":
        process(translate_mo_interface)
        process(translate_mo_deadman)
        process(translate_mo_seattle)
    elif settings.game_version == "1.2.7":
        process(translate_berlin_campaign_maps)
        process(translate_berlin_campaign_convos)
        process(translate_berlin_campaign_scenes)
        process(translate_mo_interface_correct)
        process(translate_mo_deadman)
        process(translate_mo_seattle)
        process(translate_po_berlin)
        process(translate_po_berlin_campaign)
        process(translate_berlin_campaign_misc)
        #translate passwords
    elif settings.game_version == "2.0.7":
        #translate_dragonfallextended_maps.process()
        #translate_dragonfallextended_convos.process()
        #translate_dragonfallextended_scenes.process()
        #translate_dragonfallextended_misc.process()
        #translate_mo_interface_correct.process()
        #translate_mo_deadman.process()
        #translate_mo_seattle.process()
        process(translate_po_berlin)
        #translate_po_dragonfallextended.process()
        
