#!/usr/bin/env python3
from shadowrun_returns.core.extract import * 

def process():
    if settings.game_version == "1.1.0":
        pass
        #import extract_110
    elif settings.game_version == "1.2.7":
        process(extract_berlin_abilities)
        process(extract_berlin_chars)
        process(extract_berlin_items)
        process(extract_berlin_modes)
        process(extract_berlin_totems)
        process(extract_berlin_campaign_abilities)
        process(extract_berlin_campaign_chars)
        process(extract_berlin_campaign_convos)
        process(extract_berlin_campaign_items)
        process(extract_berlin_campaign_maps)
        process(extract_berlin_campaign_misc)
        process(extract_berlin_campaign_modes)
        process(extract_berlin_campaign_scenes)
        process(extract_berlin_campaign_stories)
        process(merge_po_berlin)
        process(merge_po_berlin_campaign)
    elif settings.game_version == "2.0.7":
        process(extract_dragonfallextended_abilities)
        process(extract_dragonfallextended_chars)
        process(extract_dragonfallextended_convos)
        process(extract_dragonfallextended_items)
        process(extract_dragonfallextended_maps)
        process(extract_dragonfallextended_misc)
        process(extract_dragonfallextended_modes)
        process(extract_dragonfallextended_scenes)
        process(extract_dragonfallextended_stories)
        process(merge_po_dragonfallextended)

